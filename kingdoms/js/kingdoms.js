//(() => {
  class MegaSocket {
    constructor(url, options) {
      this.url = url;
      this.options = options || {};
      this.callstack = {
        open: [],
        close: [],
        message: [],
      }
      if (this.options.keepAlive == undefined) this.options.keepAlive = true;
      if (this.options.autoconnect == undefined) this.options.autoconnect = true;
      if (this.options.reconnect == undefined) this.options.reconnect = true;
      if (this.options.autoconnect) {
        this.status = 'connecting';
        this.connect();
      } else {
        this.status = 'idle';
        window.addEventListener('offline', function() { // Arrow Function Wont Work Due To Bind
          this.socket.close();
          this.socket.onclose();
        }.bind(this));
        if (this.options.reconnect) {
          window.addEventListener('online', function() {
            this.connect();
          }.bind(this))
        }
      }
    }
    connect() {
      try {
        this.socket = new WebSocket(this.url);
      } catch (e) {
        console.warn('WebSocket had trouble connecting to ' + this.url);
      }
      this.socket.onopen = function() {
        this.status = 'connected';
        if (this.options.keepAlive) {
          this.socket.keepAlive = setInterval(function() {
            this.socket.send('|');
          }.bind(this), 50000);
        }
        var l = 0;
        while (l < this.callstack.open.length) {
          this.callstack.open[l]();
          l++;
        }
      }.bind(this);
      this.socket.onmessage = function(data) {
        try {
          data = window.jsonpack.unpack(Helper.de(data.data));
        } catch (e) {
          alert('Error converting jsonpack from websocket message: ' + Helper.de(data.data));
        }
        // custom error handling for server
        if (data.status === 'error') {
          alert('MegaSocket.RemoteRejection: '+data.message);
          return;
        }
        // end
        var l = 0;
        while (l < this.callstack.message.length) {
          this.callstack.message[l](data);
          l++;
        }
      }.bind(this);
      this.socket.onclose = function() {
        clearInterval(this.socket.keepAlive);
        this.status = 'disconnected';
        var l = 0;
        while (l < this.callstack.close.length) {
          this.callstack.close[l]();
          l++;
        }
        if (this.options.reconnect) {
          this.connect();
        }
      }.bind(this);
    }
    on(event, operation) {
      if (event == 'connect') {
        this.callstack.open.push(operation);
      }
      if (event == 'message') {
        this.callstack.message.push(operation);
      }
      if (event == 'close') {
        this.callstack.close.push(operation);
      }
    }
    no(event) {
      if (event == 'connect') {
        this.callstack.open = [];
      }
      if (event == 'message') {
        this.callstack.message = [];
      }
      if (event == 'close') {
        this.callstack.close = [];
      }
    }
    send(data) {
      data = Helper.en(window.jsonpack.pack(data));
      this.socket.send(data);
    }
    close() {
      this.socket.close();
    }
  }

  class Helper {
    static en(c) {var x='charCodeAt',b,e={},f=c.split(""),d=[],a=f[0],g=256;for(b=1;b<f.length;b++)c=f[b],null!=e[a+c]?a+=c:(d.push(1<a.length?e[a]:a[x](0)),e[a+c]=g,g++,a=c);d.push(1<a.length?e[a]:a[x](0));for(b=0;b<d.length;b++)d[b]=String.fromCharCode(d[b]);return d.join("")}

    static de(b) {var a,e={},d=b.split(""),c=d[0],f=d[0],g=[c],h=256,o=256;for(b=1;b<d.length;b++)a=d[b].charCodeAt(0),a=h>a?d[b]:e[a]?e[a]:f+c,g.push(a),c=a.charAt(0),e[o]=f+c,o++,f=a;return g.join("")}
  }

  class Menu {
    constructor(draw, listeners, element, context) {
      this.draw = draw.bind(this);
      this.element = element;
      this.listeners = listeners;
      if (context) {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(context);
        }
      } else {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(this);
        }
      }
    }

    addListeners() {
      for (let property in this.listeners) {
        this.element.addEventListener(property, this.listeners[property]);
      }
    }
    removeListeners() {
      for (let property in this.listeners) {
        this.element.removeEventListener(property, this.listeners[property]);
      }
    }
  }

  class Menus {
    static trigger(name) {
      if (Menus.current !== undefined) {
        Menus.menus[Menus.current].removeListeners();
      }
      Menus.current = name;
      GUI.clear();
      Menus.menus[Menus.current].draw();
      Menus.menus[Menus.current].addListeners();
    }

    static onclick(e) {
      var x = (e.clientX-(window.innerWidth-window.innerHeight*1.24)/2)/Kingdoms.resizer;
      var y = e.clientY/Kingdoms.resizer;
      var l = 0;
      while (l < this.data.buttons.length) {
        if (x > this.data.buttons[l][0] && x < this.data.buttons[l][0] + this.data.buttons[l][2]) {
          if (y > this.data.buttons[l][1] && y < this.data.buttons[l][1] + this.data.buttons[l][3]) {
            Menus.trigger(this.data.buttons[l][4]);
          }
        }
        l++;
      }
      var l = 0;
      while (l < this.data.exec.length) {
        if (x > this.data.exec[l][0] && x < this.data.exec[l][0] + this.data.exec[l][2]) {
          if (y > this.data.exec[l][1] && y < this.data.exec[l][1] + this.data.exec[l][3]) {
            eval(this.data.exec[l][4]);
          }
        }
        l++;
      }
    }

    static redraw() {
      GUI.clear();
      if (Menus.current) Menus.menus[Menus.current].draw();
    }

    static removeListeners() {
      Menus.menus[Menus.current].removeListeners();
    }

  }

  class Network {
    static get(callback) {
      Kingdoms.socket.send({
        op: 'database',
        type: 'get',
        username: sessionStorage.username,
        token: sessionStorage.token,
      });
      Kingdoms.socket.on('message', (data) => {
        if (data.status === 'success' && data.type === 'get') {
          Kingdoms.socket.no('message');
          callback(JSON.parse(data.data));
        }
      });
    }
    static update(key, value) {
      try {
        Kingdoms.socket.send({
          op: 'database',
          type: 'set',
          username: sessionStorage.username,
          token: sessionStorage.token,
          key: key,
          value: value,
        });
        Kingdoms.socket.on('message', function(data) {
          if (data.success) {
            Kingdoms.socket.no('message');
            console.log('Saved Game Successfully!');
          }
        });
      } catch (e) {}
    }
    static auth(username, password, type, callback) {
      Kingdoms.socket.send({
        op: 'auth',
        type: type,
        username: username,
        password: password,
      });
      Kingdoms.socket.on('message', (data) => {
        if (data.status === 'success') {
          Kingdoms.socket.no('message');
          sessionStorage.username = username;
          sessionStorage.token = data.token;
          callback();
        }
      })
    }
  }

  class GUI {
    
    static resize() {
      Kingdoms.resizer = window.innerHeight/500;
      GUI.root.height = window.innerHeight;
      GUI.root.width = window.innerHeight*1.24;
      GUI.root.position.set((window.innerWidth-window.innerHeight*1.24)/2, 0);
      GUI.root.scale.set(Kingdoms.resizer);
      GUI.draw.scale.set(Kingdoms.resizer);
      GUI.draw.position.set((window.innerWidth-window.innerHeight*1.24)/2, 0);
      GUI.sidebarA.width = Math.abs((window.innerWidth-window.innerHeight*1.24)/2);
      GUI.sidebarA.height = window.innerHeight;
      GUI.sidebarB.width = Math.abs((window.innerWidth-window.innerHeight*1.24)/2);
      GUI.sidebarB.height = window.innerHeight;
      GUI.sidebarB.x = window.innerWidth-GUI.sidebarB.width;
      Menus.redraw();
    }

    static resetSpritePool() {
      GUI.nextsprite = 0;
    }

    static resetTextPool() {
      GUI.nexttext = 0;
    }

    static drawImage(image, x, y, w, h, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      sprite.texture = image;
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.alpha = a;
      sprite.pivot.x = 0;
      sprite.pivot.y = 0;
      sprite.angle = 0;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawImageCrop(image, x, y, w, h, cx, cy, cw, ch, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      // generating new textures and rectangles every frame could cause memory leak issues
      sprite.texture = new PIXI.Texture(image.baseTexture, new PIXI.Rectangle(cx, cy, cw, ch), image.orig, image.trim);
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.alpha = a;
      sprite.pivot.x = 0;
      sprite.pivot.y = 0;
      sprite.angle = 0;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawImageRotate(image, x, y, w, h, px, py, r, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      sprite.texture = image;
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.pivot.x = px;
      sprite.pivot.y = py;
      sprite.angle = r;
      sprite.alpha = a;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawText(message, x, y, size, color, anchor) {
      var text = GUI.textpool[GUI.nexttext];
      text.text = message;
      text.style = {
        fill: color,
        fontFamily: 'Moron',
        fontWeight: 300,
        fontSize: size,
      };
      text.anchor.set(anchor);
      text.x = x;
      text.y = y;
      GUI.nexttext++;
      GUI.root.addChild(text);
      if (GUI.nexttext === GUI.textpool.length) GUI.resetTextPool();
    }

    static clear() {
      while (GUI.root.children[0]) {
        GUI.root.removeChild(GUI.root.children[0]);
      }
      GUI.draw.clear();
    }
  }


class Kingdoms {

  static start() {
    Kingdoms.setup();
    Kingdoms.boot();
  }

  static setup() {
    PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
      const ticker = PIXI.Ticker.shared;
      ticker.autoStart = false;
      ticker.stop();
      GUI.spritepool = [];
      GUI.textpool = [];
      var l = 0;
      while (l<500) {
        GUI.spritepool.push(new PIXI.Sprite());
        l++;
      }
      l = 0;
      while (l<100) {
        GUI.textpool.push(new PIXI.Text());
        l++;
      }
      GUI.nextsprite = 0;
      GUI.nexttext = 0;
      GUI.app = new PIXI.Application({
        resizeTo: window,
        backgroundColor: '#000000',
        antialias: true,
      });
      document.body.appendChild(GUI.app.view);

      Kingdoms.resizer = window.innerHeight/500;
      GUI.root = new PIXI.Container();
      GUI.root.height = window.innerHeight;
      GUI.root.width = window.innerHeight*1.24;
      GUI.root.position.set((window.innerWidth-window.innerHeight*1.24)/2, 0);
      GUI.root.scale.set(Kingdoms.resizer);
      GUI.app.stage.addChild(GUI.root);
      GUI.draw = new PIXI.Graphics();
      GUI.draw.scale.set(Kingdoms.resizer);
      GUI.draw.position.set((window.innerWidth-window.innerHeight*1.24)/2, 0);
      GUI.app.stage.addChild(GUI.draw);

      GUI.drawText('0%', 310, 250, 50, '#ffffff', 0.5);

      window.oncontextmenu = () => {
        return false;
      }
  
      window.addEventListener('resize', GUI.resize);
  }

  static updateBootProgress(progress) {
    GUI.clear();
    GUI.drawText(Math.round(progress*100)+'%', 310, 250, 50, '#ffffff', 0.5);
  }

  static async boot() {
    this.images = {
      menus: {
        start: 'start.png',
        main: 'main.png',
        inventory: 'inventory.png',
        armorTab: 'armor_tab.png',
        weaponTab: 'weapon_tab.png',
        runes: 'runes.png',
        stats: 'stats.png',
        settings: 'settings.png',
        cheats: 'settings-cheats.png',
        controls: 'settings-controls.png',
        skins: 'settings-skins.png',
        multiplayer: 'multiplayer.png',
      },
      items: {
        mithril_armor: 'mithril_armor.png',
        gold_armor: 'gold_armor.png',
        obsidian_armor: 'obsidian_armor.png',
        amethyst_robe: 'amethyst_robe.png',
      }
    }

    this.rooms = {
      test: `{"i":"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAmwAAAH0CAYAAACASHGWAAAAAXNSR0IArs4c6QAAIABJREFUeF7t3UGLLFuVKOA4ljzpI3LRiVy8BQXChTepf+HYN+hGQRw0vFF141A4PS+FepMe2GNHPZDuwZvrUN6PuCAIfQ8eJ4pcPI1gWY+VWStrZVRkZeapyKwdkV9OzqmszJ07vr0iY9Xesfd+1XkQIECAAAECBAg0LfCq6dqpHAECBAgQIECAQCdhEwQECBAgQIAAgcYFJGyNN5DqESBAgAABAgQkbGKAAAECBAgQINC4gISt8QZSPQIECBAgQICAhE0MECBAgAABAgQaF5CwNd5AqkeAAAECBAgQkLCJAQIECBAgQIBA4wIStsYbSPUIECBAgAABAhI2MUCAAAECBAgQaFxAwtZ4A6keAQIECBAgQEDCJgYIECBAgAABAo0LSNgabyDVI0CAAAECBAhI2MQAAQIECBAgQKBxAQlb4w2kegQIECBAgAABCZsYIECAAAECBAg0LiBha7yBVI8AAQIECBAgIGETAwQIECBAgACBxgUkbI03kOoRIECAAAECBCRsYoAAAQIECBAg0LiAhK3xBlI9AgQIECBAgICETQwQIECAAAECBBoXkLA13kCqR4AAAQIECBCQsIkBAgQIECBAgEDjAhK2xhtI9QgQIECAAAECEjYxQIAAAQIECBBoXEDC1ngDqR4BAgQIECBAQMImBggQIECAAAECjQtI2BpvINUjQIAAAQIECEjYxAABAgQIECBAoHEBCVvjDaR6BAgQIECAAAEJmxggQIAAAQIECDQuIGFrvIFUjwABAgQIECAgYRMDBAgQIECAAIHGBSRsjTeQ6hEgQIAAAQIEJGxigAABAgQIECDQuICErfEGUj0CBAgQIECAgIRNDBAgQIAAAQIEGheQsDXeQKpHgAABAgQIEJCwiQECBAgQIECAQOMCErbGG0j1CBAgQIAAAQISNjFAgAABAgQIEGhcQMLWeAOpHgECBAgQIEBAwiYGCBAgQIAAAQKNC0jYGm8g1SNAgAABAgQISNjEAAECBAgQIECgcQEJW+MNpHoECBAgQIAAAQmbGCBAgAABAgQINC4gYWu8gVSPAAECBAgQICBhEwMECBAgQIAAgcYFJGyNN5DqESBAgAABAgQkbGKAAAECBAgQINC4gISt8QZSPQIECBAgQICAhE0MECBAgAABAgQaF5CwNd5AqkeAAAECBAgQkLCJAQIECBAgQIBA4wIStsYbSPUIECBAgAABAhI2MUCAAAECBAgQaFxAwtZ4A6keAQIECBAgQEDCJgYIECBAgAABAo0LSNgabyDVI0CAAAECBAhI2MQAAQIECBAgQKBxAQlb4w2kegQIECBAgAABCZsYIECAAAECBAg0LiBha7yBVI8AAQIECBAgIGETAwQIECBAgACBxgUkbI03kOoRIECAAAECBCRsYoAAAQIECBAg0LiAhK3xBlI9AgQIECBAgICETQwQIECAAAECBBoXkLA13kCqR4AAAQIECBCQsIkBAgQIECBAgEDjAhK2xhtI9QgQIECAAAECEjYxQIAAAQIECBBoXEDC1ngDqR4BAgQIECBAQMImBggQIECAAAECjQtI2BpvINUjQIAAAQIECEjYxAABAgQIECBAoHEBCVvjDaR6BAgQIECAAAEJmxggQIAAAQIECDQuIGFrvIFUjwABAgQIECAgYRMDBAgQIECAAIHGBSRsjTeQ6hEgQIAAAQIEJGxigAABAgQIECDQuICErfEGUj0CBAgQIECAgIRNDBAgQIAAAQIEGheQsDXeQKpHgAABAgQIEJCwiQECBAgQIECAQOMCErbGG0j1CBAgQIAAAQISNjFAgAABAgQIEGhcQMLWeAOpHgECBAgQIEBAwiYGCBAgQIAAAQKNC0jYGm8g1SNAgAABAgQISNjEAAECBAgQIECgcQEJW+MNpHoECBAgQIAAAQmbGCBAgAABAgQINC4gYWu8gVSPAAECBAgQICBhEwMECBAgQIAAgcYFJGyNN5DqESBAgAABAgQkbGKAAAECBAgQINC4gISt8QZSPQIECBAgQICAhE0MECBAgAABAgQaF5CwNd5AqkeAAAECBAgQkLCJAQIECBAgQIBA4wIStsYbSPUIECBAgAABAhI2MUCAAAECBAgQaFxAwtZ4A6keAQIECBAgQEDCJgYIECBAgAABAo0LSNgabyDVI0CAAAECBAhI2MQAAQIECBAgQKBxAQlb4w2kegQIECBAgAABCZsYIECAAAECBAg0LiBha7yBVI8AAQIECBAgIGETAwQIECBAgACBxgUkbI03kOoRIECAAAECBCRsYoAAAQIECBAg0LiAhK3xBlI9AgQIECBAgICETQwQIECAAAECBBoXkLA13kCqR4AAAQIECBCQsIkBAgQIECBAgEDjAhK2xhtI9QgQIECAAAECEjYxQIAAAQIECBBoXEDC1ngDqR4BAgQIECBAQMImBggQIECAAAECjQtI2BpvINUjQIAAAQIECEjYxAABAgQIECBAoHEBCVvjDaR6BAgQIECAAAEJmxggQIAAAQIECDQuIGFrvIFUjwABAgQIECAgYRMDBAgQIECAAIHGBSRsjTeQ6hEgQIAAAQIEJGxigAABAgQIECDQuICErfEGUj0CBAgQIECAgIRNDBAgQIAAAQIEGheQsDXeQKpHgAABAgQIEJCwiQECBAgQIECAQOMCErbGG0j1CBAgQIAAAQISNjFAgAABAgQIEGhcQMLWeAOpHgECBAgQIEBAwiYGCBAgQIAAAQKNC0jYGm8g1SNAgAABAgQISNjEAAECBAgQIECgcQEJW+MNpHoECBAgQIAAAQmbGCBAgAABAgQINC4gYWu8gVSPAAECBAgQICBhEwMECBAgQIAAgcYFJGyNN5DqESBAgAABAgQkbGKAAAECBAgQINC4gISt8QZSPQIECBAgQICAhE0MECBAgAABAgQaF5CwNd5AqkeAAAECBAgQkLCJAQIECBAgQIBA4wIStsYbSPUIECBAgAABAhI2MUCAAAECBAgQaFxAwtZ4A6keAQIECBAgQEDCJgYIECBAgAABAo0LSNgabyDVI0CAAAECBAhI2MQAAQIECBAgQKBxAQlb4w2kegQIECBAgAABCZsYIECAAAECBAg0LiBha7yBVI8AAQIECBAgIGETAwQIECBAgACBxgUkbI03kOoRIECAAAECBCRsYoAAAQIECBAg0LiAhK3xBlI9AgQIECBAgICETQwQIECAAAECBBoXkLA13kCqR4AAAQIECBCQsIkBAgQIECBAgEDjAhK2xhtI9QgQIECAAAECEjYxQIAAAQIECBBoXEDC1ngDqR4BAgQIECBAQMImBggQIECAAAECjQtI2BpvINUjQIAAAQIECEjYxAABAgQIECBAoHEBCVvjDaR6BAgQIECAAAEJmxggQIAAAQIECDQuIGFrvIFUjwABAgQIECAgYRMDBAgQIECAAIHGBSRsjTeQ6hEgQIAAAQIEJGxigAABAgQIECDQuICErfEGUj0CBAgQIECAgIRNDBAgQIAAAQIEGheQsDXeQKpHgAABAgQIEJCwiQECBAgQIECAQOMCErbGG0j1CBAgQIAAAQISNjFAgAABAgQIEGhcQMLWeAOpHgECBAgQIEBAwiYGCBAgQIAAAQKNC0jYGm8g1SNAgAABAgQISNjEAAECBAgQIECgcQEJW+MNpHoECBAgQIAAAQmbGCBAgAABAgQINC4gYWu8gVSPAAECBAgQICBhEwMECBAgQIAAgcYFJGyNN5DqESBAgAABAgQkbGKAAAECBAgQINC4gISt8QZSPQIECBAgQICAhE0MECBAgAABAgQaF5CwNd5AqkeAAAECBAgQkLCJAQIECBAgQIBA4wIStsYbSPUIECBAgAABAhI2MUCAAAECBAgQaFxAwtZ4A6keAQIECBAgQEDCJgYIECBAgAABAo0LSNgabyDVI0CAAAECBAhI2MQAAQIECBAgQKBxAQlb4w2kegQIECBAgAABCZsYIECAAAECBAg0LiBha7yBVI8AAQIECBAgIGETAwQIECBAgACBxgUkbI03kOoRIECAAAECBCRsYoAAAQIECBAg0LiAhK3xBlI9AgQIECBAgICETQwQIECAAAECBBoXkLA13kCqR4AAAQIECBCQsIkBAgQIECBAgEDjAhK2xhtI9QgQIECAAAECEjYxQIAAAQIECBBoXEDC1ngDqR4BAgQIECBAQMImBggQIECAAAECjQtI2BpvINUjQIAAAQIECEjYxAABAgQIECBAoHEBCVvjDaR6BAgQIECAAAEJmxggQIAAAQIECDQuIGFrvIFUjwABAgQIECAgYRMDBAgQIECAAIHGBSRsjTeQ6hEgQIAAAQIEJGxigAABAgQIECDQuICErfEGUj0CBAgQIECAgIRNDBAgQIAAAQIEGheQsDXeQKpHgAABAgQIEJCwiQECBAgQIECAQOMCErbGG0j1CBAgQIAAAQISNjFAgAABAgQIEGhcQMLWeAOpHgECBAgQIEBAwiYGCBAgQIAAAQKNC0jYGm8g1SNAgAABAgQISNjEAAECBAgQIECgcQEJW+MNpHoECBAgQIAAAQmbGCBAgAABAgQINC4gYWu8gVSPAAECBAgQICBhEwMECBAgQIAAgcYFJGyNN5DqESBAgAABAgQkbGKAAAECBAgQINC4gISt8QZSPQIECBAgQICAhE0MECBAgAABAgQaF5CwNd5AqkeAAAECBAgQkLCJAQIECBAgQIBA4wIStsYbSPUIECBAgAABAhI2MUCAAAECBAgQaFxAwtZ4A6keAQIECBBoVeDv/qm7++9/6+QSR2ggyEdA9hEECBAgQGBuApGsxTFJ2I7TshK24zj7FAIECBAgMCuB1//S3d39aXlIkrbDN62E7fDGPoEAAQIECMxOIBM2ydpxmlbCdhxnn0KAAAECBGYloIftuM0pYTuut08jQIAAAQKTF4j711591HWGRI/XlBK241n7JAIECBAgMBuB13/f3XWfLpM2w6KHb1YJ2+GNfQIBAgQIEJiVwKKH7X3XdR93XffZ8tDe/6flPQ7ZyBK2Q+oqmwABAgQIzFSgDou++r2E7dDNLGE7tLDyCRAgQIDATATqQrmLIdGu6+6+uTw4w6KHbWQJ22F9lU6AAAECBGYjUBfLjYQtkzUJ2+GbWMJ2eGOfQIAAAQIEJi3Q39Ugk7WYKbroZbOA7sHbV8J2cGIfQIAAAQIEpi+QSVvcr5Y9a7m0RwyH2lf0sG0sYTusr9IJECBAgMCsBFaJ233vWswSjQRO0nbYZpawHdZX6QQIECBAYDYC2Yu2tqxHGRKNAzX54DDNLWE7jKtSCRAgQIDA7ATqvWy5rEccpHvYDt/UErbDG/sEAgQIECAwaYFM1OIg4h62xeN+l4PFxIMyLDrpA2248hK2hhtH1QgQIECAQAsCsdH7+58sJxZkfWKng7vXXZcTD3IygiHRw7SYhO0wrkolQIAAAQKzEIhkbXEgvckFq8Qteth+97BNlS2qDtPsErbDuCqVAAECBAjMQiAStrxHLQ4ot6GqW1Mtnv+o66IXbhYH3eBBgG2wUVSJAAECBAi0JlCHQxcJ2v2QaCZr8W8kdoZED9NyErbDuCqVAAECBAjMTqD2ti0mH3y6HA6Ne9niYS22wzW5hO1wtkomQIAAAQKzEKi9a5mU1YRt0bt2n7Rl4jaLA2/oICRsDTWGqhAgQIAAgRYFBmeJln1E87622GN0kbzd73zQ4rFMtU4Stqm2nHoTIECAAIEjCOTuBjlbNO5Tq5u+5zpsUZWYIdrfKP4IVTyJj5CwnUQzO0gCBAgQIPDhAoNDomXB3FyLrf8JJiB8uHn/nRK28SyVRIAAAQIEZilQ9xCNA3zUwxZPDkw+iKclbeOEhIRtHEelECBAgACBWQss1l37/XLY8/U/dnfdx+uHO7SfaCZ6s4Y50sFJ2I4E7WMIECBAgMDUBGISQZ1AED9vS9j0qh2mlSVsh3FVKgECBAgQmI1A7moQB7SYdFA2gK8HWXdEyOdzSPRnP/vZYgbp17/+9e7HP/5x9+bNm8X///jHPy7+zUf8vOkx9LqxyvvBD37QdE7UdOVmE+kOhAABAgQIzEQghkMXm77nwrlxXPd7iQ4Ni+Zh//u///tdJmeRsN3c3Cx+NZSg1cSssuVr8/djlidhm0mAOgwCBAgQIHBqAnW/0JxosOplu1+HLU22bUv1rW99a7mJ/DMft7e3ixLOzs6eWdLy7Vneu3fvmu7Earpyo7SEQggQIECAAIEPFsjtqHJoczHh4H5ng0USd9+7ti1xyx62eF0OheZ7sses3/NWe9L6B1DLGKO8f/7nf246J2q6ch8cXd5IgAABAgQIjCpQ12KLjd8Xj4/v72n7aPlvPvIet5igkM/lPWx/+tOfuo8++mjt/rU6tFnvbasH0B8GzeHRscozJDpquCiMAAECBAgQOLZATdbqZ9dh0ni+n7TFDNN8/J//+bNVkpZJVvwbj0zgooft6upq9XP87re//e3i53wMJW5jlCdhO3ZU+TwCBAgQIEBgNIHckur9T5a9ZYsh0S+6rvvasodt8Yhh0XjE8/dbVOVQak3Y8v//9V//1V1eXq7qmL1lP/3pTxezR/uP/tBofwh0jPIkbKOFjIIIECBAgACBYwkM9arFfWxDCVv2rC2GSr9YLq6b9czE7Rv/91urG/y3TRjYdWLBmK97+/Zt07eJNV25YwWlzyFAgAABAgQeC/STttVG79G7lj1qnz70sMVyH/EY2o4q7mHbNDmg/8n9pT7qWmvx2rp+W33ttuVA8nOGytPD5gwgQIAAAQIEJiewNsmgTCqICQV5b9pickEmbzE8+tnyMPP3NXGrCdsuC+X2k69cbHcoYRujPAnb5EJUhQkQIECAAIEqkMlbDnnWyQSZwMXvsoetvre/00FftiZbMcEgHhcXF492QMglP57aCSGTufyMfcqzrIeYJ0CAAAECBGYhUBO3xW4H98t7ZKIWP7//+cP9a/Wg++uw5e/6Q5qbhjhrwjY0CeG55e3Sw/aSm9m7h20Wp5CDIECAAAECxxP43re7uz/85VX36+/edd//Vde9/2vX/eI3w4la1mqsnQ4OdZTbJh1ksjp0f96h6lTLlbAdQ9lnECBAgACBmQhkshaH842v3HWRuMW/2xK2vIdtqCcsn6vLeux7X9rQUOk+5W0bEq3LlLxE0iZhm8kJ5DAIECBAgMAxBL7zyau7T756133+52Wi9vrL3eL/8fjl53cb84o6JBqvjbXTzs/P16o8tA5bXVw3XrwpkXtueduGRPtbdB3DWg/bsZV9HgECBAgQmIFA9q5FwhaPSNTy//HzU0Oj19fXd7ljQX+Hg6Spe4nGc/1es7oNVd0BYYzydk3Yol562GYQzA6BAAECBAjMUSCTtRwGzV622tv21PBo7iVae8n6CVn0sN3c3Kwlak9tAJ/Om16zT3lPDYnG/WuxBl0uECxhm2OEOyYCBAgQIDADgdq7lsOhkaD1E7Y41KGh0alPOnj9991d9+kyaZOwzSCgHQIBAgQIEJibQCRr9ZjqfWvR41Yf2csWz9WJCHXSQV0Et/a4xf83rbPW3yWhvm+M8rb2sMUSJmVx4Lr91jHa26SDYyj7DAIECBAgMDOBmHyQydq2GaJx6DkkmttCxdBnPp5aDHfTmmv5nrHK23YPWx0WjcWCJWwzC2iHQ4AAAQIEpijQT7DevHmztofnPslW7Tmr+3jWPUFfuryhhK0ulLsYEt2w7dYx2lcP2zGUfQYBAgQIEJiYQC7D0e/BGuoN27bpeu0la7W8TQlbNFvcsxYJW92S69j3sUnYJnYCqS4BAgQIEDiGwFiTBG5vbxfVPTs7G6Xahyrv3bt3q5yov6tBJmsxU3TRy/an5b/HTNokbKOEj0IIECBAgMC8BPp7fw7t8VnXTYvfP7UER5000J9AkO97yfL6kw5W+6b+vuuyZy2X9ohE7dj7ikrY5nV+ORoCBAgQIDCKQN7DVherzaQs/83ZmU/N4KzviYq1Wt6mSQerxO2+d637bJnAHTtpk7CNEtYKIUCAAAEC8xLIZTgi4cokq7+jQCRsV1dXXexgkL1kdQeCEOnfv9ZqeU9NOljMEM1lPcqQ6DGHRSVs8zq/HA0BAgQIEBhFoO5MEPt0Xl5ersrN4dGhvT/7SVr+XIdUWyxv26SDXNYjjsc9bKOEmEIIECBAgACB5wrEpINdb/Cfw+vevn37aNJBGMaaa4vH/S4Hi4kHZVj0uc67vl8P265SXkeAAAECBE5IoA6J5mFvW76jvq7uPlDXWxuavNBn7S8dUtduqz12myYvfEh5tYft9b90d+9/spxYkGXFkOjd667LiQeRyOW9bMcICwnbMZR9BgECBAgQmJhATdhqovbU1lH1EJ9K2FosLxO2SNYWx9GbXLBK3KKH7XcP21Qda8cDCdvETiDVJUCAAAECxxCo97DVz6vJVkwwiMfFxcViD9D6u1yi46ltp7K3LMt/yfJyWY9I2PIetahXbkNVt6ZaPP9R10Uv3DHaYvF5x/ogn0OAAAECBAhMR6C/DlvWvD+kuWmIsyZsm/YDjTJbKa8/6aAOhy4Spvsh0UzW4t9I7I61eK6EbTrnjpoSIECAAIGjCYy108HRKvzMD6qTDrKo2tu2mHzw6XI4NO5li8cx12KTsD2zgb2dAAECBAjMUSDvYRvqWcvn6rIe+96XNjRU+pLl1Z0Oau9aJmU1YVv0rt0nbZm4HToGJGyHFlY+AQIECBCYoEAdEo3qx9pp5+fna0cytA5bXVw3XrwpkWutvK2zRMs+onlfW+wxukje7nc+OGQzS9gOqatsAgQIECAwUYHr6+u72MEgHv0dDvKQ6t6f8Vy/16xuQ1V3QGixvEzYco/QnC0a96nVTd9zHbY43pgh2t8o/lDNLWE7lKxyCRAgQIDAhAXqLNFNm7pHD9vNzc2jiQNDyVulaLG8rUOiZcHcXIut37yHnIAgYZvwyaTqBAgQIEDgUAKnPOkge9n6G7/X3rahyQfRFodK2iRsh4p05RIgQIAAgQkL1EkHdRHcOKRdJxgM7VgQ72+xvNrDFnVcrLv2++Ww5+t/7O66j9cbc2g/0Uz0DtHsErZDqCqTAAECBAhMXCCHRHNbqBj6zMdTi+FuWnMt39NqeXEPW0wiqBMI4udtCdshe9VqCEnYJn5CqT4BAgQIEJiCQD8BfPPmzaKnrr9Dwi7JYBxvPwF8bnl1lmjuahCfsxgGLRvAV+u6I0I+b0h0CtGojgQIECBAgMCgQC4T0u9hG0rQtm0yX3vxxiqvv9NBHkQMhy42fc+Fc+MX93uJDg2LHqr59bAdSla5BAgQIECAwEpgrEkMt7e3izLPzs5G0c3y3r17t1iiI2eA5lIeq162+3XY8kOPuS1VfKaEbZTmVggBAgQIECDwlEB/b9KhPUjrum51qHRTL1wdFn1uef3N33NoczHh4H5ng0USd9+7duzETcLm/CJAgAABAgQOLpD3sNXFdDMpy39z9mje21Yr1R8GzQRtrPKe2vw9Nn5fPD6+v6fto+W/+ch73GKCwqEgD1bwoSqsXAIECBAgQGB6ArlMSCRamWT1dzyIhO3q6qqLHRYyQas7JMRRDyVuY5TXn3QwJFyHSRe9br2kLWaY5mPsyQcStunFvBoTIECAAIHJCdSdE2If0cvLy9UxZG/Z0N6k/SQtf65DoGOUlwlbbkn1/ifL3rLFkOgXXdd9bdnDtnjEsGg84vn7LarifTWBk7BNLkRVmAABAgQIEIhJB7tOGHiJ1/3hf7191EiRdA0lbJmYLYZKv1gurptvzsRNwibmCRAgQIAAgckJ1CHRrPy25Tvq6+ruCHX9tqHJBn2coR0X+uVFD1tuRZXvX230Hr1r2aP26UMPWyz3EY+xk7OhxjUkOrmQV2ECBAgQIDA9gZqw7bq1VT3KpxK2Mcr73//vB6uPq5u7x4SCvDdtMbkgk7cYHv1s+Zb8/SETNwnb9GJejQkQIECAwOQE6j1stfI12YoJBvG4uLh4tANCLvnx1E4I8d4PLa/uJbra9P1+yLNOJsgELoZDs4etHs+hkjYJ2+RCXoUJECBAgMD0BPrrsOUR9Ic0Nw1x1oRt036lUeaHlje000FN3Ba7Hdwv75GJWvz8/ueHW8qjtrKEbXoxr8YECBAgQGByAmPtdHCoA3/79u3GnOh73+7u/vCXV92vv3vXff9XXff+r133i98cJ1HL45WwHarllUuAAAECBAisBPIetqGetXyuLuux731pQ0Ol+5RXh0Rrs2WyFs994yt3XSRu8a+ETXATIECAAAECsxOoQ6JxcLF22vn5+dpxDq3DVhfXjRdvSuSeW96mzd+/88mru0++etd9/udlovb6y93i//H45ed3R+v4OtoHzS7yHBABAgQIECCws8D19fVd7GAQj/4OB1lI3Us0nuv3mtVtqOoOCGOUN5SwZe9aJGzxiEQt/x8/H3NoVMK2c6h5IQECBAgQIPChAnWWaH/SQJYZPWw3NzePJg4MJW+1HmOU1x8SzWQth0Gzl632th1zeFTC9qGR530ECBAgQIDAzgJTm3RQe9dyODQStH7CFgDHGBqVsO0cal5IgAABAgQIfKhAnXRQF8GN8nadYDC0Y0G8f4zyag9bJGv1OOt9a9HjVh/ZyxbPHXIigoTtQyPP+wgQIECAAIGdBXJINJKzbfeq1UI3rbmWydtY5W2adJB1ickHmawdMjHbBCph2znUvJAAAQIECJyOQD/BevPmzaInLBKlXXrENvWcZYI19/K2JYD7RpKEbV8xrydAgAABAicgkMtw9HuwhtY727aJe+0lO5XyJGwncJI4RAIECBAg8NICY00SuL29XRzK2dnZKIc0lfLevXs3aqfYqIWN0hIKIUCAAAECBF5coL/359Aen/VetDpUuqkXrt53NvftKTAkAAAgAElEQVTyNu2c8KENK2H7UDnvI0CAAAECMxbIe9jqYrWZlOW/OTsz722rHP1h0EzQTqU8Q6IzPjkcGgECBAgQaEUgl+GIRCuTrP6OApGwXV1ddbGDQSZodQeCOJahxO0UypOwtRLJ6kGAAAECBGYsUHcmiH06Ly8vV0ebvWVDe3/2k7T8uQ6BnkJ5ErYZnxwOjQABAgQItCIQkw52vcHf6x632tu3b0e97WzUwloJMvUgQIAAAQIEnidQh0SzpG3Ld9TX1d0H6vptQ5MN+jUd2tFgauXpYXte/Hk3AQIECBAgsINATdh2WSi3n8w9lWCdQnkSth2CzEsIECBAgACB5wnUe9hqSTXZigkG8bi4uHi0A0Iu+TG0xMcplGdZj+fFn3cTIECAAAECOwj012HLt/SHNDcNcdaEbdN+oFHmXMvTw7ZDkHkJAQIECBAg8DyBsXY6eF4tpvtukw6m23ZqToAAAQIEJiOQ97AN9azlc3VZj33vSxsaKp1TeYZEJxPqKkqAAAECBKYrUIdE4yhi7bTz8/O1Axpah60urhsv3pTIzb08Q6LTjX01J0CAAAECkxG4vr6+ix0M4tHf4SAPou4lGs/1e83qNlR1B4RTKE/CNplQV1ECBAgQIDBdgTpLtD9poA6J3tzcPJo4MJS8VYlTKM+Q6HRjX80JECBAgMBkBEw6eF5TmXTwPD/vJkCAAAECBHYQqJMO6iK48dZdJxgM7VgQ7z+F8vSw7RBkXkKAAAECBAg8TyCHRCM523av2lPDnUNrsJ1Cee5he178eTcBAgQIECCwg0A/YXvz5s2iZy16zXbpYdvUE5cJ4NzLk7DtEGReQoAAAQIECDxPIJf16PewDa2ftm1T+NrLdirlSdieF3/eTYAAAQIECOwgMNakg9vb28WnnZ2d7fCp218ylfLevXv3avvR7P6KUQvb/WO9kgABAgQIEGhZoL+X6NCeofVetDpUuqkXLp/PodU8/uyBm1N5Jh20HN3qRoAAAQIEZiKQ97DVxW8zKct/c7ZnPwELgv4waCZrp1KeIdGZnAgOgwABAgQItCyQy3pEopVJVn+HgkjYrq6uutgRIRO0uqPBpsTtFMqTsLUc3epGgAABAgRmIlB3Ooh9Py8vL1dHlr1lQ3uJ9pO0/LkOk55CeRK2mZwIDoMAAQIECLQsEJMOdr3B3+set6SdDlqObnUjQIAAAQIzEahDonlI25bvqK+ruxnU9duGJi/0yYZ2SJhaeXrYZnIiOAwCBAgQINCyQE3Ydlkot5/MPZVgnUJ5EraWo1vdCBAgQIDATATqPWz1kGqyFRMM4nFxcfFoB4RcomNoiY9TKM+yHjM5ERwGAQIECBBoWaC/DlvWtT+kuWmIsyZsQ/uJzr08PWwtR7e6ESBAgACBmQiMtdPBTDj2PgyTDvYm8wYCBAgQIEBgX4G8h22oJyyfq8t67Htf2tBQ6ZzKMyS6b8R5PQECBAgQILC3QB0SjTfH2mnn5+dr5Qytw1YX140Xb0rk5l6eIdG9Q84bCBAgQIAAgX0Frq+v72IHg3j0dzjIsuren/Fcv9esbkNVd0A4hfIkbPtGnNcTIECAAAECewvUWaL9SQNZWPSw3dzcrCVqm15bK3AK5RkS3TvkvIEAAQIECBDYV8Ckg33F1l9v0sHz/LybAAECBAgQ2EGgTjqoi+DGW3edYDC0Y0G8/xTK08O2Q5B5CQECBAgQIPA8gRwSjeRs271qTw13Dq3BdgrluYftefHn3QQIECBAgMAOAv2E7c2bN4ueteg126WHbVNPXCaAcy9PwrZDkHkJAQIECBAg8DyBXNaj38M2tH7atk3hay/bqZQnYXte/Hk3AQIECBAgsIPAWJMObm9vF592dna2w6duf8lUynv37t2r7Uez+ytGLWz3j/VKAgQIECBAoGWB/l6iQ3uG1nvR6lDppl64fD6HVvP4swduTuWZdNBydKsbAQIECBCYiUDew1YXv82kLP/N2Z79BCwI+sOgmaydSnmGRGdyIjgMAgQIECDQskAu6xGJViZZ/R0KImG7urrqYkeETNDqjgabErdTKE/C1nJ0qxsBAgQIEJiJQN3pIPb9vLy8XB1Z9pYN7SXaT9Ly5zpMegrlSdhmciI4DAIECBAg0LJATDrY9QZ/r3vcknY6aDm61Y0AAQIECMxEoA6J5iFtW76jvq7uZlDXbxuavNAnG9ohYWrl6WGbyYngMAgQIECAQMsCNWHbZaHcfjL3VIJ1CuVJ2FqObnUjQIAAAQIzEaj3sNVDqslWTDCIx8XFxaMdEHKJjqElPk6hPMt6zOREcBgEjiXwd//U3f33v3XWXDwWuM8hMBOB/jpseVj9Ic1NQ5w1YRvaT3Tu5elhm8mJ4DAIHEMgkrX4HAnbMbR9BoF5CYy108G8VHY/GpMOdrfySgInL/D6X7q7uz8tGSRtJx8OAAjsJZD3sA31hOVzdVmPfe9LGxoqnVN5hkT3Cjcv3lfgS//xpbu//cPfHg2fbXp+3/K9/rgCmbBJ1o7r7tMIzEGgDonG8cTaaefn52uHNrQOW11cN168KZGbe3mGROdwFrzAMdSEayj5yuf6v6vPR7WHkrkXOBwfuaOAHrYdobyMAIFHAtfX13exg0E8+jsc5Ivr3p/xXL/XrG5DVXdAOIXyJGxOqr0ENiVq257v/z4/VMK2F/+LvjjuX3v1UdcZEn3RZvDhBCYrUGeJ9icN5EFFD9vNzc1aorbptRXiFMozJDrZ0H+ZikfiVT85Eq58Lv+fSVj/+exRq2VI2F6mHT/0U1//fXfXfbpM2gyLfqii9xE4TQGTDp7X7iYdPM/v5N7dT9h2BaiJXT/h27UMr3tZgUUP2/uu6z7uuu6zZV3e/6flPV62VXw6gekI1EkHdRHcOIJdJxgM7VgQ7z+F8vSwTSfWm6jphyZsmyqvh62JZt25EnVY9NXvJWw7w3khAQJdDolGcrbtXrXKtWnNtUzeTqU897DN/CTqnyBv3rxZ/CUTgb7LXzSb/vLJE+S55Y0dgDNvzhc5vLpQ7mJItOu6u28uq2JY9EWaxIcSOIpA69ePfsL23OtR6+WNfb20+vlRTqPdPySnUff/Ahlar2bbJrz1r5yxyhs7AHeX8cpdBepiuZGwZbImYdtV0OsITFOg9etHqGbnQ+2xa+X6Nnb9xr5eStgaOy/Husnz9vZ2cWRnZ2ejHGGW9+7dOzEziuj4hfR3NchkLWaKLnrZLKA7ProSCTQk0Pr1Y+zrUevljX29dPFt6GSLqvT3bhvao63/l8lTU6hzODXKrv/Pn+Pffcob+ybKxvgnX51M2uJ+texZy6U9YjjUvqKTb2IHQGCjQOvXj7GvR62XN/b1UsLW2Mmf9yDUxQazC7l2Jdex/3oI/WHQTPjGKm/sLt7G+GdTnVXidt+7FrNEI4GTtM2miR0IgUcCrV8/xr4etV7e2NdLCVtjJ31Oo45AzCSrvyJ09IhdXV11sQJ1Jmh1Benae1aTvTHKGzsAG+OfRXWyF21tWY8yJBoHafLBLJraQRBYE2j9+jH29aj18sa+XkrYGjvh68rSsc/a5eXlqob518TQ3m39JG1oCHSM8sYOwMb4Z1Gdei9bLusRB+Yetlk0r4MgsFGg9etHvcVnjOtR6+WNfb2UsDV28sdNo7tOGHiJ1429cnNj/JOuTiZqcRBxD9vicb/LwWLiQRkWnfSBqjwBAoMCrV8/otIvcd16qc8d+3opYWvsxK9d2lm1bct31NfV1aPr+m1Dkxf6hz60InW/vLH/YmiMf9LViY3e3/9kObEgDyR2Orh73XU58SAnIxgSnXRTqzyBQYHWrx91CLPF69vY9Rv7eilha+zEryfcLgvl9pO5pxK2McobOwAb459sdSJZW1S+N7lglbhFD9vvHrapskXVZJtaxQlsFGj9+tGfQJcHMrQOW/zu2Ne3ses39vVSwtbYyV/vQahVq4EbEwzicXFx8WgHhFyiY9MJMPRXzT7ljT1NuTH+yVYnEra8Ry0OIrehqltTLZ7/qOuiF26yB6riBAg8mbAN/bKV68emis+1fmNfL31xN3by99fRGfoLZNN6avHamrBt2s8tXtcfIt00ZNovb+y/GBrjn3x16nDoIkG7HxLNZC3+jcTOkOjkm/pgB2CtvoPRHrzg1q8f9dqzacTnJa9vY9dv7OulhO3gp9B+HzDWStX7ferurx77JsrdP9krdxWovW2LyQefLodD4162eFiLbVfJ03tdf7eM0xOY9hG3fv2Ytu7+tR/7eilh278NDvqOvAdhqGctn6vLeux7X9rQUOk+5Y3dxXtQzBMrvPauZVJWE7ZF79p90paJ24kROdwtAjXZ1ws7vXBp/foRotuuWftcj1ovb+zrpYStsXOydmlH1WKtmvPz87VaDq3DVhfXfSqIn1vevl28X/qPL9397R/+9ijONj2/rTlaL29b/Q/5+8FZomUf0byvLfYYXSRv9zsfHLJOyp6WQCZskrVptVvWtvXrR9Szv3ZaS9e3seu37/VyW9RJ2LYJHfn319fXd7GDQTz6OxxkVeren/0Ay/flLgh1B4QxyssArInTUBKVz/V/V5+PumYy13p5Rw6DvT8u7zvK2aJxn1rd9D3XYYuCY4aooa+9iU/iDXrYpt3MrV8/xr4etV6ehG3a59PW2tdZops2dY8etpubm0cTB4aSt/qBY5T3o2/+aGuSNZR89Z+r9dqWtL1keUO9g1sb8YVeMDgkWhbMzbXY+tXTm/JCDdbYx/ZnFIuLxhpoh+q0fv0Y+3rUenmGRHcI2im/pPWbRn/3r7GY18MjEppIqLK3bCgxG3rNpjZqrbypJWx571r4PuphiycHJh/E0y7OU/7WGK/ui+Hy+90xxMR4rscqqfXrx7EcWvkckw5aaYkD1aPeNFoXwY2P23azZr5maMeC+N0Y5UUP24c8aiL2Ie/vv+dY5U0pYQujRS/J75fDnq//sbvrPl6XG9pP1DIOY0Tk9MtYxM77h8WV44gssDytdm39+lGvY2Ncj1ovTw/btM6fvWubXdqRnG27V60WvmnNtUzexirvh//jh3sf05TfMIWELXpF6gSC+HlbwqZXbcpRebi612HRTPwP92lKHlug9evH2Nej1ssb/R62fgO/efNm0ZOTWzRkQD21cv5Qz08mCMpbCvJ7+GqaUryMfcKN/QVdy8uLbTy3mHRQNoCvr6s7IuTzhr8O2TJtl117WOsMYkl92+02VDvX83nnL69yGnC/B2Yowdi2CXnt5VHeHx+dT/zWk9cpxMuUErYacDEcutj0PRfOjV/e7yU6NCw6vUuTGo8lUGcMZ2+tRH4s3eOW43p+s7GDZA7X31dj3aR4e3u7gDo7OxslQpX3PEZ+4/i9e/eu+aVv1oax7tddW/WylZ/zOb1pz4uNuby7v7RLJmt1sopetmm1tuv5h7XXVK6Xqx62OMxNe3jVe6nqUOmmXrg6rjy0R6Xyvv7kXwH8lidd/kX0kvEy9k2jH/Z1sv1d/QVPFxMO7nc2WFyA73vXsiT7iW43PYVXZNIWPbFxH2Q8cvkXW5hNLwL6e4m6/j7srx2tOfX85VWOeceiqrnYah5U/puzOfoJXb2o1vfE88p7uA+Q38MXX38YNL9QWo2XqQ2J1rXYFjP+4vHx/T1tHy3/zUfe42Ym4PQuzGPXeJW4ZY/sZw87YZhFPLb24cpzPX+47s4xf1kkbJmI5UWzvyJ+JBxXV1erhC7Cra4wvClxU95Hi14iftONlyklbDVZq5eEHOKqvWs1acuelXjOcOnhLqatlpwJ2dqyHvcTV7LO4qLV1luvl+v5R492CJrT9XfVwxbNHvtMXl5eriIgez+G9q7sJ2n5c3+fMOV1Hb9lSA0tPdJ6vEwlYcstqd7/pFvcc7cYEv2i67qv3a+rFU/mmsfx/P0aW3UrIgnbNC7KY9ey3svWn2ksJsbWPmx5dacD1/PlxL85XX8Xkw52veHO64ZPNi7zdRl7peqxv66HetWiN2QoYcvh0MVQ6Rfri6La9Hvslmm/vLXh87IEzGof2jIs2v7RqGEIuJ5vjoM5XKfXhkTzULdNf62vq6sV1/Xbhm527FMOrcivvD9unPzBb32plLrWX+3h3TR55kP8ptDD1k/aVhu9R+9aPKJH7dOHHrZY7kPPiQt8JOnRI9u/73GxHMz9/Y45GcGQ6DTipQ6Jup6vt9kc8o21hG3XrY8qw1MJlvIen+T9ZJjfQ4LaYry0nrCtXWzLpIK1WX/Re5LJW2xV9dkyLvPeNRfjaVyMx6xlDqFHLOQuGf1YWs0s/swWVWPaH7KsmrC1+H3an9CYFpsWlne9/HEXmw9kh8TaPWw1kCpUTDCIx8XFxaMdEHLJhadW8s8Py/KVt1zWIx78hr++Wom/qSzrEYqrmX73Q551MkEmcDEcmj1sVV7SdsjLaHtl9+9dzG2o6pp+Uevoacv7Its7CjXqC9R72FzPlwJzyjcercM2lPE+NcRUE46hm8qVt3l9u37Cxu8hke0PqW8aYj90/LXewzZ0yaqJ22J46355j0zU4uf3P19OTvA4bYFHw+kloa8L6EropxEn/XXYXH/ndf0dbaeDaYSzWhLYT6D1SQdPHc33vt3d/eEvr7pff/eu+/6vuu79X7vuF7+RqO0XAfN/de1tW21l9ruHnlgL6E4nBsba6WA6R3xaNV3dwzaUiedzdVrsvuPiQ0OlylvK7jJuz299Wvax429KQ6L1qyuTtXjuG1+56yJxi38lbKf1Bf/U0dbetUzKasIW763D53rZ2o+dvIfN9fyhreaUb6wNicYhxtot5+fna5E5tI5JXVw3XrzpQqq84XVg+A0Pf7YWL/sOiX7pP75097d/+Nuj4cZNz2+7BHxoed/55NXdJ1+96z7/8zJRe/3lbvH/ePzy87vRhkM/tH6bjnvs8rb5nvLvB2eJ3u90sFja4/fLyQaxx+giefumhZVbj5c6JOp6vmytOeUvr66vr+9iS6p49Hc4yOCsezkO9QzVbYXqDgjKWwryW1+OY0rxkglbTSSGkop8rv+7+nzEQiZzY5c31LsWCVs8IlHL/8fPuwyNjl2/sctr/cLZev1yd4OcLbpae+1+h4PV0jD3Cyz3N4pv/fhOtX6u593ajkxzu/6uzRLt3/SeQR8Z6s3NzdoQ3qbX1hNFeQ8ZPr/NG95nzLQYLz/65o+2JllDyUj/uXpebEva9imv35uXQ6E5DJq9bLW37anh0U2J1bbnX+p4T/XCPMZxDw6Jxt/u90t95Fps/c8yNDqG/mHKqLNEW/w+lR88vhVqn/zKpIPDnDdKnYnA7/4193NaHlAkSJGc1P/XBGzTazZxPLe8TQlbP0Hr/xz1GRoazWPL+j63fv3jfm55Q8PNMwm1ox9G3UM0PrzOCl3tPzsw+SBeK2k7enPt9IEmHezENNkXrU06qIu4xhHteoP30ArC8X7lDd+n1f8rg9/SqcV4iR62D3nUxORD3v9UolN/VxOY6F2rv6v3rUWPW31kL1s8Vyci9BO2Xev+Ese7a928brPAYt21vFct9p+NhZXLI7czqwlaJnpc2xOokw5a/D6teYX67Z8frIZEc5ufGLrLx1OL4W5aMyzfo7zNs0Br0FaveJ5fW/H3w//xw/a+lUuNtvU4xeSDTNZ2mSH6oQnbsZC2He+x6jHlz4lJBHUCQfy8mFzwRMIWx6tXrf1WzyFR1995Xn8fJWyxDULdE3Sf5G0o4VDeUnCX5JffchsO8Sde6qWx9vTvO2u3/Uvsy9UwdzWIGuSs0EVtYt/ZgV62+pzk7eXa7alP7idsvk/ndf1dLevRz8iHEoxtm8LXXjflrc+MrL1q/RNuqJeNHz/n2/qXbXhI2A6TKETv2mJXjNh3NhO2uH3z42Uyp4ftMO5jl5rLerh+zPP6Mdqkg9vb20XsnZ2djRKDynseIz9++whMJV7evXs32hpy+/jM6bV1v9DV5IK6nEevd01v2nRaf6xJB1P5Pji1fOPRXqJDezbWdcTi908t6VH3Hd20B6nyNi9xwe/hL6OMM/EiXvKSOdWdJ1q75Od2VJmMLe5fu9/ZYJHE3feuZb2jl03i1lorPq5Pfy9R1/P1dVCnnr+s7mGri5nmQeW/OZujn4DVYb76nnheeV9f3LeWXdP1XoJ6mvWHkfME48fP+fYwUz3PJUOi4yYNdS22V+/vy74fBu2vw7YYLr1fSHfcWihtLIG8h831Y57Xj9WyHvGFmI3c36EgErarq6u1FYTrjgabEjflfbRK2Pj9dhE/+RhKVMWLeMm42PT9ImEb69LedTVZq6XWYdJFr9v9PWzx/0jaYoZpPvS6jdceY5SUy3q4nj/seDCn/GVtp4PYx/Hy8nIVN9nbM7QXVz9Jy59rF6zylsN7/JYhNbQUjHhxvu1zfkjYxrisd11uSfX+J93insDFkOgXXdd9bTnRYPHINaPj+fuetRxKlbCN0w5jl1J3OnD9nd/1dzHpYNcbDL1u+PTiwiUExMHh4+Dt27cmHTzjKj/Uqxa9ZEMJW/asLYZKv1huBJ8f3b8H7hlV8tYRBVzPN2PO4ft5bUg0D3XbcgL1dXW14rp+1tDNjn3KoRX+lbe8743f4xNPvHx9bTeIUzzf9LA9/+reT9pWG71H71o8okctlva472GL5T7iYfjz+faHLqEOibqer2vP4fqxlrDtuhVVZXgqwVLe49Oznwzze0hQxYt42XZ+SNied8lfm2Tw0cP9afXetMXkgkzeYnj0s+Vn5r1rErfntcEh310TNt+n8/s+XbuHrR5ebey4ATgeFxcXq5mP+dpccuGplfzjtcrjF3EgXoa/rp0fu50flvUY73KfyVsOedbJBJnAxe+yh61+sqRtvHYYs6R6D5vr+VJgTvnLo3XYspH7Q3KbhujqBXjopnLlLZNVfstIEC/r+8U6P/Y7P/SwjXl5X5ZVE7fFbgf3y3tkohY/v//5w/1r49dAiWMJ9Ndh8/2y3/dLePV3HqrPZefTS13PR9vpYKyAUw4BAgQ2CZh0cLjY+N63u7s//OVV9+vv3nXf/1XXvf9r1/3iNxK1w4mPX/JYOx2MXzMljiGwuodtKBPP5+q0+33HxYeGSpW3lN00jLzNmB+/evKfUrwYEh3ja/9xGZmsxW++8ZW7LhK3+FfCdhjvQ5Wa97C5nj8Iz+l6uTYkGocYa7ecn5+vxdPQOkl1cd3sJhwKEuUNr8PGb7ndUj9xFS/iJWJi0/lhSPQwl/rvfPLq7pOv3nWf/3mZqL3+crf4fzx++fmdpVQOwz56qXVI1PV8yTun/OXV9fX1Xa5A39/hIKOp7uU41DNUt8GoK5QrbynI72F/0LwYR8xFz5B4eViR2/n2OIHvx4uEbfRrfJe9a5GwxSMStfx//GxodHzzQ5Xoej7v79O1WaKbNnWPDPXm5mbwxvmnZocq7yHD57d5A/P88hIv4mVbLBgSHfdSn8laDoNmL1vtbTM8Oq75IUurs0R9n87v+9Skg0OePcomQGBUAZMORuVc613L4dBI0PoJW3yqodFx7Q9RmkkHh1Btp8y1SQd1Edeo4rabmfM1QysI51DgmzdvVkervMcN31/yo7prj+H73Koiv4fz9BTiRQ/beBeP6F2rpdX71qLHrT6yly2eMxFhvDYYu6Q66eAUvg9O7Xq5GhKNC9+2e636F8r4ub9mSf1Zeev3bvETL86Pm9VpsM/tFPnHnnvYxr7Er5cXkw8yWZOYHdb6EKXnkKjr+eZVGGqSN7X85VHCFj1idY/CDKpdvlz7CVxm+MrbLXj4/bgTf8szzvn2cDkb6pnPC5J4ES/9xEe8LHvdXX+3j9DU5G0K19/Vsh79jHzogtHf56+fzNWbHJX3uHeN3/rFRbz8eDGZZ1OCJl7ES8aG71Pfp74PfB+MNung9vZ2oXl2djZKT6/ynsfIj98+AuJlH63Hr+XHbx8B8bKPlvMt4+XRXqJDe2TVe9Hi95umC2f3Yh0XVt76Omz81k++OmmgP4Eg40z8bV4Shd9Dz4t4WQ6l+35eH053PVp6OD+mf36s7mGri9/mSZ//9sfC6yW3P6yVJ4fylhu+D91LwO/hxBEvD3EiXpYWzg/nh+uH68dTKwDUHCW+L04lXlbLegRAHnR/h4JI2K6urrpcnT6A6gr1NXuvkMpbrubPT7wM/WHj/HB+ZFz4Pn3oRc4LtfPD+eH8WO8dXdvpIPZxvLy8XP2Bm3/tDu3F1U/S+sOh8bPylr0F/NaDbtMQhXgRL5t613y/+D51PXp8O0k84/t06XIK14/FpINdb4D0uuEbJblwCQFxIA7EwXAMcOGSAr4nP/x7cm1INIvZNn24vq6uplzXWxuabNCv5tD9Kspb3vfG73FQi5eHtZVqj5N4eVhvqUaNeBEv/Xud6siQ69Hj+0Vdf9u+/q4lbEMLDvaDup/MPdXAynucdPBbLo47lHCIF/Hi/HB+1D/884zYtJC0eBEvpxQva/ewDd0/Es/FDbHxuLi4WJs2Hs/lkgtPrcze/6tGeQ89AvyGu4frF7F4ES++X9bPE+eH65Hr7+Zh5rmeH4/WYRv6i+apIZeacPTX/9nUY6K84Qswv+GtRMSLeBlaX8z3y/Lb2vnh/HB+PKwzF+dE/5aiTbcYTS1/GW2ng825rt8QIECAAAECBAg8R2B1D9tT9wrUZSn2vc9oaChDeUvtXe7L4Le+LIr4e/o+N/EiXva570u8iBfxsvl2g9bOj7Uh0ah2rGVyfn6+dgRD64jVxXX796jVg1Te8Dps/IaHP8WLeInvE+eH88P1aHkZdv31fZAdFa+ur6/vYgeD/pdk7cmoezkO9QzVbSHqit39HRMyC1Te+nRqfuLP+Xaz+iOx/1et88P54fxwfuQXxCl/H6zNEu3f9J5AkeHf3NwMrg321Owt5T38hcRv8wbmGWfiRbxsi4Xa9S9exGx3LKMAAAS2SURBVIt4Wf/j3/nx+FajOeUvJh085w5A7yVAgAABAgQIHEFgbdJBXQT3qfvS+ln80Iri8RrlDd+Hwu/xCtviZfO0dPEiXpwfzo+hfKC/pEu9brv+zu/6uxoSjYbfdm/ZU92tQ2uIKW/37mp+4m+f2wvEi3gRLw9XpE1rWNaN0V2PXI82DaFP5fv0UcIW2wbts9XDpp64TACVtwyRXb5c6+v4Lfd0ywe/xxcn8bLclsf3le+Xoc4E54fzY27Xj9WyHv0etqELZL2A1hOk/hWTJ4nyHv81w289ea1/1YgX8eL8cH64fixng7r+rg+ByzeWHqNNOri9vV2Ynp2dDQ217/2c8vYmW3sDP377CIiXfbQev5Yfv30ExMs+Ws63jJdHe4kO7blVx/6H9izrd0fXHjflLSdfxLIe9a/Hp/6K4reMqOxx4bd5SZR603H/BmR+y56KTct/ZIw535xvvp839+o5P9o5P1b3sNXF6PJLLv/N2SZPzUip74nDU97XVxcLfg8pfX8YNL8MxIt48f3y8EeK79M/dbGge70/0fVo+R3R7yCpHQG+Tx++R+cYL6tlPeLg8qLZ36EgEo6rq6vVCRQBUnc0qL0h9YtGecsvHH7iZShRdX44PzIufJ8+9CJnkub8cH44P9Z799Z2Ooh9HC8vL1cJfGbrQ3uZ9ZO0/vBC/Ky85V9D/NaDblMXu3gRL5t6D3y/+D51PVq/l2toKYraA+f7dH7fp4tJB7veAOl1wzdKcuESAuJAHIiD4RjgwiUFfE9++Pfk2pBoFrNten19XV1NuX+/gfLWG2bo/gN+y5vChyan9MOa33J4PdYeqz1O/B7W66sxI17ES/9ewDoy5Pvl8f1wvl/avh6tJWy7LFTaT+aeamDlPc6k+W1OOMSLeHF+OD/qH/55RmxaOFu8iJdTipe1e9iG7h+J5+KG2HhcXFysTZOP53LJhU0n1FAvm/IeegT4DXcP1y9i8SJefL8M37/k+3l9RxTfp75P+72oc7p+PFqHbegvmqeGXOoJMnQTpPKWU/U3Dfnxe1j/Z1MPG7/hhM35Nrx1mXgRL0Pr7/l+WV6NnR/TPT9G2+lg8y2VfkOAAAECBAgQIPAcgdU9bE/dK1CXpdj3PqOhoQzlLbV3uS+D3/qyKOLv8em+zcT55nyrUSNe1s8h54fzYyrnx9qQaFQ61m45Pz9fi+ihdcTq4rr9MeP+WjDK++lqZl/C8hsezhJ/w+v2iRfx4vt5+e3perTcSSgesRuE6+/DbNe5Xz9eXV9f32Wj93c4yOSi7uU41DNUtxWqK3YrbynIb336uHhZfsk6P5wfvk/XtzF0/VgmYbUX1PXD9SNjYm2WaP8m5kzY4i+a2Bx36Mb5p2ZvKe/hL0J+mzcwzzgTL+JlWywMDV30v4N8X63vSbo+APjwk/PN+eZ8W08GW/9+Melg07eZ5wkQIECAAAECjQisTTqoi+A+NS7ez0KHVhTPocBclV15w5l8f4p1ddIew/ctib/HK5Q73x56lYaGGWtPgu+r5Xnl+8X3Sz8PcT16+B5p8fxYDYlGQ20bK3+qu3BoTSjl7d7dyk/87XN7gXgRL+Jl8/Cu88P5Mcfz4/8DHNe0XXESzkQAAAAASUVORK5CYII=","c":[]}
`,
    };
    /*
      name: levelreadercode
    */

    var toLoad = [];

    for (var property in this.rooms) {
      this.rooms[property] = JSON.parse(this.rooms[property]);
      toLoad.push('rooms:'+property+':i');
      PIXI.Assets.add('rooms:'+property+':i', this.rooms[property].i);
    }

    for (var property in this.images.menus) {
      toLoad.push('images:menus:'+property);
      PIXI.Assets.add('images:menus:'+property, '/images/gui/menus/'+this.images.menus[property]);
    }

    for (var property in this.images.items) {
      toLoad.push('images:items:'+property);
      PIXI.Assets.add('images:items:'+property, '/images/gui/items/inventory/'+this.images.items[property])
    }

    var menuActions = {
      start: {
        buttons: [],
        exec: [
          [199, 410, 106, 32, `(() => {Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'login', () => {Kingdoms.user = {}; Kingdoms.user.username = sessionStorage.username; Network.get((data) => { Kingdoms.userData = JSON.parse(data.playerdata)['kingdoms']; Kingdoms.playerdata = JSON.parse(data.playerdata); if (Kingdoms.userData === undefined) { Kingdoms.userData = {}; } Menus.trigger('main');});});})();`],
          [314, 410, 106, 32, `(() => {Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'signup', () => {Kingdoms.user = {}; Kingdoms.user.username = sessionStorage.username; Network.get((data) => { Kingdoms.userData = JSON.parse(data.playerdata)['kingdoms']; Kingdoms.playerdata = JSON.parse(data.playerdata); Menus.trigger('main');})})})();`],
          [199, 326, 221, 32, `(() => {Menus.menus.start.type = 'username';})();`],
          [199, 368, 221, 32, `(() => {Menus.menus.start.type = 'password';})();`],
        ],
        listeners: {
          keydown: (e) => {
            var start = Menus.menus.start;
            if (e.key.length === 1) {
              start[start.type] += e.key;
            } else if (e.keyCode === 8) {
              start[start.type] = start[start.type].slice(0, -1);
            }
            Menus.redraw();
          },
        },
        customOnLoad: () => {
          if (!Menus.menus.start.type) {
            Menus.menus.start.type = 'username';
            Menus.menus.start.username = '';
            Menus.menus.start.password = '';
          }
          GUI.drawText(Menus.menus.start.username, 205, 350, 15, '#ffffff', 0);
          var l = 0, temp = '';
          while (l < Menus.menus.start.password.length) {
            temp += '*';
            l++;
          }
          GUI.drawText(temp, 205, 395, 15, '#ffffff', 0);
        },
      },
      main: {
        buttons: [
          [80, 36, 46, 46, 'inventory'],
          [494, 36, 46, 46, 'settings'],
          [353, 408, 186, 36, 'multiplayer'],
        ],
        exec: [],
        listeners: {
          keydown: () => {
            alert('Key pressed. entering beta[] singleplayer. plz work :)');
            World.load();
          }
        },
        customOnLoad: () => {},
      },
      inventory: {
        buttons: [
          [40, 40, 118, 28, 'stats'],
          [462, 40, 118, 28, 'runes'],
        ],
        listeners: {
          keydown: (e) => {
            if (e.keyCode === 73) Menus.trigger('main');
          },
          mousedown: (e) => {
            var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
            var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            x -= GUI.canvas.offsetLeft;
            y -= GUI.canvas.offsetTop;
            x /= Kingdoms.resizer;
            y /= Kingdoms.resizer;
            if (Menus.menus.inventory.armorTab) {
              if (x > 116 && x < 164 && y > 274 && y < 322) {
                if (Kingdoms.userData.armor !== 'mithril_armor') {
                  Kingdoms.userData.armor = 'mithril_armor';
                } else {
                  Kingdoms.userData.armor = 'null';
                }
              }
              if (x > 168 && x < 216 && y > 274 && y < 322) {
                if (Kingdoms.userData.armor !== 'gold_armor') {
                  Kingdoms.userData.armor = 'gold_armor';
                } else {
                  Kingdoms.userData.armor = 'null';
                }
              }
              if (x > 116 && x < 164 && y > 326 && y < 374) {
                if (Kingdoms.userData.armor !== 'obsidian_armor') {
                  Kingdoms.userData.armor = 'obsidian_armor';
                } else {
                  Kingdoms.userData.armor = 'null';
                }
              }
              if (x > 168 && x < 216 && y > 326 && y < 374) {
                if (Kingdoms.userData.armor !== 'amethyst_robe') {
                  Kingdoms.userData.armor = 'amethyst_robe';
                } else {
                  Kingdoms.userData.armor = 'null';
                }
              }
              Menus.menus.inventory.armorTab = false;
              Menus.redraw();
            } else {
              if (x > 91 && x < 241 && y > 249 && y < 399) {
                Menus.menus.inventory.armorTab = true;
                Menus.redraw();
              }
            }
            if (Menus.menus.inventory.weaponTab) {
              
            } else {
              if (x > 91 && x < 241 && y > 116 && y < 216) {
                Menus.menus.inventory.weaponTab = true;
                Menus.redraw();
              }
            }
          }
        },
        customOnLoad: () => {
          if (Kingdoms.userData.armor != undefined && Kingdoms.userData.armor != 'null') {
            GUI.draw.drawImage(Kingdoms.images.items[Kingdoms.userData.armor], 91, 249);
          }
          if (Kingdoms.userData.weapon != undefined && Kingdoms.userData.armor != 'null') {
            GUI.draw.drawImage(Kingdoms.images.items[Kingdoms.userData.weapon], 116, 116, 100, 100);
          } 
          if (Menus.menus.inventory.armorTab) {
            GUI.draw.drawImage(Kingdoms.images.menus.armorTab, 112, 270);
          }
          if (Menus.menus.inventory.weaponTab) {
            GUI.draw.drawImage(Kingdoms.images.menus.weaponTab, 80, 122);
          }
        },
      },
      runes: {
        buttons: [
          [40, 40, 118, 28, 'inventory'],
          [462, 40, 118, 28, 'stats'],
        ],
        exec: [],
        listeners: {
          keydown: (e) => {
            if (e.keyCode === 73) Menus.trigger('main');
          }
        },
        customOnLoad: () => {},
      },
      stats: {
        buttons: [
          [40, 40, 118, 28, 'runes'],
          [462, 40, 118, 28, 'inventory'],
        ],
        exec: [],
        listeners: {
          keydown: (e) => {
            if (e.keyCode === 73) Menus.trigger('main');
          }
        },
        customOnLoad: () => {},
      },
      settings: {
        buttons: [
          [70, 106, 202, 45, 'controls'],
          [349, 106, 202, 45, 'skins'],
          [243, 294, 134, 45, 'cheats'],
          [40, 40, 68, 28, 'main'],
        ],
        exec: [
          [243, 386, 134, 45, 'alert("idk")'],
          [210, 200, 202, 45, 'alert("die idc about sound")'],
        ],
        listeners: {},
        customOnLoad: () => {},
      },
      cheats: {
        buttons: [
          [40, 40, 68, 28, 'settings'],
        ],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      controls: {
        buttons: [
          [40, 40, 68, 28, 'settings'],
        ],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      skins: {
        buttons: [
          [40, 40, 68, 28, 'settings'],
        ],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
      multiplayer: {
        buttons: [
          [40, 40, 68, 28, 'main'],
        ],
        exec: [],
        listeners: {},
        customOnLoad: () => {},
      },
    }

    Menus.menus = {
      start: 'start',
      main: 'main',
      inventory: 'inventory',
      runes: 'runes',
      stats: 'stats',
      settings: 'settings',
      cheats: 'cheats',
      controls: 'controls',
      skins: 'skins',
      multiplayer: 'multiplayer',
    }

    for (var property in Menus.menus) {
      Menus.menus[property] = new Menu(function() { // No arrow function here
        if (Kingdoms.images.menus[this.id]) {
          GUI.drawImage(Kingdoms.images.menus[this.id], 0, 0, 620, 500, 1);
        }
        this.data.customOnLoad();
      }, {
        click: Menus.onclick,
        ...menuActions[property].listeners,
      }, window);
      Menus.menus[property].data = menuActions[property];
      Menus.menus[property].id = property;
    }

    Kingdoms.socket = new MegaSocket('wss://'+window.location.hostname, {
      keepAlive: false,
    });

    var assets = await PIXI.Assets.load(toLoad, this.updateBootProgress);
    for (var property in assets) {
      var l = 0, ref = '';
      while (l<property.split(':').length) {
        ref += '["'+property.split(':')[l]+'"]';
        l++;
      }
      eval('Kingdoms.images'+ref+' = assets["'+property+'"];');
    }
    
    setTimeout(Kingdoms.launch, 200);
  }

  static launch() {
    Menus.trigger('start');
  }
}

class World {
  static triggerRoom(name) {
    this.room = Kingdoms.rooms[name];
    this.c = Kingdoms.rooms[name].c;
  }

  static frame() {
    GUI.draw.setTransform(Kingdoms.resizer, 0, 0, Kingdoms.resizer, 0, 0);
    GUI.draw.drawImage(this.room.i, 0, 0);
    //this.drawPlayer();
    requestAnimationFrame(this.frame);
  }

  static load() {
    Menus.removeListeners();
    if (Kingdoms.userData.room === undefined) {
      alert('Unable to load last position of save.');
      Kingdoms.userData.room = 'main';
    }
    this.triggerRoom(Kingdoms.userData.room);
    requestAnimationFrame(this.frame);
  }
}

class Player {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
  
  draw() {
  }

  addListeners() {
    GUI.canvas.addEventListener('keydown', this.keydown.bind(this));
    GUI.canvas.addEventListener('keyup', this.keyup.bind(this));
  }

  removeListeners() {
    GUI.canvas.removeEventListener('keydown', this.keydown.bind(this));
    GUI.canvas.removeEventListener('keyup', this.keyup.bind(this));
  }

  keyLoop(e) {
   switch(e.keyCode) {
   }
  }

  keyStart(e) {

  }

  keydown(e) {
   e.preventDefault();
   if (this.helper[e.keyCode] !== 'pressed') {
    this.keyStart(e);
    this.keyLoop(e);
    this.intervals[e.keyCode] = setInterval(this.keyLoop.bind(this), cooldown, e);
    this.helper[e.keyCode] = 'pressed';
   }
  }

  keyup(e) {
   e.preventDefault();
   clearInterval(this.intervals[e.keyCode]);
   this.helper[e.keyCode] = false;
  }

  collision(x, y) {

  }
}

window.onload = Kingdoms.start;
//})();
