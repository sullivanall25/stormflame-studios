const SETTINGS = {
  use_existing_node_server: true, // Set this to false unless you have your own nodejs web server using port 80
  path: '/ffa-server', // Path you will need to connect to (e.g) example.com/ffa-server instead of example.com (use / for no path)
  bans: [],
  mutes: [],
  admins: ['cs641311', 'starwarsgeek'],
  max_players_per_room: 5, // Number of player per room before the server creates another one.
  log_strain: false, // recommended false unless you suffer from lag
  fps_boost: false, // Can lag clients and servers. Recommened false.
  UPS: 60, // Updates per second to be sent to clients (ONLY USED if fps_boost is set to false)
  filterProfanity: true, // filter profanity
}

import https from 'https';
import HyperExpress from 'hyper-express';
import jsonpack from 'jsonpack';
import Filter from 'bad-words';

const filter = new Filter();

const FFA_SERVER = new HyperExpress.Router();
var sockets = [];
var servers = [];
var incoming_per_second = 0;
var outgoing_per_second = 0;

if (SETTINGS.log_strain) {
  setInterval(() => {
    console.log('Incoming: ' + incoming_per_second + ' | Outgoing: ' + outgoing_per_second);
    incoming_per_second = 0;
    outgoing_per_second = 0;
  }, 1000);
}

setInterval(() => {
  var l = 0;
  while (l<SETTINGS.bans.length) {
    SETTINGS.bans[l].time -= 1;
    if (SETTINGS.bans[l].time <= 0) {
      SETTINGS.bans.splice(l, 1);
      l--;
    }
    l++;
  }
}, 60000);

FFA_SERVER.ws(SETTINGS.path, {
  idleTimeout: Infinity,
  max_backpressure: 1,
}, (socket) => {
  sockets.push(socket);
  socket.originalSend = socket.send;
  socket.send = function(data) {
    this.originalSend(Helper.en(jsonpack.pack(JSON.parse(data))));
  }.bind(socket);
  socket.on('message', (data) => {
    incoming_per_second++;
    data = jsonpack.unpack(Helper.de(data));
    if (!socket.username) {
      socket.username = data.username;
      var l = 0;
      while (l<SETTINGS.bans.length) {
        if (SETTINGS.bans[l].username === socket.username) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'You are banned. Banned by '+SETTINGS.bans[l].by+' for '+SETTINGS.bans[l].reason+'. You are banned for '+SETTINGS.bans[l].time+' more minutes or until an admin unbans you. You were banned by an admin on this server, not the entire game, so you can join other servers.',
          }));
          setTimeout(function() {
            socket.destroy();
          });
          return;
        }
        l++;
      }
    }
    if (data.type === 'join') {
      var l = 0;
      if (servers.length === 0) {
        servers.push(new FFA());
      }
      var l = 0;
      while (l < servers.length) {
        if (servers[l].pt.length !== 10) {
          socket.room = l;
          var q = 0;
          while (q < servers[l].pt.length) {
            if (socket.username === servers[l].pt[q].u) {
              socket.destroy();
              return;
            }
            q++;
          }
          https.get('https://pixeltanks.ml/verify?username='+socket.username+'&token='+data.token, function(res) {
            var d = [];
            res.on('data', function(chunk) {
              d.push(chunk);
            }.bind(this));
            res.on('end', function() {
              if (Buffer.concat(d).toString() === 'true') {
                this.add(socket, data.tank);
              } else {
                socket.send(JSON.stringify({
                  status: 'error',
                  message: 'Authentication failure. Your token('+data.token+') does not match with your username('+socket.username+'). This can be caused by the authentication servers restarting or by modifying the client. Simply log in again to fix this issue.',
                }))
                setTimeout(() => {
                  socket.destroy();
                }, 20);
              }
            }.bind(this));
          }.bind(servers[l]));
        }
        l++;
      }
      if (socket.room === undefined) {
        var temp = new FFA();
        socket.room = servers.length;
        temp.add(socket, data.tank);
        servers.push(temp);
      }
    } else if (data.type === 'update') {
      servers[socket.room].update(data);
    } else if (data.type === 'ping') {
      socket.send(JSON.stringify({
        event: 'ping',
        id: data.id,
      }));
    } else if (data.type === 'chat') {
      var l = 0;
      while (l < servers[socket.room].pt.length) {
        if (servers[socket.room].pt[l].u === socket.username) {
          var msg;
          try {
            msg = (SETTINGS.filterProfanity ? filter.clean(data.msg) : data.msg)
          } catch(e) {
            msg = data.msg
          }
          servers[socket.room].logs.push({
            m: '[' + socket.username + '] ' + msg,
            c: '#ffffff',
          });
        }
        servers[socket.room].pt[l].updates.logs = true;
        l++;
      }
    } else if (data.type === 'command') {
      var command = data.data[0];
      if (command === '/createteam') {
        if (data.data.length !== 2) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.',
          }));
          return;
        }
        var l = 0;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].t.split(':')[1].replace('@leader', '') === data.data[1]) {
            servers[socket.room].logs.push({
              m: socket.username+' attempted to create a team that already existed! SHAME THEM!!!!!!!',
              c: '#40C4FF',
            });
            return;
          }
          l++;
        }
        if (data.data[1].includes('@leader') || data.data[1].includes('@requestor#') || data.data[1].includes(':') || data.data[1].length > 20) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Team name not allowed.',
          }));
          return;
        }
        var l = 0;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].u === socket.username) {
            servers[socket.room].pt[l].t = socket.username+':'+data.data[1]+'@leader';
            servers[socket.room].logs.push({
              m: servers[socket.room].pt[l].u+' created team '+data.data[1]+', type /join '+data.data[1]+' to join.',
              c: '#40C4FF',
            });
            var q = 0;
            while (q<servers[socket.room].pt.length) {
              servers[socket.room].pt[q].updates.logs = true;
              q++;
            }
          }
          l++;
        }
      } else if (command === '/join') {
        if (data.data.length !== 2) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.',
          }));
          return;
        }
        var l = 0, exists = false;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].t.split(':')[1].replace('@leader', '') === data.data[1] && servers[socket.room].pt[l].t.includes('@leader')) {
            exists = true;
          }
          if (servers[socket.room].pt[l].u === socket.username && servers[socket.room].pt[l].t.includes('@leader')) return;
          l++;
        }
        if (!exists) return;
        var l = 0;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].u === socket.username) {
            servers[socket.room].pt[l].t += '@requestor#'+data.data[1];
            servers[socket.room].logs.push({
              m: socket.username+' wants to join team '+data.data[1],
              c: '#40C4FF',
            });
            var q = 0;
            while (q<servers[socket.room].pt.length) {
              servers[socket.room].pt[q].updates.logs = true;
              q++;
            }
          }
          l++;
        }
      } else if (command === '/accept') {
        if (data.data.length !== 2) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.',
          }));
          return;
        }
        var l = 0, team;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].u === socket.username) {
            if (servers[socket.room].pt[l].t.includes('@leader')) {
              team = servers[socket.room].pt[l].t.split(':')[1].replace('@leader', '');
            }
          }
          l++;
        } // check if player is a team leader or not (permission) and get team to check if requestor is trying to join that team
        var l = 0;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].u === data.data[1]) {
            if (servers[socket.room].pt[l].t.includes('@requestor#')) {
              if (servers[socket.room].pt[l].t.split('@requestor#')[1] === team) {
                servers[socket.room].pt[l].t = data.data[1]+':'+team;
                servers[socket.room].logs.push({
                  m: data.data[1]+' has joined team '+team,
                  c: '#40C4FF',
                });
                var q = 0;
                while (q<servers[socket.room].pt.length) {
                  servers[socket.room].pt[q].updates.logs = true;
                  q++;
                }
              }
            }
          }
          l++;
        } // check if joiner has requested to join
      } else if (command === '/leave') {
        var l = 0, team;
        while (l<servers[socket.room].pt.length) {
          if (servers[socket.room].pt[l].u === socket.username) {
            if (servers[socket.room].pt[l].t.includes('@leader')) {
              var q = 0;
              while (q<servers[socket.room].pt.length) {
                if (servers[socket.room].pt[q].t.split(':')[1].replace('@requsetor', '') === servers[socket.room].pt[l].t.split(':')[1].replace('@leader', '')) {
                  servers[socket.room].pt[q].t = servers[socket.room].pt[q].u+':'+Math.random(); // reset team to only themselves
                }
                q++;
              }
            }
            servers[socket.room].pt[l].t = servers[socket.room].pt[l].u+':'+Math.random();
          }
          l++;
        }
      } 
      if (!SETTINGS.admins.includes(socket.username)) {
        socket.send(JSON.stringify({
          status: 'error',
          message: 'You are not a server admin!',
        }));
        return;
      }
      if (command === '/ban') {
        if (data.data.length < 4 || isNaN(data.data[2])) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.'
          }));
          return;
        }
        if (SETTINGS.admins.includes(data.data[1])) {
          socket.send(JSON.stringify({
            status: 'error',
            message: "You can't ban another admin!", 
          }));
          return;
        }
        var l = 3, reason = '';
        while (l<data.data.length) {
          reason += data.data[l]+' ';
          l++;
        }
        SETTINGS.bans.push({
          username: data.data[1],
          by: socket.username,
          time: data.data[2],
          reason: reason,
        });
        servers[socket.room].logs.push({
          m: socket.username+' banned '+data.data[1]+' for '+data.data[2]+' minutes because "'+reason+'"',
          c: '#FF0000',
        });
        var l = 0;
        while (l<servers[socket.room].pt.length) {
          servers[socket.room].pt[l].updates.logs = true;
          l++;
        }
        var l = 0;
        while (l<sockets.length) {
          if (sockets[l].username === data.data[1]) {
            sockets[l].send(JSON.stringify({
              status: 'error',
              message: 'You were just banned!',
            }));
            setTimeout(function() {
              this.destroy();
            }.bind(sockets[l]));
          }
          l++;
        }
      } else if (command === '/unban') {
        if (data.data.length != 2) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.',
          }));
          return;
        }
        var l = 0;
        while (l<SETTINGS.bans.length) {
          if (SETTINGS.bans[l].username === data.data[1]) {
            SETTINGS.bans.splice(l, 1);
          }
          l++;
        }
      } else if (command === '/kick') {
        if (data.data.length != 2) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.'
          }));
          return;
        }
        var l = 0;
        while (l<sockets.length) {
          if (sockets[l].username === data.data[1]) {
            sockets[l].send(JSON.stringify({
              status: 'error',
              message: 'You have been kicked by '+socket.username,
            }))
            setTimeout(function() {
              this.destroy();
            }.bind(sockets[l]));
          }
          l++;
        }
      } else if (command === '/kill') {
        if (data.data.length != 2) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.',
          }));
          return;
        }
        var l = 0;
        while (l<servers.length) {
          var q = 0;
          while (q<servers[l].pt.length) {
            if (servers[l].pt[q].u === data.data[1]) {
              servers[l].pt[q].h = 0;
              servers[l].pt[q].ded = true;
            }
            q++;
          }
          l++;
        }
      } else if (command === '/target') {
        if (data.data.length != 3) {
          socket.send(JSON.stringify({
            status: 'error',
            message: 'Command has invalid arguments.',
          }));
          return;
        }
        var i = setInterval(function(u, s) {
          var l = 0;
          while (l<servers.length) {
            var q = 0;
            while (q<servers[l].pt.length) {
              if (servers[l].pt[q].u === u) {
                servers[l].b.push(new Block(servers[l].pt[q].x, servers[l].pt[q].y, Infinity, 'airstrike', s, servers[l]));
              }
              q++;
            }
            l++;
          }
        }, 1000, data.data[1], socket.username);
        setTimeout(function() {
          clearInterval(i);
        }, data.data[2]);
      }
    } else {
      setTimeout(() => {
        socket.destroy();
      }, 20);
    }
  });
  socket.on('close', (code, reason) => {
    if (socket.room !== undefined) servers[socket.room].disconnect(socket, code, reason);
  });
});

class Helper {
  static en(c) {var x='charCodeAt',b,e={},f=c.split(""),d=[],a=f[0],g=256;for(b=1;b<f.length;b++)c=f[b],null!=e[a+c]?a+=c:(d.push(1<a.length?e[a]:a[x](0)),e[a+c]=g,g++,a=c);d.push(1<a.length?e[a]:a[x](0));for(b=0;b<d.length;b++)d[b]=String.fromCharCode(d[b]);return d.join("")}

  static de(b) {var a,e={},d=b.split(""),c=d[0],f=d[0],g=[c],h=256,o=256;for(b=1;b<d.length;b++)a=d[b].charCodeAt(0),a=h>a?d[b]:e[a]?e[a]:f+c,g.push(a),c=a.charAt(0),e[o]=f+c,o++,f=a;return g.join("")}
}

class FFA {
  constructor(id) {
    this.sockets = [];
    this.spawn = {
      x: 0,
      y: 0,
    }
    this.id = id; // may remove
    this.logs = [];
    this.ai = []; // ai
    this.b = []; // blocks
    this.s = []; // bullets
    this.pt = []; // players
    this.d = []; // damage entities
    this.i = [];
    this.t = [];

    this.levels = [];
    this.levels.push(['22 2 =2222222222222222222222=1','22 = == ===================21=','    =      # # #           122','2=   = #   #   # ## ## #  1 =2','  = @= ##  ## ##         1  =2','=  ==  2## #   # #      1   =2','2=    222### # # #     1  # =2','2  ##22222##   #      1     =2','2=  ##22222##  # #   1      =2','2=   ##22222##      1       =2','2= #  ##22222##    1  #   # =2','2= ##  ##22222##  1         =2','2= ###  ##222221 1  # ##### =2','2= # ##  ##222111   #     # =2','2= # ###  ##211111#  #  # # =2','2= #   ##  #111112##  #   # =2','2= # #   #   111222##  #  # =2','2= ##### #  1 122222##  # # =2','2=         1  ##22222##  ## =2','2= #   #  1    ##22222##  # =2','2=       1      ##22222##   =2','2=      1   # # ###22222##  =2','2=     1      #   ##22222##  2','2= #  1     # # # ###222    =2','2=   1      # # # # ##2  ==  =','2=  1         # # #  ## =  =  ','2= 1  # ## ## # # #   # =   =2','221           #   #      =    ','=12=================== =  = 22','1=2222222222222222222222= 2 22']);


    this.levels.push(['2     =    #     #  #  =     2', ' ==2=    # #  #  #  #    =2== ', ' =  = =  #    #     #  = =  = ', ' 2 == =  #### # #####  = == 2 ', ' ==== =  #    #     #  = ==== ', '      1  # #     #  #  1      ', '= ===11  # # ### # ##  11=== =', '         # #     #  #         ', '  222#   ############         ', ' 22222#                ###### ', ' 22222#   2  2222  2   #  # # ', ' 22222#                #  # # ', ' 22222#     # 11 #     # ## # ', ' 22222#   2  1111  2   #    # ', ' 22222#   2 11@ 11 2   # ## # ', ' 22222#   2 11  11 2   #  # # ', ' 22222#   2  1111  2   #  # # ', ' 22222#     # 11 #     #### # ', ' 22222#                     # ', ' 22222#   2  2222  2   ## # # ', ' 22222#                #  # # ', '  222#   ############  ###### ', '         #    ##    #         ', '= ===11  # #      # #  11=== =', '      1  #    ##    #  1      ', ' ==== =  #    ##    #  = ==== ', ' 2 == =  #    ##    #  = == 2 ', ' =  = =  #    ##    #  = =  = ', ' ==2=    # #      # #    =2== ', '2     =  #    ##       =     2']);

    this.levels.push(['========   #      #   =========', '======     2      2     =======', '====11     #      #     11=====', '===111      #    #      111====', '==111#########22#########111==', '==11##   #          #   ##11==', '=   # #                ###   =', '=   #  2    #    #    ####   =', '    #   2            # 1 #    ', '    #    2          #  # #    ', '    # 1   #        ##1## #    ', '#2# #      #####2##   #  # #2#', '   ##      2222222#1### ###   ', '    #      #2    22 #    #    ', '    2  1   #2 @  2######1#    ', '    2    1 #2    22    # 2    ', '    #      #2    2####1# #    ', '   ## #    #222222# #  # ##   ', '#2# #      #####2## #1##1# #2#', '    #     1        ##  # #    ', '    #   1     1     ##1# #    ', '    #    #        1  #   #    ', '=   #          #      ####   =', '=   ### 1  1        1  ###   =', '==11###                 ##11==', '==111#########22#########111==', '===111      #    #      111===', '====11     #      #     11====', '======     2      2     ======', '========   #      #   ========']);

    this.levels.push(['===========================222', '===========        ========222', '========    ###  #    =====222', '======222####           =====2', '===== 2###    #  11#1111#====2', '====  ##22 ####  111111111===2', '===  ##22###  1  #111#1111#==2', '=== ##  ##  1 1  #111##111#==2', '==22#  ## 1 1 1   1111111122=2', '==2##2## 1   ####  #1111#122=2', '==2#22# 1 1         11111122=2', '=  #2##    #  22  # 111111#222', '= ## # 11                2222=', '=2#  #   #   #  #   #      22=', '=2# ##111# 2  @   2 #        =', '=22 #    # 2      2 #  2### #=', '=####1 2 #   #  #   ### 1  # =', '=2222 2#              1# #   =', '22221  2 1 #  22  # ## 1  ## =', '2=222#21 2        # 1 # #   ==', '2=222 #      ####   #1  # # ==', '2=2 # 12 1 1  #   ## ###  # ==', '2==2     2 22 2 # 1 1   # #===', '2==  12 2 2#   #1  # # #   ===', '2===   #   2 1 # #1#  ####====', '2====  2 21 12 # #   ##  =====', '2=====   1 2#  # # ##   ======', '222=====   1   ###    ========', '222======== 2 1#   ============', '222===========================']);

    this.levels.push(['=====         ==         =====', '===     ====  ==           ===', '===        =  =====        ===', '=          =  =====          =', '=   = =       ==       = =   =', '   = =        ==        = =   ', '      = =   ====     = =      ', '     = =    ====      = =     ', '            ==                ', '            ==   ===          ', '        =                 =   ', '  ==  ===                 === ', '  ==        =1111=  ====      ', '  ==        1    1  ====      ', '========    1 @  1    ========', '========    1    1    ========', '      ====  1    1        ==  ', '      ====  =1111=        ==  ', '  =                 ===   ==  ', ' ====                 =       ', '  =             ==            ', '                ==          = ', '     = =      ====    = =   = ', '      = =     ====   = =      ', '   = =        ==        = =   ', '=   = =    =====       = =   =', '=          =====             =', '===           ==  ===      ===', '===           ==    ===    ===', '=====         ==         =====']);

    this.levels.push([' # # # # # #  ## 1  1   1    1', '              ##111    111    ', ' # # # # # #  ## 1  1   1  1  ', '              11    1      1  ', ' # # # # # #  11  11111  11111', '              ##    1      1  ', ' # # # # # #  ## 1  1  1 1 1  ', '              ##       1      ', ' # # # # # # 1111#   11111  1 ', '            1      1   1      ', ' # # # # # #           1   1  ', '             2222  #      111 ', ' # # # ##   #1  1#  1   1  1  ', '        1  212  212  1        ', '###11###1  2  @   2  1###11###', '###11###1  2      2  1###11###', ' 22   1 1  212  212  1     22 ', '  2  11 1   #1  1#   #        ', '  2      1   2222             ', ' 22       #       #        2  ', '    22           1   111   2  ', '     2      #1111         222 ', '   2 2 11     ##        222   ', '1  2    1   22##2 2222    2   ', '1  2    111  2##2    2    2 1 ', '11            11     222222 1 ', ' 1  2         11         2  1 ', '    2222 11   ##  11111  2  1 ', '  1    2  1 22##2   1       1 ', ' 1111         ##22    11      ']);

    this.levels.push(['=   ==========================','     =========================','  @  222 =   =   =   =   =====','     ===   =   =   =   =   222','=   =========1============1==2','==2==########1############1##2','==2==#  1        #          # ','==2==#1    1   1 #          # ','==  =#   1   1   # 1    ##  # ','=== =#          1# 1      # # ','==  =#  1   1    # 1  #   # # ','== ==# 1  1    1 #    #     # ','==  =#       1   #     ##   # ','=== 11  1     2#####2       # ','==  =#    1  222111222      # ','== ==#      1#211 112#  111 # ','==  =# 1 1   #11   11#      # ','=== =#########1     1######## #','==  =#       #11   11#    # # ','== ==#  111  #211 112#  2 # # ','==  =#       222111222    # # ','=== =#        2#####2     2 # ','==  =#    ##     #     #  # # ','== ==#      #    #    ##  # # ','==  =#  #   #  1 # 2      # 2 ','=== =#  #      1 #        # # ','=== 11   ##    1 ####2####### ','===2=#           #        ### ','===2=###################2#### ','===222                        ']);

    this.levels.push(['==============================','=======================1======','==============================','=  #22       #        22#    =','=  ##22      #       222#    =','=    #22     1      222 #    =','=    #22     1     222  #    =','=    ##22    #    222  #     =','= 22  # 222  # 2 22    1     =','= 22  1   2 2#1 22     1     =','=     1   221#112  11  #     =','=     #   211 111  11  #     =','=     #   #1   1#      #     =','=##22##2221  @   ########11##=','=     #   1#   #1      #     =','=     1   11   11 11   #     =','=     1    1   1  11   1     =','=     #   #11111       1     =','=    ##   ##   ##      #     =','=    #   1#2    #1     #     =','=    #  11       11    ##    =','=  ### 11         11    #    =','=  #  ##           ##   #    =','=  # ###  ##  ##  ####  #    =','=  ##                ## #    =','=  ######             # #    =','=22#########22###22######22===','=                          ===','=                          ===','==============================']);

    this.levels.push(['==============================','=======================1======','==============================','=  #22       #        22#    =','=  ##22      #       222#    =','=    #22  T  1  T   222 #    =','=    #22     1     222  #    =','=    ##22    #    222  #     =','= T2  # 222  # 2 22    1  T  =','= 2T  1   2 1#1 22     1     =','=     1   211#112  11  #     =','=     #   T11 11T  11  #     =','=     #   11   11      #     =','=##22##2221  @  1########11##=','=     #   11   11      #     =','=     1   T1   1T 11   #     =','=     1    1   1  11   1     =','= T   #   #11111       1     =','=    ##   ##   ##      #     =','=    #   1#2    #1     #     =','=    #  11       11    ## T  =','=  ### 11         11    #    =','=  #  ##   T  T    ##   #    =','=  # ###  ##  ##  ####  #    =','=  ##                ## #    =','=  ######             # #    =','=22#########22###22######22===','=                          ===','=    T                 T   ===','==============================']);

    this.levels.push(['==============================','=====        ===        ======','===           =           ====','==     #####     #####     ===','==                         ===','=    #####1#############    ==','=    #     #       #   #    ==','=  # # ## ## ### # # # # #  ==','=  # # #   # # # ### # # #  ==','=  # # # # #   # #   # # #  ==','=  # # # ### # # # ### # #  ==','=  # # # #   #2#   #   # #  ==','=    #      ##2##  # # #    ==','==   ###### #   ###### #   ===','===  #      # @ #      #  ====','==   # ## ###   # ######   ===','=    #  # # ##2## #    #    ==','=  # ## # #  #2#  # #  # #  ===','=  # ## # ## # #    # ## #  ===','=  # 1  #    # # ####  1 #  ==','=  # #### ## #   #     # #  ==','=  # # #   # # # ## ## # #  ==','=    #   #     #    #  #    ==','=    ########1##########    ==','==                         ===','==     #####     #####     ===','===           =           ====','=====        ===        ======','==============================','==============================']);


    this.levelReader(this.levels[Math.floor(Math.random() * this.levels.length)]);


    if (!SETTINGS.fps_boost) {
      this.i.push(setInterval(this.send.bind(this), 1000 / SETTINGS.UPS));
    }
    this.i.push(setInterval(this.tick.bind(this), 1000 / 60));
  }

  tick() {

    var l = 0;
    while (l < this.ai.length) {
      if (this.ai[l].p !== 0) {
        this.ai[l].p += 0.5;
      }
      this.ai[l].update();
      l++;
    }

    var l = 0;
    while (l < this.s.length) {
      this.s[l].update();
      l++;
    }

    var l = 0;
    while (l < this.pt.length) {
      var q = 0;
      while (q < this.pt.length) {
        if ((this.pt[l].x > this.pt[q].x || this.pt[l].x + 80 > this.pt[q].x) && (this.pt[l].x < this.pt[q].x + 80 || this.pt[l].x + 80 < this.pt[q].x + 80)) {
          if ((this.pt[l].y > this.pt[q].y || this.pt[l].y + 80 > this.pt[q].y) && (this.pt[l].y < this.pt[q].y + 80 || this.pt[l].y + 80 < this.pt[q].y + 80)) {
            if (this.pt[q].i && this.pt[q].c === 'warrior' && this.pt[l].u !== this.pt[q].u && this.pt[l].canBashed) {
              this.pt[l].h -= 50;
              this.pt[l].canBashed = false;
              setTimeout(function() {
                this.canBashed = true;
              }.bind(this.pt[l]), 500);
            }
          }
        }
        q++;
      }
      l++;
    }

    var l = 0;
    while (l < this.pt.length) {
      if (this.pt[l].p != 0) {
        this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
        this.pt[l].p += 0.5;
      }
      var q = 0;
      while (q < this.b.length) {
        if ((this.pt[l].x > this.b[q].x || this.pt[l].x + 80 > this.b[q].x) && (this.pt[l].x < this.b[q].x + 100 || this.pt[l].x + 80 < this.b[q].x + 100)) {
          if ((this.pt[l].y > this.b[q].y || this.pt[l].y + 80 > this.b[q].y) && (this.pt[l].y < this.b[q].y + 100 || this.pt[l].y + 80 < this.b[q].y + 100)) {
            if (this.b[q].t === 'mine' && this.b[q].a && !this.pt[l].ded && !this.pt[l].i) {
              this.b[q].destroy();
              q--;
            }
            var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].split(':')[1].replace('@leader', '') : this.pt[l].t.split(':')[1].replace('@leader', '');
            if (this.b[q].t === 'spike' && !this.pt[l].ded && !this.pt[l].i && this.b[q].o.split(':')[1].replace('@leader', '') !== team) {
              this.pt[l].h -= 1;
              this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
              if (this.pt[l].d !== false) {
                clearTimeout(this.pt[l].d.ti);
                this.pt[l].d.d += 1;
                this.pt[l].d.x = this.pt[l].x;
                this.pt[l].d.y = this.pt[l].y;
                this.pt[l].d.ti = setTimeout(function() {
                  this.d = false;
                }.bind(this.pt[l]), 1000);
              } else {
                this.pt[l].d = {
                  d: 1,
                  x: this.pt[l].x,
                  y: this.pt[l].y,
                  ti: setTimeout(function() {
                    this.d = false;
                  }.bind(this.pt[l]), 1000),
                };
              }
              if (this.pt[l].h <= 0) {
                this.pt[l].ded = true;
                this.logs.push({
                  m: this.pt[l].u + ' was skewered on a spike.',
                  c: '#FF8C00',
                });
                var m = 0;
                while (m < this.pt.length) {
                  this.pt[m].updates.logs = true;
                  m++;
                }
              }
              break;
            }
          }
        }
        if (this.b[q].t === 'heal') {
          var distance = Math.abs(Math.sqrt((this.pt[l].x - this.b[q].x) * (this.pt[l].x - this.b[q].x) + (this.pt[l].y - this.b[q].y) * (this.pt[l].y - this.b[q].y)));
          var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].split(':')[1].replace('@leader', '') : this.pt[l].t.split(':')[1].replace('@leader', '');
          if (distance < 400 && team === this.b[q].o.split(':')[1]) {
            this.pt[l].h += .1;
            this.pt[l].h = Math.min(this.pt[l].h, this.pt[l].ma);
          }
        }
        q++;
      }
      var q = 0;
      while (q < this.pt[l].d.length) {
        this.pt[l].d[q].y -= .2;
        this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
        q++;
      }
      if (this.pt[l].grapple) {
        this.grapple(this.pt[l]);
      }
      l++;
    }

    var l = 0;
    while (l < this.d.length) {
      if (this.d[l].c) {
        var q = 0;
        while (q < this.pt.length) {
          if ((this.d[l].x > this.pt[q].x || this.d[l].x + this.d[l].w > this.pt[q].x) && (this.d[l].x < this.pt[q].x + 80 || this.d[l].x + this.d[l].w < this.pt[q].x + 80)) {
            if ((this.d[l].y > this.pt[q].y || this.d[l].y + this.d[l].h > this.pt[q].y) && (this.d[l].y < this.pt[q].y + 80 || this.d[l].y + this.d[l].h < this.pt[q].y + 80) && !this.pt[q].ded) {
              var team = this.pt[q].t.includes('@requestor#') ? this.pt[q].t.split('@requestor#')[0].split(':')[1].replace('@leader', '') : this.pt[q].t.split(':')[1].replace('@leader', '');
              if (this.d[l].a < 0 && this.d[l].o.split(':')[1] === team) {
                var d = {...this.d[l]};
                d.o = d.o.split(':')[0];
                this.damagePlayer(this.pt[q], d);
              } else if (team !== this.d[l].o.split(':')[1] && this.d[l].a > 0) {
                var d = {...this.d[l]};
                d.o = d.o.split(':')[0];
                this.damagePlayer(this.pt[q], d);
              }
            }
          }
          q++;
        }
        q = 0;
        while (q < this.b.length) {
          if ((this.d[l].x > this.b[q].x || this.d[l].x + this.d[l].w > this.b[q].x) && (this.d[l].x < this.b[q].x + 100 || this.d[l].x + this.d[l].w < this.b[q].x + 100)) {
            if ((this.d[l].y > this.b[q].y || this.d[l].y + this.d[l].h > this.b[q].y) && (this.d[l].y < this.b[q].y + 100 || this.d[l].y + this.d[l].h < this.b[q].y + 100)) {
              if (this.b[q].damage(this.d[l].a) && q !== 0) {
                q--;
              };
            }
          }
          q++;
        }
        q = 0;
        while (q < this.ai.length) {
          if ((this.d[l].x > this.ai[q].x || this.d[l].x + this.d[l].w > this.ai[q].x) && (this.d[l].x < this.ai[q].x + 80 || this.d[l].x + this.d[l].w < this.ai[q].x + 80)) {
            if ((this.d[l].y > this.ai[q].y || this.d[l].y + this.d[l].h > this.ai[q].y) && (this.d[l].y < this.ai[q].y + 80 || this.d[l].y + this.d[l].h < this.ai[q].y + 80)) {
              if (this.ai[q].o !== this.d[l].o) {
                this.ai[q].damage(this.d[l].a);
              }
            }
          }
          q++;
        }
      }
      this.d[l].c = false;
      l++;
    }
  }

  grapple(t) {
    var mpf = 20;
    var d = Math.sqrt(Math.pow((t.grapple.target.x + ((t.grapple.target.u !== undefined) ? 40 : 50)) - (t.x + 40), 2) + Math.pow((t.grapple.target.y + ((t.grapple.target.u !== undefined) ? 40 : 50)) - (t.y + 40), 2));
    var x = t.x + ((t.grapple.target.x + ((t.grapple.target.u !== undefined) ? 40 : 50)) - (t.x + 40)) * (mpf / d);
    var y = t.y + ((t.grapple.target.y + ((t.grapple.target.u !== undefined) ? 40 : 50)) - (t.y + 40)) * (mpf / d);
    if (d < mpf) {
      x = (t.grapple.target.x + ((t.grapple.target.u !== undefined) ? 40 : 50)) - 40;
      y = (t.grapple.target.y + ((t.grapple.target.u !== undefined) ? 40 : 50)) - 40;
    }
    var mx = false, my = false;
    if (!this.collision(x, t.y)) {
      x = t.x;
      mx = true;
    }
    if (!this.collision(t.x, y)) {
      y = t.y;
      my = true;
    }
    if (Math.round(x + 40) === Math.round((t.grapple.target.x + ((t.grapple.target.u !== undefined) ? 40 : 50)))) {
      mx = true;
    }
    if (Math.round(y + 40) === Math.round((t.grapple.target.y + ((t.grapple.target.u !== undefined) ? 40 : 50)))) {
      my = true;
    }
    t.x = x;
    t.y = y;
    t.grapple.bullet.sx = t.x + 40;
    t.grapple.bullet.sy = t.y + 40;
    this.triggerUpdate(t.x + 40, t.y + 40, 'bullet');
    if (mx && my) {
      t.grapple.bullet.destroy();
      t.grapple = false;
    }
    t.socket.send(JSON.stringify({
      event: 'override',
      data: [{
        key: 'x',
        value: x,
      }, {
        key: 'y',
        value: y,
      }],
    }));
  }

  triggerUpdate(x, y, type) {
    var l = 0;
    while (l < this.pt.length) {
      var t = this.pt[l];
      if ((t.x-860 > x || t.x+940 > x) && (t.x-860 < x || t.x+940 < x) && (t.y-560 > y || t.y+640 > y) && (t.y-560 < y || t.y+640 < y)) {
        t.updates[type] = true;
      }
      l++;
    }
  }

  collision(x, y) {
    if (x < 0 || y < 0 || x + 80 > 3000 || y + 80 > 3000) return false;
    var l = 0;
    while (l < this.b.length) {
      if ((x > this.b[l].x || x + 80 > this.b[l].x) && (x < this.b[l].x + 100 || x + 80 < this.b[l].x + 100) && (y > this.b[l].y || y + 80 > this.b[l].y) && (y < this.b[l].y + 100 || y + 80 < this.b[l].y + 100) && this.b[l].c) {
        return false;
      }
      l++;
    }
    return true;
  }

  levelReader(level) {
    var l, q;
    for (l = 0; l < level.length; l++) {
      for (q = 0; q < level[l].split('').length; q++) {
        var p = level[l].split(''); // Block key: # = invincible, 1 = weak, 2 = strong, @ = player, A = ai
        if (p[q] === '=') {
          this.b.push(new Block(q * 100, l * 100, Infinity, 'void', 'MapGenerator[]:3', this));
        } else if (p[q] === '#') {
          this.b.push(new Block(q * 100, l * 100, Infinity, 'barrier', 'MapGenerator[]:3', this));
        } else if (p[q] === '2') {
          this.b.push(new Block(q * 100, l * 100, 200, 'strong', 'MapGenerator[]:3', this));
        } else if (p[q] === '1') {
          this.b.push(new Block(q * 100, l * 100, 100, 'weak', 'MapGenerator[]:3', this));
        } else if (p[q] === '0') {
          this.b.push(new Block(q * 100, l * 100, Infinity, 'spike', 'MapGenerator[]:3', this));
        } else if (p[q] === 'U') {
          this.b.push(new Block(q * 100, l * 100, 400, 'fortress', 'MapGenerator[]:3', this));
        }else if (p[q] == '@') {
          this.spawn.x = q * 100;
          this.spawn.y = l * 100;
        }
      }
    }
  }

  add(socket, data) {
    this.sockets.push(socket);
    data.updates = {
      block: true,
      tank: true,
      bullet: true,
      logs: true,
    };
    data.d = false;
    data.ma = data.m*50+300; // ma = max
    data.h = data.ma;
    data.socket = socket;
    data.canIn = true; // can change invis status
    data.canBashed = true; // can take warrior class bash attack damage
    data.s = 0;
    data.t = data.u+':'+Math.random();
    socket.send(JSON.stringify({
      event: 'override',
      data: [{
        key: 'x',
        value: this.spawn.x,
      }, {
        key: 'y',
        value: this.spawn.y,
      }],
    }));
    this.pt.push(data);
    this.logs.push({
      m: this.joinMsg(data.u),
      c: '#66FF00',
    });
    var l = 0;
    while (l < this.pt.length) {
      this.pt[l].updates.logs = true;
      l++;
    }
  }

  update(data) {
    var tank = data.data;
    var l = 0;
    while (l < this.pt.length) {
      if (this.pt[l].u === data.username) {
        if (this.pt[l].ded) {
          var x = this.spawn.x;
          var y = this.spawn.y;
          setTimeout(function() {
            this.socket.send(JSON.stringify({
              event: 'ded',
            }));
            this.socket.send(JSON.stringify({
              event: 'override',
              data: [{
                key: 'x',
                value: x,
              }, {
                key: 'y',
                value: y,
              }],
            }));
            this.ded = false;
            this.h = this.ma;
          }.bind(this.pt[l]), 10000);
        }
        var doTankUpdate = false;
        this.pt[l].br = tank.br;
        this.pt[l].i = tank.i;
        this.pt[l].a = tank.a;
        if (this.pt[l].e !== tank.e) {
          this.pt[l].e = tank.e;
          doTankUpdate = true;
        }
        if (this.pt[l].canIn) this.pt[l].in = tank.in;
        if (!this.pt[l].grapple) {
          if (this.pt[l].x !== tank.x) {
            this.pt[l].x = tank.x;
            doTankUpdate = true;
          }
          if (this.pt[l].y !== tank.y) {
            this.pt[l].y = tank.y;
            doTankUpdate = true;
          }
        }
        if (this.pt[l].r !== tank.r) {
          this.pt[l].r = tank.r;
          doTankUpdate = true;
        }
        if (!this.pt[l].ded) {
          if (this.pt[l].ba !== tank.ba) {
            this.pt[l].ba = tank.ba;
            doTankUpdate = true;
          }
          if (tank.dy) { // dynamite
            var q = 0;
            while (q<this.s.length) {
              if (this.s[q].u.split(':')[0] === this.pt[l].t.split(':')[0] && this.s[q].t === 'dynamite') {
                this.d.push(new Damage(this.s[q].x-100, this.s[q].y-100, 200, 200, 100, this.s[q].u, this));
                this.s[q].destroy();
                q--;
              }
              q++;
            }
          }
          if (tank.fi.length > 0) {
            this.pt[l].p = -3; // pushback
            var q = 0;
            var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].replace('@leader', '') : this.pt[l].t.replace('@leader', '');
            while (q < tank.fi.length) {
              this.s.push(new Shot(this.pt[l].x + 40, this.pt[l].y + 40, tank.fi[q].x, tank.fi[q].y, tank.fi[q].t, tank.fi[q].r, tank.fi[q].t === 'grapple' ? this.pt[l].u : team, this));
              q++;
            }
            doTankUpdate = true;
          }
          if (tank.to) {
            if (this.pt[l].healInterval === undefined) {
              this.pt[l].in = false;
              this.pt[l].canIn = false;
              this.pt[l].healInterval = setInterval(function(host) {
                this.h += 1;
                this.h = Math.min(this.ma, this.h);
                var q = 0;
                while (q<host.ai.length) {
                  if (host.ai[q].o.split(':')[0] === this.u) {
                    host.ai[q].hp += 1;
                    host.ai[q].hp = Math.min(500, host.ai[q].hp);
                  }
                  q++;
                }
              }.bind(this.pt[l]), 100, this);
              this.pt[l].healTimeout = setTimeout(function(host) {
                this.h = this.ma;
                var q = 0;
                while (q<host.ai.length) {
                  if (host.ai[q].o.split(':')[0] === this.u) {
                    host.ai[q].hp = 600;
                  }
                  q++;
                }
                clearInterval(this.healInterval);
                this.healInterval = undefined;
                this.canIn = true;
              }.bind(this.pt[l]), 7500, this);
            } else {
              clearInterval(this.pt[l].healInterval);
              clearTimeout(this.pt[l].healTimeout);
              this.pt[l].healInterval = undefined;
              this.pt[l].canIn = true;
            }
            doTankUpdate = true;
          }
          if (tank.ta) {
            this.pt[l].h += this.pt[l].ma * .25;
            this.pt[l].h = Math.min(this.pt[l].ma, this.pt[l].h);
            var q = 0;
            while (q<this.ai.length) {
              if (this.ai[q].o.split(':')[0] === this.pt[l].u) {
                this.ai[q].hp += 600*.25;
                this.ai[q].hp = Math.min(600, this.ai[q].hp);
              }
              q++;
            }
          }
          if (tank.gl) {
            clearInterval(this.pt[l].gluInterval);
            this.pt[l].gluInterval = setInterval(function(host) {
              this.h += 2;
              this.h = Math.min(this.ma, this.h);
              var l = 0;
              while (l<host.ai.length) {
                if (host.ai[l].o === this.u) {
                  host.ai[l].hp += 2;
                  host.ai[l].hp = Math.min(600, host.ai[l].hp);
                }
                l++;
              }
            }.bind(this.pt[l]), 100, this);
            this.pt[l].gluTimeout = setTimeout(function() {
              clearInterval(this.gluInterval);
            }.bind(this.pt[l]), 10000);
          }
          if (tank.pl) {
            var key = {
              strong: 200,
              weak: 100,
              gold: 300,
              heal: 300,
              mine: 0,
              spike: 0,
              fortress: 200,
            };
            var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].replace('@leader', '') : this.pt[l].t.replace('@leader', '');
            if (tank.r >= 337.5 || tank.r < 22.5) {
              this.b.push(new Block(this.pt[l].x - 10, this.pt[l].y + 80, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 22.5 && tank.r < 67.5) {
              this.b.push(new Block(this.pt[l].x - 100, this.pt[l].y + 80, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 67.5 && tank.r < 112.5) {
              this.b.push(new Block(this.pt[l].x - 100, this.pt[l].y - 10, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 112.5 && tank.r < 157.5) {
              this.b.push(new Block(this.pt[l].x - 100, this.pt[l].y - 100, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 157.5 && tank.r < 202.5) {
              this.b.push(new Block(this.pt[l].x - 10, this.pt[l].y - 100, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 202.5 && tank.r < 247.5) {
              this.b.push(new Block(this.pt[l].x + 80, this.pt[l].y - 100, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 247.5 && tank.r < 292.5) {
              this.b.push(new Block(this.pt[l].x + 80, this.pt[l].y - 10, key[tank.st], tank.st, team, this));
            }
            if (tank.r >= 292.5 && tank.r < 337.5) {
              this.b.push(new Block(this.pt[l].x + 80, this.pt[l].y + 80, key[tank.st], tank.st, team, this));
            }
            var b = this.b[this.b.length - 1];
            this.triggerUpdate(b.x, b.y, 'block');
            doTankUpdate = true;
          }
          if (tank.fl) {
            // stun tank
            var q = 0;
            while (q < this.pt.length) {
              if ((this.pt[l].x-860 > this.pt[q].x || this.pt[l].x+940 > this.pt[q].x) && (this.pt[l].x-860 < this.pt[q].x+40 || this.pt[l].x+940 < this.pt[q].x+40) && (this.pt[l].y-560 > this.pt[q].y || this.pt[l].y+640 > this.pt[q].y) && (this.pt[l].y-560 < this.pt[q].y+40 || this.pt[l].y+640 < this.pt[q].y+40)) {
                this.pt[q].fb = true; // flashbanged
                clearTimeout(this.pt[q].flashbangTimeout);
                this.pt[q].flashbangTimeout = setTimeout(function() {
                  this.fb = false;
                  this.updates.tank = true;
                }.bind(this.pt[q]), 2500);
              }
              q++;
            }
          }
          if (tank.bo || tank.po) {
            // break blocks
            var q = 0;
            while (q < this.b.length) {
              if ((this.pt[l].x > this.b[q].x || this.pt[l].x + 80 > this.b[q].x) && (this.pt[l].x < this.b[q].x + 100 || this.pt[l].x + 80 < this.b[q].x + 100) && (this.pt[l].y > this.b[q].y || this.pt[l].y + 80 > this.b[q].y) && (this.pt[l].y < this.b[q].y + 100 || this.pt[l].y + 80 < this.b[q].y + 100)) {
                this.b[q].destroy();
                q--;
              }
              q++;
            }
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
            doTankUpdate = true;
          }
          if (tank.po) {
            // break blocks
            this.d.push(new Damage(this.pt[l].x - 40, this.pt[l].y - 40, 160, 160, 70, this.pt[l].t, this));
            doTankUpdate = true;
          }
          if (tank.tu) {
            var q = 0;
            while (q < this.ai.length) {
              if (this.ai[q].o.split(':')[0] === this.pt[l].u) {
                this.ai.splice(q, 1);
                q--;
              }
              q++;
            }
            this.ai.push(new Ai(this.pt[l].x, this.pt[l].y, 0, this.pt[l].t, this));
          }
          if (tank.bu) {
            this.pt[l].buff = true;
            setTimeout(function() {
              this.buff = false;
            }.bind(this.pt[l]), 10000);
          }
          if (tank.bui) {
            var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].replace('@leader', '') : this.pt[l].t.replace('@leader', '');
            if (tank.r <= 45 || tank.r > 315) {
              this.b.push(new Block(this.pt[l].x - 55, this.pt[l].y + 40, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x - 5, this.pt[l].y + 40, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x + 45, this.pt[l].y + 40, 800, 'gold', team, this));
            }
            if (tank.r > 45 && tank.r <= 135) {
              this.b.push(new Block(this.pt[l].x - 50, this.pt[l].y - 55, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x - 50, this.pt[l].y - 5, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x - 50, this.pt[l].y + 45, 800, 'gold', team, this));
            }
            if (tank.r > 135 && tank.r <= 225) {
              this.b.push(new Block(this.pt[l].x - 55, this.pt[l].y - 50, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x - 5, this.pt[l].y - 50, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x + 45, this.pt[l].y - 50, 800, 'gold', team, this));
            }
            if (tank.r > 225 && tank.r <= 315) {
              this.b.push(new Block(this.pt[l].x + 40, this.pt[l].y - 55, 800, 'gold', team, this.pt[l].u, this));
              this.b.push(new Block(this.pt[l].x + 40, this.pt[l].y - 5, 800, 'gold', team, this));
              this.b.push(new Block(this.pt[l].x + 40, this.pt[l].y + 45, 800, 'gold', team, this));
            }
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
          }
          if (tank.mi) { // place landmine directly under tank
            var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].replace('@leader', '') : this.pt[l].t.replace('@leader', '');
            this.b.push(new Block(this.pt[l].x, this.pt[l].y, 0, 'mine', team, this));
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
          }
          if (tank.sh) {
            this.pt[l].s += 100; // add a hundred shields
            this.pt[l].s = Math.min(500, this.pt[l].s); // max of 500 shields
          }
          if (tank.as !== false && tank.as !== undefined) {
            var team = this.pt[l].t.includes('@requestor#') ? this.pt[l].t.split('@requestor#')[0].replace('@leader', '') : this.pt[l].t.replace('@leader', '');
            this.b.push(new Block(tank.as.x, tank.as.y, Infinity, 'airstrike', team, this));
            this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'block');
          }
        }
        if (doTankUpdate) {
          this.triggerUpdate(this.pt[l].x, this.pt[l].y, 'tank');
        }
        var render = {
          left: {
            x: Math.floor((this.pt[l].x - 560) / 50),
            y: Math.floor((this.pt[l].y - 560) / 50),
          },
          right: {
            x: Math.ceil((this.pt[l].x + 860) / 50),
            y: Math.ceil((this.pt[l].y + 860) / 50),
          },
        }
        if (JSON.stringify(render) !== JSON.stringify(this.pt[l].render)) {
          this.pt[l].updates.block = true;
          this.pt[l].render = render;
        }
      }
      l++;
    }
    if (SETTINGS.fps_boost) {
      this.send();
    }
  }

  send() {
    var l = 0;
    while (l < this.pt.length) {
      outgoing_per_second++;

      var message = {};

      if (this.pt[l].updates.block || this.pt[l].u === '[Spector]') {
        message.blocks = [];
        var q = 0;
        while (q < this.b.length) {
          if (((this.pt[l].x-860 > this.b[q].x || this.pt[l].x+940 > this.b[q].x) && (this.pt[l].x-860 < this.b[q].x+100 || this.pt[l].x+940 < this.b[q].x+100) && (this.pt[l].y-560 > this.b[q].y || this.pt[l].y+640 > this.b[q].y) && (this.pt[l].y-560 < this.b[q].y+100 || this.pt[l].y+640 < this.b[q].y+100)) || this.pt[l].u === '[Spector]') {
            message.blocks.push(JSON.parse(JSON.stringify(this.b[q], (name, value) => {
              if (['ho', 'bar', 'sd'].includes(name)) {
                return undefined;
              } else {
                return value;
              }
            })));
          }
          q++;
        }
        this.pt[l].updates.block = false;
      }

      if (this.pt[l].updates.tank || this.pt[l].u === '[Spector]') {
        message.tanks = [];
        var q = 0;
        while (q < this.pt.length) {
          if (((this.pt[l].x-860 > this.pt[q].x || this.pt[l].x+940 > this.pt[q].x) && (this.pt[l].x-860 < this.pt[q].x+80 || this.pt[l].x+940 < this.pt[q].x+80) && (this.pt[l].y-560 > this.pt[q].y || this.pt[l].y+640 > this.pt[q].y) && (this.pt[l].y-560 < this.pt[q].y+80 || this.pt[l].y+640 < this.pt[q].y+80)) || this.pt[l].u === '[Spector]') {
            message.tanks.push(JSON.parse(JSON.stringify(this.pt[q], (name, value) => {
              if (['updates', 'socket', 'render', 'healInterval', 'healTimeout', 'flashbangTimeout', 'grapple', 'gluInterval', 'ti', 'gluTimeout'].includes(name)) {
                return undefined;
              } else {
                return value;
              }
            })));
          }
          q++;
        }
        message.ai = [];
        var q = 0;
        while (q < this.ai.length) {
          // REVISE add culling
          message.ai.push(JSON.parse(JSON.stringify(this.ai[q], (name, value) => {
            if (['o', 'h', 'canFire', 'target', 'self_destruct'].includes(name)) {
              return undefined;
            } else {
              return value;
            }
          })));
          q++;
        }
        this.pt[l].updates.tank = false;
      }

      if (this.pt[l].updates.bullet || this.pt[l].u === '[Spector]' || true) {
        var q = 0;
        message.bullets = [];
        while (q < this.s.length) {
          if (((this.pt[l].x-860 > this.s[q].x || this.pt[l].x+940 > this.s[q].x) && (this.pt[l].x-860 < this.s[q].x+10 || this.pt[l].x+940 < this.s[q].x+10) && (this.pt[l].y-560 > this.s[q].y || this.pt[l].y+640 > this.s[q].y) && (this.pt[l].y-560 < this.s[q].y+10 || this.pt[l].y+640 < this.s[q].y+10)) || this.pt[l].u === '[Spector]') {
            message.bullets.push(JSON.parse(JSON.stringify(this.s[q], function(name, value) {
              if (['host', 'd', 'damage', 'ra', 'target', 'offset', 'settings', 'md'].includes(name)) {
                return undefined;
              } else {
                return value;
              }
            })));
          }
          q++;
        }
        q = 0;
        message.explosions = [];
        while (q < this.d.length) { // add culling
          message.explosions.push(JSON.parse(JSON.stringify(this.d[q], function(name, value) {
            if (['host', 'a', 'c'].includes(name)) {
              return undefined;
            } else {
              return value;
            }
          })));
          q++;
        }
        this.pt[l].updates.bullet = false;
      }

      if (this.pt[l].updates.logs || this.pt[l].u === '[Spector]') {
        message.logs = this.logs;
        this.pt[l].updates.logs = false;
      }

      if (Object.values(message).length !== 0) {
        message.event = 'hostupdate';
        this.pt[l].socket.send(JSON.stringify(message));
      }

      l++;
    }
  }

  damagePlayer(victim, damage) {
    if (victim.i) {
      return;
    }

    if (victim.s > 0) {
      victim.s -= damage.a;
      victim.s = Math.max(victim.s, 0);
      return;
    }

    if (victim.buff) {
      damage.a *= .8;
    }

    victim.h -= damage.a;

    if (victim.d !== false) {
      clearTimeout(victim.d.ti);
      victim.d.d += damage.a;
      victim.d.x = damage.x;
      victim.d.y = damage.y;
      victim.d.ti = setTimeout(function() {
        this.d = false;
      }.bind(victim), 1000);
    } else {
      victim.d = {
        d: damage.a,
        x: damage.x,
        y: damage.y,
        ti: setTimeout(function() {
          this.d = false;
        }.bind(victim), 1000),
      };
    }

    if (victim.h <= 0) {
      victim.ded = true;
      this.logs.push({
        m: this.deathMsg(victim.u, damage.o),
        c: '#FF8C00',
      });
      var l = 0;
      while (l < this.pt.length) {
        if (this.pt[l].u === damage.o) {
          this.pt[l].socket.send(JSON.stringify({
            event: 'kill',
          }));
        }
        this.pt[l].updates.logs = true;
        l++;
      }
      l = 0;
      while (l < this.ai.length) {
        if (this.ai[l].o.split(':')[0] === victim.u) {
          this.ai.splice(l, 1);
          l--;
        }
        l++;
      }
      var x = this.spawn.x;
      var y = this.spawn.y;
      setTimeout(() => {
        victim.socket.send(JSON.stringify({
          event: 'ded',
        }));
        victim.socket.send(JSON.stringify({
          event: 'override',
          data: [{
            key: 'x',
            value: x,
          }, {
            key: 'y',
            value: y,
          }],
        }));
        victim.ded = false;
        victim.h = victim.ma;
      }, 10000);
    }
    if (victim.h > victim.ma) {
      victim.h = victim.ma;
    }
  }

  deathMsg(victim, killer) {
    var junk = [
      '{VICTIM} was killed by {KILLER}',
      '{VICTIM} was put out of their misery by {KILLER}',
      '{VICTIM} was assasinated by {KILLER}',
      '{VICTIM} got comboed by {KILLER}',
      '{VICTIM} got eliminated by {KILLER}',
      '{VICTIM} was crushed by {KILLER}',
      '{VICTIM} had a skill issue while fighting {KILLER}',
      '{VICTIM} was sniped by {KILLER}',
      '{VICTIM} was exploded by {KILLER}',
      '{VICTIM} was executed by {KILLER}',
      '{VICTIM} was deleted by {KILLER}',
      '{VICTIM} was killed by THE PERSON WITH KILLSTREAK ---> {KILLER}',
      '{VICTIM} got buffed, but get nerfed harder by {KILLER}',
      '{VICTIM} got flung into space by {KILLER}',
      '{VICTIM} was pound into paste by {KILLER}',
      '{VICTIM} was fed a healty dose of explosives by {KILLER}',
      "{VICTIM} became another number in {KILLER}'s kill streak",
      "{VICTIM} got wrekt by {KILLER}",
      "{VICTIM} got hit by a tank driven by {KILLER}",
    ]
    return junk[Math.floor(Math.random() * junk.length)].replace('{VICTIM}', victim).replace('{KILLER}', killer);
  }

  joinMsg(player) {
    var junk = [
      '{IDOT} joined the game',
      '{IDOT} spawned',
      '{IDOT} joined',
      '{IDOT} wants a killsteak',
      '{IDOT} exists',
      "{IDOT} has been summoned",
      "{IDOT} is back",
      "a wild {IDOT} spawned",
      "{IDOT} is here to kill them all",
      "{IDOT} doesn't have anything better to do",
      "{IDOT} is here to fight to the death",
    ];
    return junk[Math.floor(Math.random() * junk.length)].replace('{IDOT}', player);
  }

  rageMsg(player) {
    var junk = [
      '{IDOT}.exe is unresponsive',
      '{IDOT} left the game',
      '{IDOT} ragequit',
      '{IDOT} stopped existing',
      '{IDOT} got away',
      'wild {IDOT} fled',
      "{IDOT} disconnected",
      "{IDOT} lost internet connection",
      "{IDOT} is not found",
    ];
    return junk[Math.floor(Math.random() * junk.length)].replace('{IDOT}', player);
  }

  disconnect(socket, code, reason) {
    this.sockets.splice(this.sockets.indexOf(socket), 1);
    var l = 0;
    while (l < this.pt.length) {
      if (this.pt[l].u === socket.username) {
        this.pt.splice(l, 1);
      }
      l++;
    }
    if (this.pt.length === 0) {
      var l = 0;
      while (l < this.i.length) {
        clearInterval(this.i[l]);
        l++;
      }
      l = 0;
      while (l < this.t.length) {
        clearTimeout(this.t[l]);
        l++;
      }
      if (servers.length - 1 === servers.indexOf(this)) {
        servers = [];
      } else {
        servers[servers.indexOf(this)] = new FFA();
      }
      return;
    }
    this.logs.push({
      m: this.rageMsg(socket.username),
      c: '#E10600',
    });
    var l = 0;
    while (l < this.pt.length) {
      this.pt[l].updates.logs = true;
      l++;
    }
  }
}

class Block {
  constructor(x, y, health, type, owner, host) {
    this.x = x;
    this.y = y;
    this.m = health; // max health
    this.h = health; // health
    this.t = type; // type
    this.ho = host; // host
    this.s = false; // show health bar
    this.c = true; // collision
    this.o = owner;
    if (type === 'spike' || type === 'mine') {
      this.c = false;
      this.sd = setTimeout(function() {
        this.destroy();
      }.bind(this), 30000);
    }
    if (type === 'airstrike') {
      this.c = false;
      var l = 0;
      while (l<20) {
        setTimeout(function(b) {
          this.d.push(new Damage(b.x+Math.random()*200, b.y+Math.random()*200, 200, 200, 200, b.o, this));
          b.destroy();
        }.bind(this.ho), 5000+Math.random()*500, this);
        l++;
      }
    }
    if (type === 'mine') {
      this.a = false; // armed or not
      setTimeout(() => {
        this.a = true;
      }, 3000);
    }
  }

  damage(d) {
    this.ho.triggerUpdate(this.x, this.y, 'block');

    this.h -= d;

    if (this.h !== Infinity) {
      this.s = true;
      clearTimeout(this.bar);
      this.bar = setTimeout(function() {
        this.s = false;
        this.ho.triggerUpdate(this.x, this.y, 'block');
      }.bind(this), 3000);
    }

    if (this.h <= 0) {
      this.destroy();
      return true;
    }
    if (this.h > this.m) {
      this.h = this.m;
    }
    return false;
  }

  destroy() {
    clearTimeout(this.sd);
    if (this.t === 'mine') this.ho.d.push(new Damage(this.x, this.y, 100, 100, 100, this.o, this.ho));
    if (this.ho.b.indexOf(this) !== -1) this.ho.b.splice(this.ho.b.indexOf(this), 1);
  }
}

class Shot {
  constructor(x, y, xm, ym, t, r, u, h) {
    this.settings = {
      damage: {
        bullet: 20,
        shotgun: 20,
        grapple: 0,
        powermissle: 100,
        megamissle: 200,
        healmissle: -100,
        dynamite: 0,
      },
      speed: {
        bullet: 1,
        shotgun: .8,
        grapple: 2,
        powermissle: 1.5,
        megamissle: 1.5,
        healmissle: 1.5,
        dynamite: .8,
      }
    }

    this.d = 0;
    this.u = u;
    this.r = r;
    this.t = t;
    this.sx = x;
    this.sy = y;
    this.host = h;
    this.e = new Date().getTime();

    var d = 18;
    this.xm = xm * (d / Math.sqrt(xm * xm + ym * ym));
    this.ym = ym * (d / Math.sqrt(xm * xm + ym * ym));
    var data = Shot.calc(x, y, xm, ym);
    this.x = data.x;
    this.y = data.y;

    if (xm == 0 && ym == 1) {
      this.x = x;
      this.y = 35 + y;
    } else if (xm == -1 && ym == 0) {
      this.x = -35 + x;
      this.y = y;
    } else if (xm == 0 && ym == -1) {
      this.x = x;
      this.y = -35 + y;
    } else if (xm == 1 && ym == 0) {
      this.x = 35 + x;
      this.y = y;
    }

    var l = 0;
    while (l < this.host.pt.length) {
      if (this.host.pt[l].u === this.u.split(':')[0]) {
        this.damage = this.settings.damage[this.t]/500*this.host.pt[l].ma;
        if (this.host.pt[l].buff) this.damage *= 1.3;
      }
      l++;
    }
    this.md = this.damage; // original damage or max damage for shotgun, used for calculating dmg downgradation over distance
    this.xm *= this.settings.speed[this.t];
    this.ym *= this.settings.speed[this.t];
  }

  static calc(x, y, xm, ym) {
    var sgn, x, y;
    if (((ym / xm) * -20 - (ym / xm) * 20) < 0) {
      sgn = -1;
    } else {
      sgn = 1;
    }
    var x1 = ((20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20) + sgn * -40 * Math.sqrt(1000 * (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20))) - ((20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * (20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20)))) / (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)));
    var x2 = ((20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20) - sgn * -40 * Math.sqrt(1000 * (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20))) - ((20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * (20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20)))) / (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)));
    var y1 = (-(20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * -40 + Math.abs(((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(1000 * (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20))) - ((20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * (20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20)))) / (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)));
    var y2 = (-(20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * -40 - Math.abs(((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(1000 * (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20))) - ((20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20) * (20 * (ym / xm) * -20 - (-20) * (ym / xm) * 20)))) / (Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)) * Math.sqrt(-40 * -40 + ((ym / xm) * -20 - (ym / xm) * 20) * ((ym / xm) * -20 - (ym / xm) * 20)));
    if (ym >= 0) {
      x = x1*2 + x;
      y = y1*2 + y;
    } else {
      x = x2*2 + x;
      y = y2*2 + y;
    }
    return {
      x: x,
      y: y,
    };
  }

  collision() {
    var x = this.x;
    var y = this.y;
    var key = {
      bullet: false,
      shotgun: false,
      powermissle: 100,
      megamissle: 150,
      grapple: false,
      healmissle: 20,
      dynamite: false,
    }

    var l = 0;
    while (l < this.host.pt.length) {
      if ((x > this.host.pt[l].x || x + 10 > this.host.pt[l].x) && (x < this.host.pt[l].x + 80 || x + 10 < this.host.pt[l].x + 80)) {
        if ((y > this.host.pt[l].y || y + 10 > this.host.pt[l].y) && (y < this.host.pt[l].y + 80 || y + 10 < this.host.pt[l].y + 80)) {
          if (!this.host.pt[l].ded) {
            if (this.t === 'grapple') {
              if (this.host.pt[l].grapple) {
                this.host.pt[l].grapple.bullet.destroy();
              }
              var target;
              var q = 0;
              while (q<this.host.pt.length) {
                if (this.host.pt[q].u === this.u) {
                  target = this.host.pt[q];
                }
                q++;
              }
              this.host.pt[l].grapple = {
                target: target,
                bullet: this,
              }
              this.update = () => {};
              return false;
            } else if (this.t === 'dynamite') {
              this.target = this.host.pt[l];
              this.offset = [this.host.pt[l].x-this.x, this.host.pt[l].y-this.y];
              this.update = function() {
                this.x = this.target.x-this.offset[0];
                this.y = this.target.y-this.offset[1];
              }
              this.update.bind(this);
              return false;
            }
            if (key[this.t]) {
              this.host.d.push(new Damage(this.x - key[this.t] / 2 + 10, this.y - key[this.t] / 2 + 10, key[this.t], key[this.t], this.damage, this.u, this.host));
            } else if (this.host.pt[l].t.includes('@requestor#') ? this.host.pt[l].t.split('@requestor#')[0].split(':')[1].replace('@leader', '') : this.host.pt[l].t.split(':')[1].replace('@leader', '') !== this.u.split(':')[1]) {
              this.host.damagePlayer(this.host.pt[l], {
                a: this.damage,
                x: this.x,
                y: this.y,
                o: this.u.split(':')[0],
              });
            }
            return true;
          }
        }
      }
      l++;
    }

    var l = 0;
    while (l < this.host.ai.length) {
      if ((x > this.host.ai[l].x || x + 10 > this.host.ai[l].x) && (x < this.host.ai[l].x + 80 || x + 10 < this.host.ai[l].x + 80)) {
        if ((y > this.host.ai[l].y || y + 10 > this.host.ai[l].y) && (y < this.host.ai[l].y + 80 || y + 10 < this.host.ai[l].y + 80)) {
            if (this.t === 'dynamite') {
              this.target = this.host.ai[l];
              this.offset = [this.host.ai[l].x-this.x, this.host.ai[l].y-this.y];
              this.update = function() {
                this.x = this.target.x-this.offset[0];
                this.y = this.target.y-this.offset[1];
              }
              this.update.bind(this);
              return false;
            }
          if (key[this.t]) {
            this.host.d.push(new Damage(this.x - key[this.t] / 2 + 10, this.y - key[this.t] / 2 + 10, key[this.t], key[this.t], this.damage, this.u, this.host));
          } else if (this.host.ai[l].o !== this.u) {
            this.host.ai[l].damage(this.damage);
          }
          return true;
        }
      }
      l++;
    }

    var offMap = false;
    if (x < 0 || y < 0 || x + 10 > 3000 || y + 10 > 3000) offMap = true;
    var l = 0;
    while (l < this.host.b.length) {
      if (((x > this.host.b[l].x || x + 5 > this.host.b[l].x) && (x < this.host.b[l].x + 100 || x + 5 < this.host.b[l].x + 100)) || offMap) {
        if (((y > this.host.b[l].y || y + 5 > this.host.b[l].y) && (y < this.host.b[l].y + 100 || y + 5 < this.host.b[l].y + 100)) || offMap) {
          if (this.host.b[l].c) {
            if (this.t === 'grapple') {
              var q = 0;
              while (q < this.host.pt.length) {
                if (this.host.pt[q].u === this.u) {
                  if (this.host.pt[q].grapple) {
                    this.host.pt[q].grapple.bullet.destroy();
                  }
                  this.host.pt[q].grapple = {
                    target: this.host.b[l],
                    bullet: this,
                  }
                }
                q++;
              }
              this.update = () => {};
              return false;
            }  else if (this.t === 'dynamite') {
              this.target = this.host.b[l];
              this.offset = [this.host.b[l].x-this.x, this.host.b[l].y-this.y];
              this.update = function() {
                this.x = this.target.x-this.offset[0];
                this.y = this.target.y-this.offset[1];
              }
              this.update.bind(this);
              return false;
            } else if (this.host.b[l].t === 'fortress' && this.host.b[l].o.split(':')[1] === this.u.split(':')[1]) {} else {
              if (key[this.t]) {
                this.host.d.push(new Damage(this.x - key[this.t] / 2 + 10, this.y - key[this.t] / 2 + 10, key[this.t], key[this.t], this.damage, this.u, this.host));
              } else {
                this.host.b[l].damage(this.damage);
              }
              return true;
            }
          }
        }
      }
      l++;
    }

    if (x < 0 || x > 3000 || y < 0 || y > 3000) {
      return true;
    }
    return false;
  }

  update() {

    var l = 0;
    while (l < this.host.pt.length) {
      if ((this.host.pt[l].x-860 > this.x || this.host.pt[l].x+940 > this.x) && (this.host.pt[l].x-860 < this.x+10 || this.host.pt[l].x+940 < this.x+10) && (this.host.pt[l].y-560 > this.y || this.host.pt[l].y+640 > this.y) && (this.host.pt[l].y-560 < this.y+10 || this.host.pt[l].y+640 < this.y+10)) {
        this.host.pt[l].updates.bullet = true;
      }
      l++;
    }

    this.x += this.xm;
    this.y += this.ym;
    if (this.t === 'dynamite') this.r += 5;
    if (this.collision()) {
      this.destroy();
      return;
    }

    this.d += Math.sqrt(Math.pow(this.xm, 2) + Math.pow(this.ym, 2));

    if (this.t === 'shotgun') {
      this.damage = this.md-(this.d/300)*this.md;
      if (this.d >= 300) {
        this.destroy();
      }
    }
  }

  destroy() {
    if (this.host.s.indexOf(this) !== -1) this.host.s.splice(this.host.s.indexOf(this), 1);
  }
}

class Damage {
  constructor(x, y, w, h, a, o, host) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.a = a;
    this.o = o;
    this.c = true;
    this.host = host;
    this.f = 0;
    setInterval(function() {
      this.host.triggerUpdate(this.x, this.y, 'bullet');
      this.f++;
    }.bind(this), 18);
    setTimeout(function() {
      this.destroy();
    }.bind(this), 200);
  }

  destroy() {
    this.host.triggerUpdate(this.x, this.y, 'bullet');
    if (this.host.d.indexOf(this) !== -1) this.host.d.splice(this.host.d.indexOf(this), 1);
  }
}

class Ai {
  constructor(x, y, type, owner, host) {
    /*
      0 -> turret
      1 -> normal
      2 -> shotgun
    */
    this.x = x;
    this.y = y;
    this.r = 0;
    this.o = owner;
    this.h = host;
    this.hp = 600;
    this.p = 0;
    this.setup = false;
    setTimeout(function() {
      this.setup = true;
    }.bind(this), 1000);
    if (type === 0) {
      this.turret = true;
    } else {
      this.turret = false;
    }
    this.canFire = true;
    var l = 0;
    while (l < this.h.pt.length) {
      if (this.h.pt[l].u === this.o) {
        this.c = this.h.pt[l].co;
      }
      l++;
    }
  }

  update() {
    if (!this.setup) return;
    if (!this.identify()) {
      return
    };
    this.aim();
    if (this.canFire) {
      this.fire();
      this.canFire = false;
      setTimeout(function() {
        this.canFire = true;
      }.bind(this), 300);
    }
  }

  toAngle(x, y) {
    var angle = Math.atan2(x, y) * 180 / Math.PI
    angle = -angle //-90;
    if (angle < 0) {
      angle += 360;
    }
    if (angle >= 360) {
      angle -= 360;
    }
    return angle;
  }

  toPoint(angle) {
    var theta = (-angle) * Math.PI / 180;
    var y = Math.cos(theta);
    var x = Math.sin(theta);
    if (x < 0) {
      if (y < 0) {
        return {
          x: -1,
          y: Math.round(-Math.abs(y / x) * 1000) / 1000,
        };
      } else {
        return {
          x: -1,
          y: Math.round(Math.abs(y / x) * 1000) / 1000,
        };
      }
    } else {
      if (y < 0) {
        return {
          x: 1,
          y: Math.round(-Math.abs(y / x) * 1000) / 1000,
        };
      } else {
        return {
          x: 1,
          y: Math.round(Math.abs(y / x) * 1000) / 1000,
        };
      }
    }
  }

  identify() {
    var l = 0,
      targets = [];
    while (l < this.h.pt.length) {
      if (this.h.pt[l].t.split(':')[1].replace('@leader', '') !== this.o.split(':')[1].replace('@leader', '') && !this.h.pt[l].ded && (!this.h.pt[l].in || this.hp !== 400)) {
        var x = this.h.pt[l].x - this.x;
        var y = this.h.pt[l].y - this.y;
        var distance = Math.sqrt(x * x + y * y);
        targets.push({
          idot: this.h.pt[l],
          distance: distance,
        });
      }
      l++;
    }
    l = 0;
    while (l < this.h.ai.length) {
      if (this.h.ai[l].o !== this.o) {
        var x = this.h.ai[l].x - this.x;
        var y = this.h.ai[l].y - this.y;
        var distance = Math.sqrt(x * x + y * y);
        targets.push({
          idot: this.h.ai[l],
          distance: distance,
        });
      }
      l++;
    }
    this.h.triggerUpdate(this.x, this.y, 'tank');
    if (targets.length === 0) {
      this.r += 1;
      return false; // no idots :(
    }
    targets.sort((a, b) => {
      return a.distance - b.distance;
    }); // sort array to closest target
    if (targets[0].distance > 1000) {
      this.r += 1;
      return false; // out of range
    }
    this.target = targets[0].idot; // select closest idot
    return true;
  }

  aim() {
    this.r = this.toAngle(this.target.x - this.x, this.target.y - this.y);
  }

  fire() {
    this.p = -3;
    var data = this.toPoint(this.r);
    this.h.s.push(new Shot(this.x + 40, this.y + 40, data.x, data.y, 'bullet', 0, this.o, this.h));
  }

  damage(d) {
    this.hp -= d;
    if (this.hp <= 0) {
      this.h.ai.splice(this.h.ai.indexOf(this), 1);
    }
  }
}

if (!SETTINGS.use_existing_node_server) {
  FFA_SERVER.listen(80);
}

export {
  FFA_SERVER
};
