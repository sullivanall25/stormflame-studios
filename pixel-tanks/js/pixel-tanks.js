(() => {

  class MegaSocket {
    constructor(url, options) {
      this.url = url;
      this.options = options || {};
      this.callstack = {
        open: [],
        close: [],
        message: [],
      }
      if (this.options.keepAlive == undefined) this.options.keepAlive = true;
      if (this.options.autoconnect == undefined) this.options.autoconnect = true;
      if (this.options.reconnect == undefined) this.options.reconnect = true;
      if (this.options.autoconnect) {
        this.status = 'connecting';
        this.connect();
      } else {
        this.status = 'idle';
        window.addEventListener('offline', function() { // Arrow Function Wont Work Due To Bind
          this.socket.close();
          this.socket.onclose();
        }.bind(this));
        if (this.options.reconnect) {
          window.addEventListener('online', function() {
            this.connect();
          }.bind(this))
        }
      }
    }
    connect() {
      try {
        this.socket = new WebSocket(this.url);
      } catch (e) {
        console.warn('WebSocket had trouble connecting to ' + this.url);
      }
      this.socket.onopen = function() {
        this.status = 'connected';
        if (this.options.keepAlive) {
          this.socket.keepAlive = setInterval(function() {
            this.socket.send('|');
          }.bind(this), 50000);
        }
        var l = 0;
        while (l < this.callstack.open.length) {
          this.callstack.open[l]();
          l++;
        }
      }.bind(this);
      this.socket.onmessage = function(data) {
        try {
          data = window.jsonpack.unpack(Helper.de(data.data));
        } catch (e) {
          alert('Error converting jsonpack from websocket message: ' + Helper.de(data.data));
        }
        // custom error handling for server
        if (data.status === 'error') {
          alert('MegaSocket.RemoteRejection: '+data.message);
          return;
        }
        // end
        var l = 0;
        while (l < this.callstack.message.length) {
          this.callstack.message[l](data);
          l++;
        }
      }.bind(this);
      this.socket.onclose = function() {
        clearInterval(this.socket.keepAlive);
        this.status = 'disconnected';
        var l = 0;
        while (l < this.callstack.close.length) {
          this.callstack.close[l]();
          l++;
        }
        if (this.options.reconnect) {
          this.connect();
        }
      }.bind(this);
    }
    on(event, operation) {
      if (event == 'connect') {
        this.callstack.open.push(operation);
      }
      if (event == 'message') {
        this.callstack.message.push(operation);
      }
      if (event == 'close') {
        this.callstack.close.push(operation);
      }
    }
    no(event) {
      if (event == 'connect') {
        this.callstack.open = [];
      }
      if (event == 'message') {
        this.callstack.message = [];
      }
      if (event == 'close') {
        this.callstack.close = [];
      }
    }
    send(data) {
      data = Helper.en(window.jsonpack.pack(data));
      this.socket.send(data);
    }
    close() {
      this.socket.close();
    }
  }

  class Helper {
    static en(c) {var x='charCodeAt',b,e={},f=c.split(""),d=[],a=f[0],g=256;for(b=1;b<f.length;b++)c=f[b],null!=e[a+c]?a+=c:(d.push(1<a.length?e[a]:a[x](0)),e[a+c]=g,g++,a=c);d.push(1<a.length?e[a]:a[x](0));for(b=0;b<d.length;b++)d[b]=String.fromCharCode(d[b]);return d.join("")}

    static de(b) {var a,e={},d=b.split(""),c=d[0],f=d[0],g=[c],h=256,o=256;for(b=1;b<d.length;b++)a=d[b].charCodeAt(0),a=h>a?d[b]:e[a]?e[a]:f+c,g.push(a),c=a.charAt(0),e[o]=f+c,o++,f=a;return g.join("")}
  }

  class Menu {
    constructor(draw, listeners, element, context) {
      this.draw = draw.bind(this);
      this.element = element;
      this.listeners = listeners;
      if (context) {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(context);
        }
      } else {
        for (let property in this.listeners) {
          this.listeners[property] = this.listeners[property].bind(this);
        }
      }
    }

    addListeners() {
      for (let property in this.listeners) {
        this.element.addEventListener(property, this.listeners[property]);
      }
    }
    removeListeners() {
      for (let property in this.listeners) {
        this.element.removeEventListener(property, this.listeners[property]);
      }
    }
  }

  class Menus {
    static trigger(name) {
      if (Menus.current !== undefined) {
        Menus.menus[Menus.current].removeListeners();
      }
      Menus.current = name;
      GUI.clear();
      Menus.menus[Menus.current].draw();
      Menus.menus[Menus.current].addListeners();
    }

    static onclick(e) {
      var x = (e.clientX-(window.innerWidth-window.innerHeight*1.6)/2)/PixelTanks.resizer;
      var y = e.clientY/PixelTanks.resizer;
      var l = 0;
      while (l < this.data.buttons.length) {
        if (x > this.data.buttons[l][0] && x < this.data.buttons[l][0] + this.data.buttons[l][2]) {
          if (y > this.data.buttons[l][1] && y < this.data.buttons[l][1] + this.data.buttons[l][3]) {
            Menus.trigger(this.data.buttons[l][4]);
          }
        }
        l++;
      }
      var l = 0;
      while (l < this.data.exec.length) {
        if (x > this.data.exec[l][0] && x < this.data.exec[l][0] + this.data.exec[l][2]) {
          if (y > this.data.exec[l][1] && y < this.data.exec[l][1] + this.data.exec[l][3]) {
            eval(this.data.exec[l][4]);
          }
        }
        l++;
      }
    }

    static redraw() {
      GUI.clear();
      if (Menus.current) Menus.menus[Menus.current].draw();
    }

    static removeListeners() {
      Menus.menus[Menus.current].removeListeners();
    }

  }

  class Network {
    static get(callback) {
      PixelTanks.socket.send({
        op: 'database',
        type: 'get',
        username: sessionStorage.username,
        token: sessionStorage.token,
      });
      PixelTanks.socket.on('message', (data) => {
        if (data.status === 'success' && data.type === 'get') {
          PixelTanks.socket.no('message');
          callback(JSON.parse(data.data));
        }
      });
    }
    static update(key, value) {
      try {
        PixelTanks.socket.send({
          op: 'database',
          type: 'set',
          username: sessionStorage.username,
          token: sessionStorage.token,
          key: key,
          value: value,
        });
        PixelTanks.socket.on('message', function(data) {
          if (data.success) {
            PixelTanks.socket.no('message');
            console.log('Saved Game Successfully!');
          }
        });
      } catch (e) {}
    }
    static auth(username, password, type, callback) {
      PixelTanks.socket.send({
        op: 'auth',
        type: type,
        username: username,
        password: password,
      });
      PixelTanks.socket.on('message', (data) => {
        if (data.status === 'success') {
          PixelTanks.socket.no('message');
          sessionStorage.username = username;
          sessionStorage.token = data.token;
          callback();
        }
      })
    }
  }

  class GUI {
    
    static resize() {
      PixelTanks.resizer = window.innerHeight/1000;
      GUI.root.height = window.innerHeight;
      GUI.root.width = window.innerHeight*1.6;
      GUI.root.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.root.scale.set(PixelTanks.resizer);
      GUI.draw.scale.set(PixelTanks.resizer);
      GUI.draw.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.sidebarA.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarA.height = window.innerHeight;
      GUI.sidebarB.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarB.height = window.innerHeight;
      GUI.sidebarB.x = window.innerWidth-GUI.sidebarB.width;
      Menus.redraw();
    }

    static resetSpritePool() {
      GUI.nextsprite = 0;
    }

    static resetTextPool() {
      GUI.nexttext = 0;
    }

    static drawImage(image, x, y, w, h, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      sprite.texture = image;
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.alpha = a;
      sprite.pivot.x = 0;
      sprite.pivot.y = 0;
      sprite.angle = 0;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawImageCrop(image, x, y, w, h, cx, cy, cw, ch, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      // generating new textures and rectangles every frame could cause memory leak issues
      sprite.texture = new PIXI.Texture(image.baseTexture, new PIXI.Rectangle(cx, cy, cw, ch), image.orig, image.trim);
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.alpha = a;
      sprite.pivot.x = 0;
      sprite.pivot.y = 0;
      sprite.angle = 0;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawImageRotate(image, x, y, w, h, px, py, r, a) {
      var sprite = GUI.spritepool[GUI.nextsprite];
      sprite.texture = image;
      sprite.x = x;
      sprite.y = y;
      sprite.width = w;
      sprite.height = h;
      sprite.pivot.x = px;
      sprite.pivot.y = py;
      sprite.angle = r;
      sprite.alpha = a;
      GUI.root.addChild(sprite);
      GUI.nextsprite++;
      if (GUI.nextsprite === GUI.spritepool.length) GUI.resetSpritePool();
    }

    static drawText(message, x, y, size, color, anchor) {
      var text = GUI.textpool[GUI.nexttext];
      text.text = message;
      text.style = {
        fill: color,
        fontFamily: 'Font',
        fontWeight: 300,
        fontSize: size,
      };
      text.anchor.set(anchor);
      text.x = x;
      text.y = y;
      GUI.nexttext++;
      GUI.root.addChild(text);
      if (GUI.nexttext === GUI.textpool.length) GUI.resetTextPool();
    }

    static clear() {
      while (GUI.root.children[0]) {
        GUI.root.removeChild(GUI.root.children[0]);
      }
      GUI.draw.clear();
    }
  }

  class PixelTanks {

    static start() {
      PixelTanks.setup();
      PixelTanks.boot() // Load all variables, images, sockets, and other stuff for launch
    }

    static setup() {
      PIXI.settings.SCALE_MODE = PIXI.SCALE_MODES.NEAREST;
      const ticker = PIXI.Ticker.shared;
      ticker.autoStart = false;
      ticker.stop();
      GUI.spritepool = [];
      GUI.textpool = [];
      GUI.fontstyle = [];
      GUI.fontstyle[30] = new PIXI.TextStyle({fontFamily: 'Font', fontSize: 30, align: 'center'});
      GUI.fontstyle[50] = new PIXI.TextStyle({fontFamily: 'Font', fontSize: 50, align: 'center'});
      var l = 0;
      while (l<500) {
        GUI.spritepool.push(new PIXI.Sprite());
        l++;
      }
      l = 0;
      while (l<100) {
        GUI.textpool.push(new PIXI.Text());
        l++;
      }
      GUI.nextsprite = 0;
      GUI.nexttext = 0;
      GUI.app = new PIXI.Application({
        resizeTo: window,
        backgroundColor: '#000000',
        antialias: true,
      });
      document.body.appendChild(GUI.app.view);

      PixelTanks.resizer = window.innerHeight/1000;
      GUI.root = new PIXI.Container();
      GUI.root.height = window.innerHeight;
      GUI.root.width = window.innerHeight*1.6;
      GUI.root.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.root.scale.set(PixelTanks.resizer);
      GUI.app.stage.addChild(GUI.root);
      GUI.draw = new PIXI.Graphics();
      GUI.draw.scale.set(PixelTanks.resizer);
      GUI.draw.position.set((window.innerWidth-window.innerHeight*1.6)/2, 0);
      GUI.app.stage.addChild(GUI.draw);
      GUI.sidebarA = PIXI.Sprite.from(PIXI.Texture.WHITE);
      GUI.sidebarA.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarA.height = window.innerHeight;
      GUI.sidebarA.x = 0
      GUI.sidebarA.y = 0;
      GUI.sidebarA.zIndex = 1000;
      GUI.sidebarA.tint = 0x000000;
      GUI.app.stage.addChild(GUI.sidebarA);
      GUI.sidebarB = PIXI.Sprite.from(PIXI.Texture.WHITE);
      GUI.sidebarB.width = Math.abs((window.innerWidth-window.innerHeight*1.6)/2);
      GUI.sidebarB.height = window.innerHeight;
      GUI.sidebarB.x = window.innerWidth-GUI.sidebarB.width;
      GUI.sidebarB.y = 0;
      GUI.sidebarB.zIndex = 1000;
      GUI.sidebarB.tint = 0x000000;
      GUI.app.stage.addChild(GUI.sidebarB);

      GUI.drawText('0%', 800, 500, 50, '#ffffff', 0.5);

      window.oncontextmenu = () => {
        return false;
      }
  
      window.addEventListener('resize', GUI.resize);
    }

    static updateBootProgress(progress) {
      GUI.clear();
      GUI.drawText(Math.round(progress*100)+'%', 800, 500, 50, '#ffffff', 0.5);
    }

    static async boot() {
      
      // variable setup
      PixelTanks.texturepack = 'default'; // texturepack
      PixelTanks.user = {};
      // setup sprite map
      PixelTanks.images = { // mapping
        blocks: {
          default: {
            barrier: '/images/blocks/default/barrier.png',
            strong: '/images/blocks/default/strong.png',
            weak: '/images/blocks/default/weak.png',
            spike: '/images/blocks/default/spike.png',
            floor: '/images/blocks/default/floor.png',
            void: '/images/blocks/default/void.png',
            gold: '/images/blocks/default/gold.png',
            heal: '/images/blocks/default/heal.png',
            mine: '/images/blocks/default/mine.png',
            airstrike: '/images/blocks/default/airstrike.png',
            fortress: '/images/blocks/fortress.png',
          },
        },
        bullets: {
          default: {
            // normal: '/images/bullets/default/normal.png', no image yet :(
            shotgun: '/images/bullets/default/shotgun.png',
            condensed: '/images/bullets/default/condensed.png',
            powermissle: '/images/bullets/default/powermissle.png',
            megamissle: '/images/bullets/default/megamissle.png',
            grapple: '/images/bullets/default/grapple.png',
            explosion: '/images/bullets/default/explosion.png',
            dynamite: '/images/bullets/default/dynamite.png',
          },
        },
        tanks: {
          default: {
            other: {
              buff: '/images/tanks/default/other/buff.png',
            },
            red: {
              top: '/images/tanks/default/red/top.png',
              bottom: ['/images/tanks/default/red/bottom.png', '/images/tanks/default/red/bottom2.png'],
              base: '/images/tanks/default/red/base.png'
            },
            steel: {
              top: '/images/tanks/default/iron/top.png',
              bottom: ['/images/tanks/default/iron/bottom.png', '/images/tanks/default/iron/bottom2.png'],
            },
            crystal: {
              top: '/images/tanks/default/diamond/top.png',
              bottom: ['/images/tanks/default/diamond/bottom.png', '/images/tanks/default/diamond/bottom2.png'],
            },
            dark: {
              top: '/images/tanks/default/dark/top.png',
              bottom: ['/images/tanks/default/dark/bottom.png', '/images/tanks/default/dark/bottom2.png'],
            },
            light: {
              top: '/images/tanks/default/light/top.png',
              bottom: ['/images/tanks/default/light/bottom.png', '/images/tanks/default/light/bottom2.png'],
            },
            cosmetics: {
              'Aaron': 'aaron',
              'Astronaut': 'astronaut',
              'Onfire': 'onfire',
              'Assassin': 'assassin',
              'Redsus': 'redsus',
              'Venom': 'venom',
              'Blue Tint': 'blue_tint',
              'Purple Flower': 'purple_flower',
              'Leaf': 'leaf',
              'Basketball': 'basketball',
              'Purple Top Hat': 'purple_top_hat',
              'Terminator': 'terminator',
              'Dizzy': 'dizzy',
              'Knife': 'knife',
              'Scared': 'scared',
              'Laff': 'laff',
              'Hacker Hoodie': 'hacker_hoodie',
              'Error': 'error',
              'Purple Grad Hat': 'purple_grad_hat',
              'Bat Wings': 'bat_wings',
              'Back Button': 'back',
              'Fisher Hat': 'fisher_hat',
              'Ban': 'ban',
              'Blue Ghost': 'blue_ghost',
              'Pumpkin Face': 'pumpkin_face',
              'Pumpkin Hat': 'pumpkin_hat',
              'Red Ghost': 'red_ghost',
              'Candy Corn': 'candy_corn',
              'Yellow Pizza': 'yellow_pizza',
              'Orange Ghost': 'orange_ghost',
              'Pink Ghost': 'pink_ghost',
              'Paleontologist': 'paleontologist',
              'Yellow Hoodie': 'yellow_hoodie',
              'X': 'x',
              'Sweat': 'sweat',
              'Spirals': 'spirals',
              'Spikes': 'spikes',
              'Rudolph': 'rudolph',
              'Reindeer Hat': 'reindeer_hat',
              'Red Hoodie': 'red_hoodie',
              'Question Mark': 'question_mark',
              'Pink/Purple Hoodie': 'purplepink_hoodie',
              'Purple Hoodie': 'purple_hoodie',
              'Pumpkin': 'pumpkin',
              'Pickle': 'pickle',
              'Orange Hoodie': 'orange_hoodie',
              'Helment': 'helment',
              'Green Hoodie': 'green_hoodie',
              'Exclaimation Point': 'exclaimation_point',
              'Eggplant': 'eggplant',
              'Devils Wings': 'devils_wings',
              'Christmas Tree': 'christmas_tree',
              'Christmas Lights': 'christmas_lights',
              'Checkmark': 'checkmark',
              'Cat Hat': 'cat_hat',
              'Blueberry': 'blueberry',
              'Blue Hoodie': 'blue_hoodie',
              'Blue Helment': 'blue_helment',
              'Banana': 'bannana',
              'Aqua Helment': 'aqua_helment',
              'Apple': 'apple',
              'Hoodie': 'hoodie',
              'Purple Helment': 'purple_helment',
              'Angel Wings': 'angel_wings',
              'Boost': 'boost',
              'Bunny Ears': 'bunny_ears',
              'Cake': 'cake',
              'Cancelled': 'cancelled',
              'Candy Cane': 'candy_cane',
              'Cat Ears': 'cat_ears',
              'Christmas Hat': 'christmas_hat',
              'Controller': 'controller',
              'Deep Scratch': 'deep_scratch',
              'Devils Horns': 'devil_horn',
              'Headphones': 'earmuffs',
              'Eyebrows': 'eyebrows',
              'First Aid': 'first_aid',
              'Flag': 'flag',
              'Halo': 'halo',
              'Hacks': 'hax',
              'Low Battery': 'low_battery',
              'Mini Tank': 'mini_tank',
              'MLG Glasses': 'mlg_glasses',
              'Money Eyes': 'money_eyes',
              'No Mercy': 'no_mercy',
              'Peace': 'peace',
              'Police': 'police',
              'Question Mark': 'question_mark',
              'Rage': 'rage',
              'Small Scratch': 'small_scratch',
              'Speaker': 'speaker',
              'Swords': 'swords',
              'Tools': 'tools',
              'Top Hat': 'top_hat',
              'Uno Reverse': 'uno_reverse',
              'Victim': 'victim',
            },
          },
          // more texture packs
        },
        menus: {
          default: {
            ui: '/images/menus/default/ui.png',
            start: '/images/menus/default/start.png',
            main: '/images/menus/default/main.png',
            multiplayer: '/images/menus/default/multiplayer.png',
            pvp: '/images/menus/default/pvp.png',
            ffa: '/images/menus/default/ffa.png',
            crate: '/images/menus/default/crate.png',
            cosmeticLeft: '/images/menus/default/cosmetics_left.png',
            cosmeticCenter: '/images/menus/default/cosmetics_middle.png',
            cosmeticRight: '/images/menus/default/cosmetics_right.png',
            defeat: '/images/menus/default/defeat.png',
            victory: '/images/menus/default/victory.png',
            settings: '/images/menus/default/settings.png',
            keybinds: '/images/menus/default/keybinds.png',
            inventory: '/images/menus/default/inventory.png',
            classTab: '/images/menus/default/classTab.png',
            healthTab: '/images/menus/default/healthTab.png',
            itemTab: '/images/menus/default/itemTab.png',
            shop_armor: '/images/menus/default/shop_armor.png',
            shop_class: '/images/menus/default/shop_class.png',
            shop_items: '/images/menus/default/shop_items.png',
            shop_kits: '/images/menus/default/shop_kits.png',
          },
        },
        emotes: { // type: 0=loop 1=play once 2=static
          speech: {
            image: '/images/emotes/speech.png',
            speed: 50,
          },
          mlg: {
            image: '/images/emotes/mlg.png',
            type: 1,
            frames: 13,
            speed: 50,
          },
          wink: {
            image: '/images/emotes/wink.png',
            type: 2,
            speed: 50,
          },
          confuzzled: {
            image: '/images/emotes/confuzzled.png',
            type: 2,
            speed: 50,
          },
          surrender: {
            image: '/images/emotes/surrender.png',
            type: 2,
            speed: 50,
          },
          anger: {
            image: '/images/emotes/anger.png',
            type: 0,
            frames: 4,
            speed: 50,
          },
          ded: {
            image: '/images/emotes/ded.png',
            type: 2,
            speed: 50,
          },
          mercy: {
            image: '/images/emotes/mercy.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          suffocation: {
            image: '/images/emotes/suffocation.png',
            type: 0,
            frames: 3,
            speed: 50,
          },
          nomercy: {
            image: '/images/emotes/nomercy.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          idea: {
            image: '/images/emotes/idea.png',
            type: 1,
            frames: 6,
            speed: 50,
          },
          scared: {
            image: '/images/emotes/scared.png',
            type: 2,
            speed: 50,
          },
          crying: {
            image: '/images/emotes/crying.png',
            type: 0,
            frames: 5,
            speed: 50,
          },
          flat: {
            image: '/images/emotes/flat.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          noflat: {
            image: '/images/emotes/noflat.png',
            type: 0,
            frames: 1,
            speed: 50,
          },
          rage: {
            image: '/images/emotes/rage.png',
            type: 0,
            frames: 5,
            speed: 50,
          },
          sad: {
            image: '/images/emotes/sad.png',
            type: 0,
            frames: 2,
            speed: 50,
          },
          sweat: {
            image: '/images/emotes/sweat.png',
            type: 0,
            frames: 10,
            speed: 50,
          },
          teamedon: {
            image: '/images/emotes/miss.png',
            type: 1,
            frames: 28,
            speed: 75,
          },
          evanism: {
            image: '/images/emotes/evanism.png',
            type: 1,
            frames: 45,
            speed: 100,
          },
          miss: {
            image: '/images/emotes/teamedon.png',
            type: 0,
            frames: 12,
            speed: 50,
          }
        }, // REVISE make loader more optimized (fix repeating /images/emotes/)
        animations: {
          tape: {
            image: '/images/animations/tape.png',
            frames: 17,
            speed: 50,
          },
          toolkit: {
            image: '/images/animations/toolkit.png',
            frames: 16,
            speed: 50,
          },
          /*glu: {
            image: '/images/animations/glu.png',
            frames: 0,
            speed: 1000,
          },*/
        },
        items: {
          airstrike: '/images/items/airstrike.png',
          duck_tape: '/images/items/duck-tape.png',
          super_glu: '/images/items/super-glu.png',
          shield: '/images/items/shield.png',
          flashbang: '/images/items/flashbang.png',
          bomb: '/images/items/bomb.png',
          dynamite: '/images/items/dynamite.png',
          power_bomb: '/images/items/power-bomb.png',
          weak: '/images/items/weak.png',
          strong: '/images/items/strong.png',
          spike: '/images/items/spike.png',
          mine: '/images/items/mine.png',
          fortress: '/images/items/fortress.png',
        }
      };

      Menus.menus = {
        start: '',
        main: '',
        multiplayer: '',
        pvp: '',
        co_op: '',
        ffa: '',
        duels: '',
        crate: '',
        cosmetic: '',
        settings: '',
        keybinds: '',
        inventory: '',
        shop: '',
        defeat: '',
        victory: '',
      }

      PixelTanks.crates = {
        red: {
          common: ['X', 'Red Hoodie', 'Devils Wings', 'Devils Horns', 'Exclaimation Point'],
          uncommon: ['Apple'],
          rare: ['Hacks'],
          epic: ['Rage'],
          legendary: ['Redsus'],
        },
        orange: {
          common: ['Orange Hoodie'],
          uncommon: ['Pumpkin', 'Basketball'],
          rare: ['Tools'],
          epic: ['Onfire'],
          legendary: [],
        },
        yellow: {
          common: ['Yellow Hoodie', 'Dizzy'],
          uncommon: ['Banana'],
          rare: ['Money Eyes'],
          epic: ['Halo'],
          legendary: ['Uno Reverse'],
        },
        green: {
          common: ['Green Hoodie', 'Leaf'],
          uncommon: ['Pickle'],
          rare: ['Checkmark'],
          epic: [],
          legendary: [],
        },
        blue: {
          common: ['Blue Hoodie'],
          uncommon: ['Blueberry'],
          rare: ['Sweat', 'Scared'],
          epic: ['Police', 'Blue Tint'],
          legendary: [],
        },
        purple: {
          common: ['Purple Hoodie', 'Purple Flower'],
          uncommon: ['Eggplant'],
          rare: ['Purple Top Hat', 'Purple Grad Hat'],
          epic: [],
          legendary: [],
        },
        mark: {
          common: ['Boost', 'Cancelled', 'Spirals', 'Laff'],
          uncommon: ['Peace', 'Question Mark', 'Small Scratch', 'Ban'],
          rare: ['Eyebrows'],
          epic: ['Deep Scratch', 'Back'],
          legendary: [],
        },
        grey: {
          common: ['Speaker', 'Spikes', 'Bat Wings'],
          uncommon: ['Headphones', 'Knife'],
          rare: ['Helment'],
          epic: ['Assassin', 'Astronaut', 'Controller'],
          legendary: ['Terminator'],
        },
        christmas: {
          common: ['Christmas Tree', 'Candy Cane'],
          uncommon: ['Reindeer Hat'],
          rare: ['Rudolph'],
          epic: ['Christmas Lights'],
          legendary: ['Christmas Hat'],
        },
        halloween: {
          common: ['Pumpkin Face'],
          uncommon: ['Pumpkin Hat', 'Cat Ears'],
          rare: ['Candy Corn'],
          epic: [],
          legendary: [],
        },
        misc: {
          common: ['Top Hat', 'Victim', 'Pink/Purple Hoodie', 'Bunny Ears'],
          uncommon: ['Cake', 'Cat Hat', 'First Aid', 'Fisher Hat'],
          rare: ['Flag', 'Swords'],
          epic: ['No Mercy', 'Error'],
          legendary: ['Mini Tank', 'Paleontologist'],
          mythic: ['MLG Glasses'],
        },
        pizza: {
          common: ['Red Ghost', 'Blue Ghost', 'Pink Ghost', 'Orange Ghost'],
          uncommon: [],
          rare: [],
          epic: [],
          legendary: ['Yellow Pizza'],
        },
      };

      var menuActions = {
        start: {
          buttons: [],
          exec: [
            [580, 740, 200, 100, `Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'login', () => {PixelTanks.getData(() => {Menus.trigger('main');});});`],
            [820, 740, 200, 100, `Network.auth(Menus.menus.start.username, Menus.menus.start.password, 'signup', () => {PixelTanks.getData(() => {Menus.trigger('main');});});`],
            [580, 480, 440, 60, `Menus.menus.start.type = 'username';`],
            [580, 580, 440, 60, `Menus.menus.start.type = 'password';`],
          ],
          listeners: {
            keydown: (e) => {
              var start = Menus.menus.start;
              if (e.key.length === 1) {
                start[start.type] += e.key;
              } else if (e.keyCode === 8) {
                start[start.type] = start[start.type].slice(0, -1);
              }
              Menus.redraw();
            }
          },
          customOnLoad: () => {
            if (!Menus.menus.start.type) {
              if (sessionStorage.username !== undefined) {
                PixelTanks.getData(() => {
                  Menus.trigger('main');
                });
              } 
              Menus.menus.start.type = 'username';
              Menus.menus.start.username = '';
              Menus.menus.start.password = '';
            }
            GUI.drawText(Menus.menus.start.username, 580, 495, 30, '#ffffff', 0)
            var l = 0, temp = '';
            while (l < Menus.menus.start.password.length) {
              temp += '*';
              l++;
            }
            GUI.drawText(temp, 580, 595, 30, '#ffffff', 0);
          },
        },
        main: {
          buttons: [
            [1180, 920, 80, 80, 'settings'],
            [580, 500, 440, 100, 'multiplayer'],
            [580, 360, 440, 100, 'levelSelect'],
            [580, 640, 440, 100, 'shop'],
            [420, 920, 80, 80, 'inventory'],
          ],
          exec: [
            [320, 920, 80, 80, `(() => {sessionStorage.token = undefined; sessionStorage.username = undefined; Menus.trigger('start');})();`],
          ],
          listeners: {},
          customOnLoad: () => {
            PixelTanks.save();

            var key = {
              0: 'red',
              1: 'steel',
              2: 'crystal',
              3: 'dark',
              4: 'light',
            };

            GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].bottom[0], 250, 400, 80, 80, 1);
            GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].top, 250, 400, 80, 90, 1);
            if (PixelTanks.userData.cosmetic != '' || PixelTanks.userData.cosmetic != undefined) {
              for (var property in PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics) {
                if (PixelTanks.userData.cosmetic === property) {
                  GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[property], 250, 400, 80, 90, 1);
                }
              }
            }
            GUI.drawText(PixelTanks.user.username, 300, 420, 30, '#ffffff', 0.5)

            var xpToLevelUp = Math.ceil(Math.pow(1.6, PixelTanks.userData.stats[4]-1)+20*(PixelTanks.userData.stats[4]-1));
            while (PixelTanks.userData.stats[3] >= xpToLevelUp) {
              PixelTanks.userData.stats[3] -= xpToLevelUp;
              PixelTanks.userData.stats[4] += 1;
              xpToLevelUp = Math.ceil(Math.pow(1.6, PixelTanks.userData.stats[4]-1)+20*(PixelTanks.userData.stats[4]-1));
            }
            GUI.drawText('Rank: '+PixelTanks.userData.stats[4], 300, 440, '#ffffff', 0);
            GUI.drawText('XP - '+PixelTanks.userData.stats[3]+'/'+xpToLevelUp, 300, 460, '#ffffff', 0);
            GUI.drawText(PixelTanks.userData.coins, 300, 480, '#ffffff', 0);
          },
        },
        multiplayer: {
          buttons: [
            [416, 722, 376, 166, 'co_op'],
            [810, 722, 376, 166, 'pvp'],
            [418, 112, 106, 106, 'main'],
          ],
          exec: [],
          listeners: {},
          customOnLoad: () => {} // Server Data?
        },
        pvp: {
          buttons: [
            [118, 112, 106, 106, 'multiplayer'],
            [810, 778, 376, 110, 'duels'],
            [396, 778, 376, 110, 'ffa'],
          ],
          exec: [],
          customOnLoad: () => {},
        },
        co_op: {
          buttons: [],
          exec: [],
          customOnLoad: () => {
            alert('This area is still in testing...');
            Menus.trigger('multiplayer');
          },
        },
        ffa: {
          buttons: [
            [118, 112, 106, 106, 'pvp'],
          ],
          exec: [
            [616, 466, 376, 110, `(() => {Menus.removeListeners(); PixelTanks.user.joiner = new MultiPlayerTank(); PixelTanks.user.joiner.control();})();`],
          ],
          listeners: {},
          customOnLoad: () => {},
        },
        duels: {
          buttons: [
            [59, 56, 53, 53, 'pvp'],
          ],
          exec: [],
          listeners: {},
          customOnLoad: () => {},
        },
        crate: {
          buttons: [
            [418, 112, 106, 106, 'main'],
            [1076, 114, 106, 106, 'cosmetic'],
          ],
          exec: [
            [438, 606, 106, 106, 'PixelTanks.openCrate(`red`);'],
            [946, 290, 106, 106, 'PixelTanks.openCrate(`orange`);'],
            [946, 446, 106, 106, 'PixelTanks.openCrate(`yellow`);'],
            [946, 612, 106, 106, 'PixelTanks.openCrate(`green`);'],
            [696, 610, 106, 106, 'PixelTanks.openCrate(`blue`);'],
            [694, 444, 106, 106, 'PixelTanks.openCrate(`purple`);'],
            [438, 286, 106, 106, 'PixelTanks.openCrate(`mark`);'],
            [946, 766, 106, 106, 'PixelTanks.openCrate(`grey`);'],
            [436, 440, 106, 106, 'PixelTanks.openCrate(`christmas`);'],
            [696, 764, 106, 106, 'PixelTanks.openCrate(`halloween`);'],
            [438, 760, 106, 106, 'PixelTanks.openCrate(`misc`);'],
            [696, 290, 106, 106, 'PixelTanks.openCrate(`pizza`);'],
          ],
          listeners: {},
          customOnLoad: () => {
            GUI.drawText('Crates: ' + PixelTanks.userData.stats[1], 800, 260, 30, '#ffffff', 0.5);
          }
        },
        cosmetic: {
          buttons: [
            [59, 56, 53, 53, 'crate'],
          ],
          exec: [], // handled later
          listeners: {
            keydown: (e) => {
              if (e.keyCode === 37 && Menus.menus.cosmetic.menuNum != 0) {
                Menus.menus.cosmetic.menuNum--;
              } else if (e.keyCode === 39 && (Menus.menus.cosmetic.menuNum + 1) * 30 < PixelTanks.userData.cosmetics.length) {
                Menus.menus.cosmetic.menuNum++;
              }
              Menus.redraw();
            },
            mousedown: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - PixelTanks.offset * PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              var y2 = (y - ((y - 140) % 59)) / 59;
              var x2 = (x - ((x - 78) % 59)) / 59;
              var y3, x3;
              if (((y - 140) % 59 < y2 + 50) && y > 140) {
                y3 = Math.floor(y2 - 2);
              }
              if (((x - 78) % 59 < x2 + 50) && x > 78) {
                x3 = Math.floor(x2);
              }
              if (x3 != undefined && y3 != undefined) {
                if (e.button === 0) {
                  PixelTanks.userData.cosmetic = PixelTanks.userData.cosmetics[Menus.menus.cosmetic.menuNum * 30 + y3 * 6 + x3 - 1];
                } else {
                  PixelTanks.userData.cosmetics.splice(Menus.menus.cosmetic.menuNum * 30 + y3 * 6 + x3 - 1, 1);
                }
                Menus.redraw();
              }
            }
          },
          customOnLoad: () => {
            var cosmetic = Menus.menus.cosmetic;
            if (!cosmetic.menuNum) {
              cosmetic.menuNum = 0;
            }

            var totalStages = (PixelTanks.userData.cosmetics.length - (PixelTanks.userData.cosmetics.length % 30)) / 30;

            if (cosmetic.menuNum === 0) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].cosmeticLeft, 0, 0);
            } else if (cosmetic.menuNum !== totalStages) {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].cosmeticCenter, 0, 0);
            } else {
              GUI.draw.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].cosmeticRight, 0, 0);
            }

            var l = cosmetic.menuNum * 30;
            while (l < Math.min(cosmetic.menuNum * 30 + 30, PixelTanks.userData.cosmetics.length)) {
              if (PixelTanks.userData.cosmetic === PixelTanks.userData.cosmetics[l]) {
                GUI.draw.lineWidth = 5;
                GUI.draw.strokeStyle = '#FFA500';
                GUI.draw.strokeRect(((l - cosmetic.menuNum * 30) % 6) * 59 + 78 - 2, (((l - cosmetic.menuNum * 30) - ((l - cosmetic.menuNum * 30) % 6)) / 6) * 59 + 140 - 2, 50, 50);
              }
              var q = 0;
              while (q < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
                if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[q].name === PixelTanks.userData.cosmetics[l]) {
                  GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[q].image, ((l - cosmetic.menuNum * 30) % 6) * 59 + 78, (((l - cosmetic.menuNum * 30) - ((l - cosmetic.menuNum * 30) % 6)) / 6) * 59 + 140);
                }
                q++;
              }
              l++;
            }
          }
        },
        settings: {
          buttons: [
            [59, 56, 53, 53, 'main'],
            [397, 65, 38, 35, 'keybinds'],
          ],
          exec: [],
          listeners: {},
          customOnLoad: () => {}
        },
        keybinds: {
          buttons: [
            [59, 56, 53, 53, 'settings'],
          ],
          exec: [],
          listeners: {
            keydown: (e) => {
              if (Menus.menus.keybinds.keybind !== undefined) {
                PixelTanks.userData.settings[Menus.menus.keybinds.keybind] = e.keyCode;
              }
              Menus.redraw();
              PixelTanks.save();
            },
            mousedown: (e) => {
              var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
              var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
              x /= PixelTanks.resizer;
              y /= PixelTanks.resizer;
              if ((x > 70 && x < 150) && (y > 140 && y < 190)) {
                Menus.menus.keybinds.keybind = 'item1';
              } else if ((x > 70 && x < 150) && (y > 200 && y < 250)) {
                Menus.menus.keybinds.keybind = 'item2';
              } else if ((x > 70 && x < 150) && (y > 260 && y < 310)) {
                Menus.menus.keybinds.keybind = 'item3';
              } else if ((x > 70 && x < 150) && (y > 320 && y < 370)) {
                Menus.menus.keybinds.keybind = 'item4';
              } else if ((x > 70 && x < 150) && (y > 380 && y < 430)) {
                Menus.menus.keybinds.keybind = 'shoot';
              } else if ((x > 260 && x < 340) && (y > 140 && y < 190)) {
                Menus.menus.keybinds.keybind = 'fire1';
              } else if ((x > 260 && x < 340) && (y > 200 && y < 250)) {
                Menus.menus.keybinds.keybind = 'fire2';
              } else {
                Menus.menus.keybinds.keybind = undefined;
              }
              Menus.redraw();
            },
          },
          customOnLoad: () => {
            GUI.draw.strokeStyle = '#ffffff';
            GUI.draw.lineWidth = 5;
            switch (Menus.menus.keybinds.keybind) {
              case 'item1':
                GUI.draw.strokeRect(70, 140, 80, 50);
                break;
              case 'item2':
                GUI.draw.strokeRect(70, 200, 80, 50);
                break;
              case 'item3':
                GUI.draw.strokeRect(70, 260, 80, 50);
                break;
              case 'item4':
                GUI.draw.strokeRect(70, 320, 80, 50);
                break;
              case 'shoot':
                GUI.draw.strokeRect(70, 380, 80, 50);
                break;
              case 'fire1':
                GUI.draw.strokeRect(260, 140, 80, 50);
                break;
              case 'fire2':
                GUI.draw.strokeRect(260, 200, 80, 50);
                break;
            }
            GUI.draw.fillStyle = '#000000';
            GUI.draw.font = '20px Font';
            GUI.draw.fillText(PixelTanks.userData.settings.item1, 190, 170);
            GUI.draw.fillText(PixelTanks.userData.settings.item2, 190, 230);
            GUI.draw.fillText(PixelTanks.userData.settings.item3, 190, 290);
            GUI.draw.fillText(PixelTanks.userData.settings.item4, 190, 350);
            GUI.draw.fillText(PixelTanks.userData.settings.shoot, 190, 410);
            GUI.draw.fillText(PixelTanks.userData.settings.fire1, 420, 150);
            GUI.draw.fillText(PixelTanks.userData.settings.item5, 420, 210);
          },
        },
        inventory: {
          buttons: [
            [416, 110, 108, 110, 'main'],
            [756, 220, 88, 88, 'crate'],
          ],
          exec: [
            [1064, 458, 88, 88, `(() => {if (Menus.menus.inventory.healthTab === undefined) {Menus.menus.inventory.healthTab = false;} Menus.menus.inventory.healthTab = !Menus.menus.inventory.healthTab; Menus.menus.inventory.classTab = false; Menus.menus.inventory.itemTab = false; Menus.redraw();})();`],
            [1112, 814, 88, 88, `(() => {if (Menus.menus.inventory.classTab === undefined) {Menus.menus.inventory.classTab = false;} Menus.menus.inventory.classTab = !Menus.menus.inventory.classTab; Menus.menus.inventory.healthTab = false; Menus.menus.inventory.itemTab = false; Menus.redraw();})();`],
            [400, 814, 88, 88, `(() => {if (Menus.menus.inventory.itemTab === undefined) {Menus.menus.inventory.itemTab = false;} Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab; Menus.menus.inventory.healthTab = false; Menus.menus.inventory.classTab = false; Menus.menus.inventory.currentItem = 1; Menus.redraw();})();`],
            [488, 814, 88, 88, `(() => {if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false; Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab; Menus.menus.inventory.healthTab = false; Menus.menus.inventory.classTab = false; Menus.menus.inventory.currentItem = 2; Menus.redraw();})();`],
            [576, 814, 88, 88, `(() => {if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false; Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab; Menus.menus.inventory.healthTab = false; Menus.menus.inventory.classTab = false; Menus.menus.inventory.currentItem = 3; Menus.redraw();})();`],
            [664, 814, 88, 88, `(() => {if (Menus.menus.inventory.itemTab === undefined) Menus.menus.inventory.itemTab = false; Menus.menus.inventory.itemTab = !Menus.menus.inventory.itemTab; Menus.menus.inventory.healthTab = false; Menus.menus.inventory.classTab = false; Menus.menus.inventory.currentItem = 4; Menus.redraw();})();`],
          ],
          listeners: {
            mousedown: (e) => {
              var x = (e.clientX-(window.innerWidth-window.innerHeight*1.6)/2)/PixelTanks.resizer;
              var y = e.clientY/PixelTanks.resizer;
              if (Menus.menus.inventory.healthTab) {
                if (x > 754 && x < 844 && y > 240 && y < 328) {
                  PixelTanks.userData.material = 0;
                }
                if (x > 754 && x < 844 && y > 344 && y < 436) {
                  if (PixelTanks.userData.armors[0]) {
                    PixelTanks.userData.material = 1;
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 754 && x < 844 && y > 456 && y < 544) {
                  if (PixelTanks.userData.armors[1]) {
                    PixelTanks.userData.material = 2;
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 754 && x < 844 && y > 564 && y < 652) {
                  if (PixelTanks.userData.armors[2]) {
                    PixelTanks.userData.material = 3;
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 754 && x < 844 && y > 672 && y < 760) {
                  if (PixelTanks.userData.armors[3]) {
                    PixelTanks.userData.material = 4;
                  } else {
                    alert('You need to buy this first');
                  }
                }
              }
              if (Menus.menus.inventory.classTab) {
                if (x > 702 && x < 790 && y > 294 && y < 382) {
                  if (PixelTanks.userData.classes[0]) {
                    PixelTanks.userData.class = 'tactical';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 816 && x < 898 && y > 294 && y < 382) {
                  if (PixelTanks.userData.classes[1]) {
                    PixelTanks.userData.class = 'stealth';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 816 && x < 898 && y > 510 && y < 598) {
                  if (PixelTanks.userData.classes[2]) {
                    PixelTanks.userData.class = 'warrior';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 816 && x < 898 && y > 618 && y < 706) {
                  if (PixelTanks.userData.classes[3]) {
                    PixelTanks.userData.class = 'medic';
                  } else {
                    alert('You need to buy this first');
                  }
                }
                if (x > 816 && x < 898 && y > 402 && y < 490) {
                  if (PixelTanks.userData.classes[4]) {
                    PixelTanks.userData.class = 'builder';
                  } else {
                    alert('You need to buy this first');
                  }
                }
              }
              if (Menus.menus.inventory.itemTab) {
                var key = {
                  airstrike: [600, 300],
                  super_glu: [708, 300],
                  duck_tape: [816, 300],
                  shield: [924, 300],
                  flashbang: [600, 408],
                  bomb: [708, 408],
                  dynamite: [816, 408],
                  power_bomb: [924, 408],
                  weak: [600, 516],
                  strong: [708, 516],
                  spike: [816, 516],
                  mine: [904, 516],
                  fortress: [600, 904],
                }
                for (var property in key) {
                  if (x > key[property][0] && x < key[property][0]+80 && y > key[property][1] && y < key[property][1]+80) {
                    if (!PixelTanks.userData.items.includes(property)) {
                      PixelTanks.userData.items[Menus.menus.inventory.currentItem-1] = property;
                    } else {
                      alert('You are not allowed to have more than 1 of the same item');
                    }
                  }
                }
              }
              Menus.redraw();
              PixelTanks.save();
            },
            mousemove: (e) => {
              var x = e.clientX+document.body.scrollLeft+document.documentElement.scrollLeft;
              var y = e.clientY+document.body.scrollTop+document.documentElement.scrollTop;
              Menus.menus.inventory.target = {
                x: x-window.innerWidth/2,
                y: y-window.innerHeight/2,
              }
              Menus.redraw();
            },
            keydown: (e) => {
              if (e.key.length === 1) {
                Menus.menus.inventory.color += e.key;
              } else if (e.keyCode === 8) {
                Menus.menus.inventory.color = Menus.menus.inventory.color.slice(0, -1);
              }
              PixelTanks.userData.color = Menus.menus.inventory.color;
              Menus.redraw();
            }
          },
          customOnLoad: () => {
            var key = {
              0: 'red',
              1: 'steel',
              2: 'crystal',
              3: 'dark',
              4: 'light',
            };

            if (PixelTanks.userData.color === undefined) PixelTanks.userData.color = '#A9A9A9';
            if (Menus.menus.inventory.color === undefined) Menus.menus.inventory.color = PixelTanks.userData.color;
            GUI.draw.beginFill(PixelTanks.userData.color.replace('#', '0x'));
            GUI.draw.drawRect(1116, 264, 40, 40);
            GUI.draw.endFill();
            GUI.drawText(Menus.menus.inventory.color, 1052, 256, 20, PixelTanks.userData.color, 0);

            GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].top, 1064, 458, 88, 88, 1);
            GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[0]], 400, 814, 80, 80, 1);
            GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[1]], 488, 814, 80, 80, 1);
            GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[2]], 576, 814, 80, 80, 1);
            GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[3]], 664, 814, 80, 80, 1);

            if (Menus.menus.inventory.target === undefined) Menus.menus.inventory.target = {
              x: 0,
              y: 0
            };

            var angle = Math.atan2(Menus.menus.inventory.target.x, Menus.menus.inventory.target.y) * 180 / Math.PI
            angle = -angle;
            if (angle < 0) {
              angle += 360;
            }
            if (angle >= 360) {
              angle -= 360;
            }

            GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].bottom[0], 680, 380, 240, 240, 1);
            GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack][key[PixelTanks.userData.material]].top, 800, 500, 240, 270, 20, 20, angle, 1);
            if (PixelTanks.userData.cosmetic !== '' && PixelTanks.userData.cosmetic !== undefined) {
              for (var property in PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics) {
                if (property === PixelTanks.userData.cosmetic) GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[property], 800, 500, 240, 270, 20, 20, angle, 1);
              }
            }

            if (Menus.menus.inventory.healthTab) {
              GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].healthTab, 742, 226, 116, 584, 1);
              GUI.draw.lineStyle(10, 0xFFFF00, 1);
              var key = [240, 348, 456, 564, 672];
              GUI.draw.drawRect(754, key[PixelTanks.userData.material], 88, 88);
              GUI.draw.endFill();
            }
            if (Menus.menus.inventory.classTab) {
              GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].classTab, 688, 280, 224, 440, 1);
              GUI.draw.lineStyle(10, 0xFFFF00, 1);
              if (PixelTanks.userData.class === 'tactical') GUI.draw.drawRect(701, 294, 88, 88);
              if (PixelTanks.userData.class === 'stealth') GUI.draw.drawRect(816, 294, 88, 88);
              if (PixelTanks.userData.class === 'builder') GUI.draw.drawRect(816, 402, 88, 88);
              if (PixelTanks.userData.class === 'warrior') GUI.draw.drawRect(816, 510, 88, 88);
              if (PixelTanks.userData.class === 'medic') GUI.draw.drawRect(816, 618, 88, 88);
              GUI.draw.endFill();
            }
            if (Menus.menus.inventory.itemTab) {
              GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].itemTab, 580, 280, 440, 440, 1);
              GUI.drawImage(PixelTanks.images.items.airstrike, 600, 300, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.super_glu, 708, 300, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.duck_tape, 816, 300, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.shield, 924, 300, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.flashbang, 600, 408, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.bomb, 708, 408, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.dynamite, 816, 408, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.power_bomb, 924, 408, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.weak, 600, 516, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.strong, 708, 516, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.spike, 816, 516, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.mine, 924, 516, 80, 80, 1);
              GUI.drawImage(PixelTanks.images.items.fortress, 600, 924, 80, 80, 1);
            }
          },
        },
        shop: {
          buttons: [
            [424, 28, 108, 108, 'main'],
          ],
          exec: [
            [88, 212, 328, 64, '(() => {Menus.menus.shop.tab="items"; Menus.redraw()})();'],
            [456, 212, 328, 64, '(() => {Menus.menus.shop.tab="armor"; Menus.redraw()})();'],
            [824, 212, 328, 64, '(() => {Menus.menus.shop.tab="class"; Menus.redraw()})();'],
            [1192, 212, 328, 64, '(() => {Menus.menus.shop.tab="kits"; Menus.redraw()})();']
          ],
          listeners: {
            mousedown: (e) => {
              var x = (e.clientX-(window.innerWidth-window.innerHeight*1.6)/2)/PixelTanks.resizer;
              var y = e.clientY/PixelTanks.resizer;
              if (Menus.menus.shop.tab === 'items') {
                
              } else if (Menus.menus.shop.tab === 'armor') {
                if (x > 424 && x < 584 && y > 460 && y < 620) {
                  PixelTanks.purchase('steel');
                }
                if (x > 624 && x < 784 && y > 460 && y < 620) {
                  if (!PixelTanks.userData.armors[0]) {
                    alert('You need to buy steel tank first!');
                  } else {
                    PixelTanks.purchase('crystal');
                  }
                }
                if (x > 824 && x < 984 && y > 460 && y < 620) {
                  if (!PixelTanks.userData.armors[1]) {
                    alert('You need to buy crystal tank first!');
                  } else {
                    PixelTanks.purchase('dark');
                  }
                }
                if (x > 1024 && x < 1184 && y > 460 && y < 620) {
                  if (!PixelTanks.userData.armors[2]) {
                    alert('You need to buy dark tank first!');
                  } else {
                    PixelTanks.purchase('light');
                  }
                }
              } else if (Menus.menus.shop.tab === 'class') {

              } else {

              }
            },
          },
          customOnLoad: () => {
            GUI.drawText(PixelTanks.userData.stats[0]+' coinage', 800, 350, 20, '#ffffff', 0.5);
            if (Menus.menus.shop.tab === undefined) Menus.menus.shop.tab = 'armor';
            GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack]['shop_'+Menus.menus.shop.tab], 0, 0, 1600, 1000, 1);
            if (Menus.menus.shop.tab === 'items') {
              var items = ['airstrike', 'super_glue', 'duck_tape', 'shield', 'flashbang', 'bomb', 'dynamite', 'power_bomb', 'weak', 'strong', 'spike', 'mine', 'fortress'];
              
            } else if (Menus.menus.shop.tab === 'armor') {
              if (!PixelTanks.userData.armors[0]) console.log('bdafaKiAN29AK91nA8Al_18');
            } else if (Menus.menus.shop.tab === 'class') {

            } else {

            }
          },
        },
        defeat: '/images/menus/default/defeat.png',
        victory: '/images/menus/default/victory.png',
      }

      // Add Images to Asset loader
      var toLoad = [];
      for (var property in PixelTanks.images.blocks) {
        for (var blok in PixelTanks.images.blocks[property]) {
          toLoad.push('blocks:'+property+':'+blok);
          PIXI.Assets.add('blocks:'+property+':'+blok, PixelTanks.images.blocks[property][blok]);
        }
      }
      
      for (var property in PixelTanks.images.bullets) {
        for (var bullet in PixelTanks.images.bullets[property]) {
          toLoad.push('bullets:'+property+':'+bullet);
          PIXI.Assets.add('bullets:'+property+':'+bullet, PixelTanks.images.bullets[property][bullet]);
        }
      }

      for (var property in PixelTanks.images.tanks) {
        for (var tank in PixelTanks.images.tanks[property]) {
          if (tank != 'cosmetics' && tank != 'other') {
            toLoad.push('tanks:'+property+':'+tank+':top');
            PIXI.Assets.add('tanks:'+property+':'+tank+':top', PixelTanks.images.tanks[property][tank].top);
            toLoad.push('tanks:'+property+':'+tank+':bottom:'+0);
            PIXI.Assets.add('tanks:'+property+':'+tank+':bottom:'+0, PixelTanks.images.tanks[property][tank].bottom[0]);
            toLoad.push('tanks:'+property+':'+tank+':bottom:'+1);
            PIXI.Assets.add('tanks:'+property+':'+tank+':bottom:'+1, PixelTanks.images.tanks[property][tank].bottom[1]);
          }
        }
        for (var cosmetic in PixelTanks.images.tanks[property].cosmetics){
          var src = PixelTanks.images.tanks[property].cosmetics[cosmetic];
          toLoad.push('tanks:'+property+':cosmetics:'+cosmetic);
          PIXI.Assets.add('tanks:'+property+':cosmetics:'+cosmetic, '/images/tanks/'+property+'/cosmetics/'+src+'.png');
        }
        for (var item in PixelTanks.images.tanks[property].other) {
          toLoad.push('tanks:'+property+':'+item);
          PIXI.Assets.add('tanks:'+property+':'+item, PixelTanks.images.tanks[property].other[item]);
        }
      }

      for (var property in PixelTanks.images.menus) {
        for (var menu in PixelTanks.images.menus[property]) {
          toLoad.push('menus:'+property+':'+menu);
          PIXI.Assets.add('menus:'+property+':'+menu, PixelTanks.images.menus[property][menu]);
        }
      }

      for (var property in PixelTanks.images.emotes) {
        toLoad.push('emotes:'+property+':image');
        PIXI.Assets.add('emotes:'+property+':image', PixelTanks.images.emotes[property].image);
      }

      for (var property in PixelTanks.images.animations) {
        toLoad.push('animations:'+property+':image');
        PIXI.Assets.add('animations:'+property+':image', PixelTanks.images.animations[property].image);
      }

      for (var property in PixelTanks.images.items) {
        toLoad.push('items:'+property);
        PIXI.Assets.add('items:'+property, PixelTanks.images.items[property]);
      }

      // load menus

      for (var property in Menus.menus) {
        if (property.includes('levelSelect')) {
          var menuNum = property.replace('levelSelect', '');
          if (menuNum == '') menuNum = 1;
          menuNum = new Number(menuNum);
          var y = 0;
          while (y < 4) {
            var x = 0;
            while (x < 3) {
              menuActions[property].exec.push({
                x: 89 * x + 140,
                y: 88 * y + 94,
                w: 44,
                h: 44,
                ref: `(() => { PixelTanks.user.world = new World(); PixelTanks.user.world.level = ` + (12 * (menuNum - 1) + (x + y * 3) + 1) + `; Menus.removeListeners(); PixelTanks.user.world.init();})();`,
              });
              x++;
            }
            y++;
          }
        }
        Menus.menus[property] = new Menu(function() { // No arrow function here
          if (PixelTanks.images.menus[PixelTanks.texturepack][this.id]) {
            GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack][this.id], 0, 0, 1600, 1000, 1);
          }
          this.data.customOnLoad = this.data.customOnLoad.bind(this);
          this.data.customOnLoad();
        }, {
          click: Menus.onclick,
          ...menuActions[property].listeners,
        }, window);
        Menus.menus[property].data = menuActions[property];
        Menus.menus[property].id = property;
      }

      PixelTanks.socket = new MegaSocket('wss://' + window.location.hostname, {
        keepAlive: false,
      });

      // Load Assets
      var assets = await PIXI.Assets.load(toLoad, this.updateBootProgress);
      for (var property in assets) {
        var l = 0, ref = '';
        while (l<property.split(':').length) {
          ref += '["'+property.split(':')[l]+'"]';
          l++;
        }
        eval('PixelTanks.images'+ref+' = assets["'+property+'"];');
      }

      setTimeout(PixelTanks.launch, 200);
    }

    static launch() {
      Menus.trigger('start');
    }

    static save() {
      try {
        var temp = PixelTanks.playerData;
        temp['pixel-tanks'] = PixelTanks.userData;
        Network.update('playerdata', JSON.stringify(temp));
      } catch (e) {
        // PixelTanks.user is not logged in
        console.log('User Not Logged In?: ERR->' + e);
      }
    }

    static getData(callback) {
        PixelTanks.user.username = sessionStorage.username;
        Network.get((data) => {
          PixelTanks.userData = JSON.parse(data.playerdata)['pixel-tanks'];
          PixelTanks.playerData = JSON.parse(data.playerdata);
          if (!PixelTanks.userData) {
            PixelTanks.userData = {
              username: sessionStorage.username,
              material: 0,
              class: 'normal',
              cosmetic: '',
              cosmetics: [],
              color: '#ffffff',
              stats: [
                0, // coins
                0, // crates
                1, // level
                0, // xp
                1, // rank
              ],
              classes: [
               false, // tactical
                false, // stealth
                false, // warrior
                false, // medic
                false, // builder
                false, // summoner
                false, // fire
                false, // ice
              ],
              armors: [
                false, // steel
                false, // crystal
                false, // dark
                false, // light
              ],
              items: ['duck_tape', 'weak', 'bomb', 'flashbang'],
              keybinds: {
                items: [49, 50, 51, 52],
                emotes: [53, 54, 55, 56, 57, 48],    
              },
            };
          }
          callback();
        });
    }

    static openCrate(crate) {

      if (PixelTanks.userData.stats[1] > 0) {
        PixelTanks.userData.stats[1]--;
      } else {
        alert('You need to get crates to open them. You can get crates by beating players in multiplayer');
        return;
      }

      var rarity;
      if (Math.floor(Math.random() * (1001)) < 1) {
        rarity = 'mythic';
      } else if (Math.floor(Math.random() * (1001)) < 10) {
        rarity = 'legendary';
      } else if (Math.floor(Math.random() * (1001)) < 50) {
        rarity = 'epic';
      } else if (Math.floor(Math.random() * (1001)) < 150) {
        rarity = 'rare';
      } else if (Math.floor(Math.random() * (1000 - 0 + 1)) < 300) {
        rarity = 'uncommon';
      } else {
        rarity = 'common';
      }
      var items = [];
      var l = 0;
      while (l < PixelTanks.crates[crate].length) {
        if (PixelTanks.crates[crate][l].rarity == rarity) {
          items.push(PixelTanks.crates[crate][l].name);
        }
        l++;
      }
      var number = Math.floor(Math.random() * (items.length));
      var c;
      var l = 0;
      while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
        if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name == items[number]) {
          c = PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l];
        }
        l++;
      }
      GUI.draw.clearRect(0, 0, 500, 500);
      GUI.draw.drawImage(c.image, 150, 200, 200, 200);
      GUI.draw.textAlign = 'center';
      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.fillText(c.name, 250, 400);
      GUI.draw.textAlign = 'left';
      setTimeout(() => {
        Menus.redraw();
      }, 2000);
      if (PixelTanks.userData.cosmetics.includes(c.name)) {
        setTimeout(function() {
          var t = confirm('You already have this. Sell for 100G?');
          if (t) {
            PixelTanks.userData.stats[0] += 100;
          } else {
            PixelTanks.userData.cosmetics.push(c.name);
            PixelTanks.save();
          }
        }, 2000);
        return;
      }
      PixelTanks.userData.cosmetics.push(c.name);
      PixelTanks.save();
    }

    static purchase(stat) {
      var key = {
        tactical: [['classes', 0], 70000],
        stealth: [['classes', 1], 30000],
        warrior: [['classes', 2], 70000],

        steel: [['armors', 0], 10000],
        crystal: [['armors', 1], 50000],
        dark: [['armors', 2], 100000],
        light: [['armors', 3], 150000],
      }
      if (key[stat] === undefined) alert('The ['+stat+'] item is not registered. Scream at me to add it.');
      if (PixelTanks.userData[key[stat][0][0]][key[stat][0][1]]) {
        alert('You already bought this :/');
      } else if (PixelTanks.userData.stats[0] >= key[stat][1]) {
        PixelTanks.userData.stats[0] -= key[stat][1];
        PixelTanks.userData[key[stat][0][0]][key[stat][0][1]] = true;
        alert('purchase succes. thank u 4 ur monee');
      } else {
        alert('Your brok boi');
      }
    }
  }

  class MultiPlayerTank {
    control() {
      this.SETTINGS = {
        fps: false, // chromebooks cant handle this :( 
      }

      this.timers = {
        boost: new Date('Nov 28 2006'),
        powermissle: new Date('Nov 28 2006'),
        toolkit: new Date('Nov 28 2006'),
        class: {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        },
        items: [{
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }, {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }, {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }, {
          date: new Date('Nov 28 2006'),
          cooldown: -1,
        }],
      };

      this.xp = 0;
      this.crates = 0;
      this.kills = 0;
      this.coins = 0;
      this.hostupdate = {};
      this.fireType = 1;

      this.halfSpeed = false;
      this.canFire = true;
      this.canBoost = true;
      this.canToolkit = true;
      this.canPowermissle = true;
      this.canMegamissle = true;
      this.canInvis = true;
      this.canTurret = true;
      this.canBuild = true;
      this.canBuff = true;
      this.canHeal = true;
      this.canDynamite = true;
      this.hasDynamite = false;
      this.canItem0 = true;
      this.canItem1 = true;
      this.canItem2 = true;
      this.canItem3 = true;

      this.gamemode = 'ffa';
      this.canChangePaused = true;
      this.paused = false;
      this.speed = 4;
      this.helper = [];
      this.intervals = []; // optimize rename to i
      this.intervals2 = [];
      this.left = null;
      this.up = null;
      this.grapples = 1;
      this.canGrapple = true;
      this.showChat = false;
      this.msg = '';

      this.tank = {
        s: 0, // shields [U]
        x: 0, // x [U, J]
        y: 0, // y [U, J]
        r: 0, // rotation [U, J]
        e: null, // emote [U]
        ra: PixelTanks.userData.stats[4], // rank [j]
        br: true, // base rotation [J, U]
        u: PixelTanks.user.username, // username [J]
        to: false, // toolkit [U]
        b: false, // place block [U]
        ba: false, // base first -> false, second -> true
        p: 0, // pushback [U, J]
        pl: 0, // place scaffolding [U]
        fl: false, // flashbang fired [U]
        i: false, // immune [U]
        in: false, // invis  [U]
        c: PixelTanks.userData.class, // class [J]
        co: PixelTanks.userData.cosmetic, // cosmetic [J]
        fi: [], // firing
        a: false, // animations
        mi: false, // place mine
      };

      this.tank.m = PixelTanks.userData.material;

      this.socket = new MegaSocket('wss://' + window.location.hostname + '/ffa-server', {
        keepAlive: false,
        reconnect: false,
        autoconnect: true,
      });

      this.socket.on('message', function(data) {
        this.ups++;
        if (this.paused) return;
        if (data.event == 'hostupdate') {
          if (data.tanks) {
            this.hostupdate.tanks = data.tanks;
          }
          if (data.ai) {
            this.hostupdate.ai = data.ai;
          }
          if (data.blocks) {
            this.hostupdate.blocks = data.blocks;
          }
          if (data.bullets) {
            this.hostupdate.bullets = data.bullets;
          }
          if (data.explosions) {
            this.hostupdate.explosions = data.explosions;
          }
          if (data.logs) {
            this.hostupdate.logs = data.logs.reverse();
          }
        } else if (data.event == 'ded') {
          this.halfSpeed = false;
          this.canFire = true;
          this.canBoost = true;
          this.canToolkit = true;
          this.canPowermissle = true;
          this.canMegamissle = true;
          this.canInvis = true;
          this.canTurret = true;
          this.canBuild = true;
          this.canBuff = true;
          this.canHeal = true;
          this.canDynamite = true;
          this.hasDynamite = false;
          this.canItem0 = true;
          this.canItem1 = true;
          this.canItem2 = true;
          this.canItem3 = true;
          this.kills = 0;
          this.timers = {
            boost: new Date('Nov 28 2006'),
            powermissle: new Date('Nov 28 2006'),
            toolkit: new Date('Nov 28 2006'),
            class: {
              date: new Date('Nov 28 2006'),
              cooldown: -1,
            },
            items: [{
              date: new Date('Nov 28 2006'),
            cooldown: -1,
            }, {
              date: new Date('Nov 28 2006'),
              cooldown: -1,
            }, {
              date: new Date('Nov 28 2006'),
              cooldown: -1,
            }, {
              date: new Date('Nov 28 2006'),
            cooldown: -1,
            }],
          };
        } else if (data.event == 'gameover') {
          gameover(data.data);
        } else if (data.event == 'override') {
          var l = 0;
          while (l < data.data.length) {
            this.tank[data.data[l].key] = data.data[l].value;
            l++;
          }
        } else if (data.event == 'kill') {
          this.kills++;
          var crates = Math.floor(Math.random() * (2) + 1);
          var coins = Math.floor(Math.random()*1000);
          this.xp += 10;
          this.crates += crates;
          this.coins += coins;
          PixelTanks.userData.stats[1] += crates;
          PixelTanks.userData.stats[3] += 10;
          PixelTanks.userData.stats[0] += coins;
          PixelTanks.save();
        } else if (data.event == 'ping') {
          this.ping = new Date().getTime() - this.pingStart;
        }
      }.bind(this));

      this.socket.on('connect', function() {
        this.socket.send({
          username: PixelTanks.user.username,
          token: sessionStorage.token,
          type: 'join',
          tank: {
            x: 0, // x [U, J]
            y: 0, // y [U, J]
            r: 0, // rotation [U, J]
            p: 0, // pushback [U, J]
            ra: PixelTanks.userData.stats[4], // rank [J]
            br: 0, // base rotation [J]
            u: PixelTanks.user.username, // username [J]
            ba: 0, // base image stage [U, J]
            c: PixelTanks.userData.class, // class [J]
            co: PixelTanks.userData.cosmetic, // cosmetic [J]
            m: PixelTanks.userData.material,
            col: PixelTanks.userData.color,
          },
        });

        this.pingStart = new Date().getTime();
        this.pingId = Math.random();
        this.socket.send({
          type: 'ping',
          id: this.pingId,
        })

        if (!this.SETTINGS.fps) {
          setInterval(this.send.bind(this), 1000/60);
          requestAnimationFrame(this.frame.bind(this));
        }
      }.bind(this));

      document.addEventListener('keydown', this.keydown.bind(this));
      document.addEventListener('keyup', this.keyup.bind(this));
      document.addEventListener('mousemove', this.mousemove.bind(this));
      document.addEventListener('mousedown', this.mousedown.bind(this));
      document.addEventListener('mouseup', this.mouseup.bind(this));

      setInterval(function() {
        this.pingId = Math.random();
        this.pingStart = new Date().getTime();
        this.socket.send({
          type: 'ping',
          id: this.pingId,
        });
        // use stats here
        this.ops = 0;
        this.ups = 0;
        this.fps = 0;
      }.bind(this), 1000);
    }

    drawBlock(b) {
      var team = false;
      var l = 0;
      while (l<this.hostupdate.tanks.length) {
        if (this.hostupdate.tanks[l].t.split(':')[1] === b.o.split(':')[1]) {
          team = true;
        }
        l++;
      }
      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack][b.t], b.x, b.y, b.t === 'airstrike' ? 200 : 100, b.t === 'airstrike' ? 200 : 100, (b.t === 'mine' && !team) ? .03 : 1);
      if (b.s) {
        GUI.draw.beginFill(0x000000);
        GUI.draw.drawRect(b.x-2, b.y+108, 104, 11);
        GUI.draw.endFill();
        GUI.draw.beginFill(0x0000FF);
        GUI.draw.drawRect(b.x, b.y+110, 100*b.h/b.m, 5);
        GUI.draw.endFill();
      }
    }

    drawShot(s) {
      if (s.t == 'bullet') {
        GUI.draw.beginFill(0x000000);
        GUI.draw.drawRect(s.x, s.y, 10, 10)//-2.5, -2.5, 5, 5);
        GUI.draw.endFill();
      } else if (s.t === 'powermissle' || s.t === 'healmissle') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].powermissle, s.x, s.y, 20, 40, 5, 10, s.r+180, 1);
      } else if (s.t == 'megamissle') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].megamissle, s.x, s.y, 20, 40, 5, 10, s.r+180, 1); //-5, -10, 10, 20);
      } else if (s.t == 'shotgun') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].shotgun, s.x, s.y, 10, 10, 2.5, 2.5, s.r+180, 1); //-2.5, -2.5);
      } else if (s.t == 'grapple') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].grapple, s.x, s.y, 45, 45, 22.5, 22.5, s.r+180, 1); //-22.5, -22.5);
        /*GUI.draw.strokeStyle = 'darkgray';
        GUI.draw.rotate(-(s.r+180)*Math.PI/180);
        GUI.draw.lineWidth = 5;
        GUI.draw.beginPath();
        GUI.draw.moveTo(-2.5, -2.5);
        GUI.draw.lineTo(s.sx-s.x-2.5, s.sy-s.y-2.5);
        GUI.draw.stroke();
        GUI.draw.rotate((s.r+180)*Math.PI/180);*/
      } else if (s.t === 'dynamite') {
        GUI.drawImageRotate(PixelTanks.images.bullets[PixelTanks.texturepack].dynamite, s.x, s.y, 10, 40, 2.5, 2.5, s.r+180, 1);
      }
    }

    drawExplosion(e) {
      GUI.drawImageCrop(PixelTanks.images.bullets[PixelTanks.texturepack].explosion, e.x, e.y, e.w, e.h, e.f*50, 0, 50, 50, 1);
    }

    drawAi(a) {
      GUI.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack]['red'].base, a.x, a.y, 40, 40);
      GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack]['red'].top, a.x, a.y+a.p, 80, 80, 20, 20, a.r, 1);
      for (var cosmetic in PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics) {
        if (cosmetic === a.c) {
          GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[cosmetic], a.x, a.y+a.p, 80, 80, 20, 20, a.r, 1);
        }
      }
      GUI.draw.beginFill(0x000000);
      GUI.draw.drawRect(a.x, a.y+100, 80, 10);
      GUI.draw.endFill();
      GUI.draw.beginFill('#00FF00');
      GUI.draw.drawRect(a.x+4, a.y+102, 72*a.hp/600, 6);
      GUI.draw.endFill();
    }

    drawTank(t) {
      var key = {
        0: 'red',
        1: 'steel',
        2: 'crystal',
        3: 'dark',
        4: 'light',
      };
      var a = 1;
      if (t.ded || t.in) a = .5;
      GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack][key[t.m]].bottom[t.ba ? 0 : 1], t.x+40, t.y+40, 80, 80, 20, 20, t.br, a);
      GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack][key[t.m]].top, t.x+40, t.y+40, 80, 90, 20, 20-t.p, t.r, a);
      if (t.co) {
        for (var cosmetic in PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics) {
          if (t.co === cosmetic) {
            GUI.drawImageRotate(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[cosmetic], t.x+40, t.y+40, 80, 90, 20, 20-t.p, t.r, a);
          }
        }
      }

      if (!t.ded) { // health bar
        GUI.draw.beginFill(0x000000);
        GUI.draw.drawRect(t.x-2, t.y+98, 84, 11);
        GUI.draw.endFill();
        GUI.draw.beginFill(0x90EE90);
        GUI.draw.drawRect(t.x, t.y+100, 80*t.h/t.ma, 5);
        GUI.draw.endFill();
      }

      var username = '['+t.ra+'] '+t.u;
      if (t.t.split(':')[1].includes('@leader')) {
        username += ' ['+t.t.split(':')[1].replace('@leader', '')+'] (Leader)'
      } else if (t.t.split(':')[1].includes('@requestor#')) {
        username += ' [Requesting...] ('+t.t.split(':')[1].split('@requestor#')[1]+')';
      } else if (new Number(t.t.split(':')[1]) < 1) {} else {
        username += ' ['+t.t.split(':')[1]+']';
      }

      var style = PIXI.TextMetrics.measureText(username, GUI.fontstyle[30]);
      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].void, t.x-style.width/2+40, t.y-style.height/2-25, style.width, 50, 0.5);
      GUI.drawText(username, t.x+40, t.y-25, 50, t.col, 0.5);

      if (t.s > 0 && !t.in) {
        GUI.draw.beginFill(0x7DF9FF, .2);
        GUI.draw.drawCircle(t.x+40, t.y+40, 66);
        GUI.draw.endFill();
      }

      if (t.buff) {
        GUI.drawImage(PixelTanks.images.tanks.default.other.buff, t.x-5, t.y-5, 80, 80, .2);
      }

      if (t.d !== false) {
        var msg = (Math.round(t.d.d) < 0) ? '+' : '-';
        msg += Math.round(t.d.d);
        if (PixelTanks.user.username === t.u) {
          GUI.drawText(msg, t.d.x, t.d.y, Math.round(t.d.d/10)+20, '#FFFFFF', 0.5);
          GUI.drawText(msg, t.d.x, t.d.y, Math.round(t.d.d/10)+18, '#FF0000', 0.5);
        } else {
          GUI.drawText(msg, t.d.x, t.d.y, Math.round(t.d.d/10)+20, '#FFFFFF', 0.5);
          GUI.drawText(msg, t.d.x, t.d.y, Math.round(t.d.d/10)+18, '#0000FF', 0.5);
        }
      }
      
      if (t.e) {
        GUI.drawImage(PixelTanks.images.emotes.speech.image, t.x+45, t.y-15);
        GUI.drawImageCrop(PixelTanks.images.emotes[t.e.a].image, t.x+45, t.y-15, 100, 100, t.e.f*50, 0, 50, 50, 1);
      }

      if (t.a) {
        GUI.drawImageCrop(PixelTanks.images.animations[t.a.i].image, t.x, t.y, 80, 90, t.a.f*40, 0, 40, 45, 1);
      }
    }

    frame() {
      requestAnimationFrame(this.frame.bind(this));
      GUI.clear();
      GUI.resetSpritePool();
      GUI.resetTextPool();
      this.fps++;
      var t = this.hostupdate.tanks, b = this.hostupdate.blocks, s = this.hostupdate.bullets, a = this.hostupdate.ai, e = this.hostupdate.explosions;
      this.stats = '(' + this.ops + ', ' + this.ups + ', ' + this.fps + ') ping: ' + this.ping + ' ['+this.hostupdate.bullets.length+', '+this.hostupdate.blocks.length+', '+this.hostupdate.tanks.length+']';

      var l = 0;
      while (l<t.length) {
        if (t[l].u == PixelTanks.user.username) {
          t[l].x = this.tank.x;
          t[l].y = this.tank.y;
          t[l].r = this.tank.r;
          t[l].br = this.tank.br; // player smoothing
          GUI.root.pivot.set(t[l].x-760, t[l].y-460);
          GUI.draw.pivot.set(t[l].x-760, t[l].y-460);
        }
        l++;
      }

      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].floor, 0, 0, 3000, 3000, 1);

      var l = 0;
      while (l<b.length) {
        this.drawBlock(b[l]);
        l++;
      }

      var l = 0;
      while (l<s.length) {
        this.drawShot(s[l]);
        l++;
      }

      var l = 0;
      while (l<a.length) {
        //this.drawAi(PixelTanks.user.joiner.hostupdate.ai[l]);
        l++;
      }

      var l = 0;
      while (l<t.length) {
        this.drawTank(t[l]);
        l++;
      }

      var l = 0;
      while (l<e.length) {
        this.drawExplosion(e[l]);
        l++;
      }

      var l = 0;
      while (l<t.length) {
        if (t[l].u == PixelTanks.user.username) {
          if (t[l].fb) {;
            GUI.drawImage(PIXI.Texture.WHITE, GUI.root.pivot.x, GUI.root.pivot.y, 1600, 1000, 0);
          }
        }
        l++;
      }

      GUI.drawImage(PixelTanks.images.menus[PixelTanks.texturepack].ui, GUI.root.pivot.x, GUI.root.pivot.y, 1600, 1000, 1);

      GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[0]], GUI.root.pivot.x+500, GUI.root.pivot.y+900, 100, 100, 1);
      GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[1]], GUI.root.pivot.x+666, GUI.root.pivot.y+900, 100, 100, 1);
      GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[2]], GUI.root.pivot.x+832, GUI.root.pivot.y+900, 100, 100, 1);
      GUI.drawImage(PixelTanks.images.items[PixelTanks.userData.items[3]], GUI.root.pivot.x+998, GUI.root.pivot.y+900, 100, 100, 1);

      GUI.draw.beginFill(PixelTanks.userData.color.replace('#', '0x'), .5);
      GUI.draw.drawRect(GUI.root.pivot.x+500, GUI.root.pivot.y+900+(Math.min((Date.now()-this.timers.items[0].date.getTime())/this.timers.items[0].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(GUI.root.pivot.x+666, GUI.root.pivot.y+900+(Math.min((Date.now()-this.timers.items[1].date.getTime())/this.timers.items[1].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(GUI.root.pivot.x+832, GUI.root.pivot.y+900+(Math.min((Date.now()-this.timers.items[2].date.getTime()) / this.timers.items[2].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(GUI.root.pivot.x+998, GUI.root.pivot.y+900+(Math.min((Date.now()-this.timers.items[3].date.getTime()) / this.timers.items[3].cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(GUI.root.pivot.x+358, GUI.root.pivot.y+900+(Math.min((Date.now()-this.timers.class.date.getTime())/this.timers.class.cooldown, 1))*100, 100, 100);
      GUI.draw.drawRect(GUI.root.pivot.x+1142, GUI.root.pivot.y+900+(Math.min((Date.now()-this.timers.powermissle.getTime())/10000, 1))*100, 100, 100);
      GUI.draw.endFill();

      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].void, GUI.root.pivot.x, GUI.root.pivot.y, 180, 250, 0.5);
      GUI.drawText('Kills Streak: ' + this.kills, GUI.root.pivot.x+10, GUI.root.pivot.y+50, 30, '#ffffff', 0);
      GUI.drawText('Crates: ' + this.crates, GUI.root.pivot.x+10, GUI.root.pivot.y+100, 30, '#ffffff', 0);
      GUI.drawText('Experience: ' + this.xp, GUI.root.pivot.x+10, GUI.root.pivot.y+150, 30, '#ffffff', 0);
      GUI.drawText('Coins: '+this.coins, GUI.root.pivot.x+10, GUI.root.pivot.y+200, 30, '#ffffff', 0);
      var style = PIXI.TextMetrics.measureText(this.stats, GUI.fontstyle[30]);
      GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].void, GUI.root.pivot.x+800-style.width/2, GUI.root.pivot.y+50-style.height/2, style.width, style.height, 0.5);
      GUI.drawText(this.stats, GUI.root.pivot.x+800, GUI.root.pivot.y+50, 30, '#ffffff', 0.5);

      var l = 0, len;
      if (this.showChat || this.hostupdate.logs.length < 3) {
        len = this.hostupdate.logs.length;
      } else {
        len = 3;
      }
      // possible memory leak with creating new objects every frame
      while (l<Math.min(len, 30)) {
        GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].void, GUI.root.pivot.x, GUI.root.pivot.y+800-l*30, PIXI.TextMetrics.measureText(this.hostupdate.logs[l].m, GUI.fontstyle[30]).width, 30, .5);
        GUI.drawText(this.hostupdate.logs[l].m, GUI.root.pivot.x, GUI.root.pivot.y+800-l*30, 30, this.hostupdate.logs[l].c, 0);
        l++;
      }

      if (this.showChat) {
        GUI.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].void, GUI.root.pivot.x, GUI.root.pivot.y+830, PIXI.TextMetrics.measureText(this.msg, GUI.fontstyle[30]).width, 30, .5);
        GUI.drawText(this.msg, GUI.root.pivot.x, GUI.root.pivot.y+830, 30, '#ffffff', 0);
      }
    }

    chat(e) {
      if (e.key.length === 1) {
        this.msg += e.key;
      } else if (e.keyCode === 8) {
        this.msg = this.msg.slice(0, -1);
      } else if (e.keyCode === 13) {
        if (this.msg !== '') {
          if (this.msg.split('')[0] === '/') {
            var command = this.msg.split(' ')[0]
            if (command === '/emote') {
              if (this.msg.split(' ')[1] === 'set') {
                var data = this.msg.split(' ')[2];
                data = data.split('~');
                if ((new Number(data[1]) >= 5 && new Number(data[1]) <= 9) || data[1] == 0) {
                  if (data[1] == 0) {
                    data[1] = 10;
                  }
                  PixelTanks.userData['emote' + (data[1] - 4)] = data[0];
                } else {
                  alert('invalid everything. bad idot.')
                }
              } else {
                this.emote(this.msg.split(' ')[1])
              }
            } else if (['/createteam', '/join', '/leave', '/accept', '/ban', '/unban', '/kick', '/kill', '/target'].includes(command)) {
              this.socket.send({
                type: 'command',
                data: this.msg.split(' '),
              });
            } else {
              alert('Command nonexistant. Use /emote emotename');
            }
          } else {
            this.socket.send({
              type: 'chat',
              msg: this.msg,
            });
          }
          this.msg = '';
        }
        this.showChat = false;
      }
    }

    keydown(e) {
      e.preventDefault();
      if (this.helper[e.keyCode] !== 'pressed') {
        if (this.showChat) {
          this.chat(e);
          return;
        }
        var cooldown = this.tank.in ? 12 : 15;
        this.keyStart(e);
        this.keyLoop(e);
        this.intervals2[e.keyCode] = setInterval(function() {
          this.tank.ba = !this.tank.ba;
          var left = this.left;
          var up = this.up;
          if (left === null) {
            if (up === null) {} else if (up) {
              this.tank.br = 180;
            } else if (!up) {
              this.tank.br = 0;
            }
          } else if (left) {
            if (up === null) {
              this.tank.br = 90;
            } else if (up) {
              this.tank.br = 135;
            } else if (!up) {
              this.tank.br = 45;
            }
          } else if (!left) {
            if (up === null) {
              this.tank.br = 270;
            } else if (up) {
              this.tank.br = 225;
            } else if (!up) {
              this.tank.br = 315;
            }
          }
        }.bind(this), 100);
        this.intervals[e.keyCode] = setInterval(this.keyLoop.bind(this), cooldown, e);
      }
      this.helper[e.keyCode] = 'pressed';
    }

    keyup(e) {
      if (this.paused) return;
      e.preventDefault();
      clearInterval(this.intervals[e.keyCode]);
      clearInterval(this.intervals2[e.keyCode]);
      if (e.keyCode == 65 || e.keyCode == 68) {
        this.left = null;
      }
      if (e.keyCode == 87 || e.keyCode == 83) {
        this.up = null;
      }
      this.helper[e.keyCode] = false;
    }

    mousemove(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      var targetX = x - window.innerWidth/2, targetY = y - window.innerHeight/2;
      var rotation = this.toAngle(targetX, targetY);
      this.tank.r = Math.round(rotation);
      this.mouse = {
        x: targetX,
        y: targetY,
      }
      if (this.SETTINGS.fps) {
        this.send();
      }
    }

    toAngle(x, y) {
      var angle = Math.atan2(x, y) * 180 / Math.PI
      angle = -angle //-90;
      if (angle < 0) {
        angle += 360;
      }
      if (angle >= 360) {
        angle -= 360;
      }
      return angle;
    }

    toPoint(angle) {
      var theta = (-angle) * Math.PI / 180;
      var y = Math.cos(theta);
      var x = Math.sin(theta);
      if (x < 0) {
        if (y < 0) {
          return {
            x: -1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: -1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      } else {
        if (y < 0) {
          return {
            x: 1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: 1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      }
    }

    mousedown(e) {
      var cooldown = this.fireType === 1 ? 200 : 600;
      if (this.canFire) {
        this.fire(e.button);
        this.canFire = false;
        setTimeout(function() {
          this.canFire = true;
        }.bind(this), cooldown);
      }

      clearInterval(this.fireInterval);
      this.fireInterval = setInterval(this.fire.bind(this), cooldown, e.button);
      }

    mouseup() {
      clearInterval(this.fireInterval);
    }

    fire(type) {
      var fireType = this.fireType;
      if (type === 'grapple' || type === 'megamissle') {
        fireType = 1;
      } else if (type === 'dynamite') {
        fireType = 1;
      } else if (type == 2) {
        if (this.canPowermissle) {
          type = 'powermissle';
          fireType = 1;
          this.canPowermissle = false;
          this.timers.powermissle = new Date();
          var cooldown = 10000;
          setTimeout(function() {
            this.canPowermissle = true;
          }.bind(this), cooldown);
        } else {
          if (this.fireType == 1) {
            type = 'bullet';
          } else if (this.fireType == 2) {
            type = 'shotgun';
          }
        }
      } else {
        if (this.fireType == 1) {
          type = 'bullet';
        } else if (this.fireType == 2) {
          type = 'shotgun';
        }
      }

      if (PixelTanks.userData.class === 'medic' && type === 'powermissle') type = 'healmissle';

      if (fireType == 1) {
        var bullet = this.toPoint(this.tank.r);
        bullet.t = type;
        bullet.r = this.tank.r;
        this.tank.fi.push(bullet);
      } else if (fireType == 2) { // REVISE with while loop
        var l = -10;
        while (l < 15) {
          var bullet = this.toPoint(this.tank.r + l);
          bullet.t = type;
          bullet.r = this.tank.r + l;
          this.tank.fi.push(bullet);
          l += 5;
        }
      }
    
      if (this.SETTINGS.fps) {
        this.send();
      }
    }

    collision(x, y) {

      var l = 0, team;
      while (l < this.hostupdate.tanks.length) {
        if (this.hostupdate.tanks[l].u === PixelTanks.user.username) {
          team = this.hostupdate.tanks[l].t.split(':')[1].replace('@leader', '').replace('@requestor#', '');
          if (this.hostupdate.tanks[l].ded) return true;
        }
        l++;
      }

      if (x < 0 || y < 0 || x + 80 > 3000 || y + 80 > 3000) {
        return false;
      }

      if (this.tank.in && this.tank.i) return true;
      var l = 0,
        blocks = this.hostupdate.blocks,
        len = blocks.length;
      while (l < len) {
        if ((x > blocks[l].x || x + 80 > blocks[l].x) && (x < blocks[l].x + 100 || x + 80 < blocks[l].x + 100) && (y > blocks[l].y || y + 80 > blocks[l].y) && (y < blocks[l].y + 100 || y + 80 < blocks[l].y + 100)) {
          if ((blocks[l].t === 'fortress' && blocks[l].o.split(':')[1] === team) || blocks[l].t === 'heal') {} else if (blocks[l].c) {
            return false;
          }
        }
        l++;
      }
      return true;
    }

    playAnimation(id) {
      this.tank.a = {
        i: id,
        f: 0,
      };
      clearInterval(this.animationInterval);
      this.animationInterval = setInterval(function() {
        if (this.tank.a.f === PixelTanks.images.animations[id].frames) {
          clearInterval(this.animationInterval);
          setTimeout(function() {
            this.tank.a = false;
          }.bind(this), PixelTanks.images.animations[id].speed);
        } else {
          this.tank.a.f++;
        }
      }.bind(this), PixelTanks.images.animations[id].speed);
    }

    item(id, slot) {
      /*
        case 32:
          if (this.canBlock) {
            if (PixelTanks.userData.blocks > 0) {
              this.canBlock = false;
              this.timers.block = new Date();
              PixelTanks.userData.blocks -= 1;
              this.tank.pl = true; // place scaffolding
              if (PixelTanks.userData.class == 'builder') {
                this.tank.st = 'gold';
              } else {
                this.tank.st = 'weak';
              }
              var cooldown = 5000;
              if (PixelTanks.userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                this.canBlock = true;
              }.bind(this), cooldown);
            }
          }
          break;
        case 69:
          if (PixelTanks.userData.flashbangs > 0) {
            if (this.canFlashbang) {
              PixelTanks.userData.flashbangs -= 1;
              this.tank.fl = true;
              this.canFlashbang = false;
              this.timers.flashbang = new Date();
              var cooldown = 40000;
              if (PixelTanks.userData.kit == 'cooldown') {
                cooldown *= .9;
              }
              setTimeout(function() {
                this.canFlashbang = true;
              }.bind(this), cooldown);
            }
          }
      */
      var key = {
        duck_tape: [function() {
          this.tank.ta = true;
          this.playAnimation('tape');
        }, 30000, false],
        super_glu: [function() {
          this.tank.gl = true;
        }, 40000, false],
        shield: [function() {
          this.tank.sh = true;
        }, 30000, false],
        weak: [function() {
          this.tank.pl = true; // place scaffolding
          this.tank.st = PixelTanks.userData.class === 'builder' ? 'gold' : 'weak';
        }, 3000, false],
        strong: [function() {
          this.tank.pl = true;
          this.tank.st = PixelTanks.userData.class === 'builder' ? 'gold' : 'strong';
        }, 7000, false],
        spike: [function() {
          this.tank.pl = true;
          this.tank.st = 'spike';
        }, 10000, false],
        flashbang: [function() {
          this.tank.fl = true;
        }, 20000, false],
        bomb: [function() {
          this.tank.bo = true;
        }, 10000, false],
        power_bomb: [function() {
          this.tank.po = true;
        }, 10000, false],
        mine: [function() {
          this.tank.pl = true;
          this.tank.st = 'mine';
        }, 5000, false],
        dynamite: [function() {
          if (!this['canItem'+slot]) {
            this.tank.dy = true;
          } else {
            this.fire('dynamite');
            this['canItem'+slot] = false;
            this.timers.items[slot].cooldown = 25000;
            this.timers.items[slot].date = new Date();
            setTimeout(function() {
              this['canItem'+slot] = true;
            }.bind(this), 25000);
          }
        }, 25000, true],
        airstrike: [function() {
          this.tank.as = {
            x: this.mouse.x*PixelTanks.resizer+GUI.root.pivot.x,
            y: this.mouse.y*PixelTanks.resizer+GUI.root.pivot.y,
          };
        }, 40000, false],
        fortress: [function() {
          this.tank.pl = true;
          this.tank.st = 'fortress';
        }, 30000, false],
      }
      this.useItem(key[id][0], key[id][1], slot, key[id][2]);
    }

    useItem(enable, cooldown, slot, c) {
      if (c) {
        enable = enable.bind(this);
        enable();
        return;
      }
      if (this['canItem'+slot]) {
        enable = enable.bind(this);
        enable();
        this.timers.items[slot].cooldown = cooldown;
        this.timers.items[slot].date = new Date();
        this['canItem'+slot] = false;
        setTimeout(function() {
          this['canItem'+slot] = true;
        }.bind(this), cooldown);
      }
    }

    keyStart(e) {
      if (this.paused && e.keyCode !== 22) return;
      switch (e.keyCode) {
        case PixelTanks.userData.keybinds.items[0]:
          this.item(PixelTanks.userData.items[0], 0);
          break;
        case PixelTanks.userData.keybinds.items[1]:
          this.item(PixelTanks.userData.items[1], 1);
          break;
        case PixelTanks.userData.keybinds.items[2]:
          this.item(PixelTanks.userData.items[2], 2);
          break;
        case PixelTanks.userData.keybinds.items[3]:
          this.item(PixelTanks.userData.items[3], 3);
          break;
        case 53:
          this.emote(PixelTanks.userData.emote1);
          break;
        case 54:
          this.emote(PixelTanks.userData.emote2);
          break;
        case 55:
          this.emote(PixelTanks.userData.emote3);
          break;
        case 56:
          this.emote(PixelTanks.userData.emote4);
          break;
        case 57:
          this.emote(PixelTanks.userData.emote5)
          break;
        case 48:
          this.emote(PixelTanks.userData.emote6)
          break;
        case 13:
          this.showChat = true;
          break;
        case false: //PixelTanks.userData.settings.fire1:
          this.fireType = 1;
          clearInterval(this.fireInterval);
          break;
        case false: //PixelTanks.userData.settings.fire2:
          this.fireType = 2;
          clearInterval(this.fireInterval);
          break;
        case 9:
          if (this.fireType === 2) {
            this.fireType = 1;
          } else {
            this.fireType++;
          }
          clearInterval(this.fireInterval);
          break;
        case 82:
          if (this.grapples > 0) {
            this.fire('grapple');
            this.grapples--;
            this.canGrapple = false;
            setTimeout(function() {
              this.canGrapple = true;
            }.bind(this), 200)
            if (this.grapples === 0) {
              setTimeout(function() {
                this.grapples = 1;
              }.bind(this), 5000);
            }
          }
          break;
        case 81:
          if (this.canToolkit) {
            if (this.halfSpeed) {
              this.tank.to = true;
              this.halfSpeed = false;
            } else {
              if (PixelTanks.userData.class !== 'medic') {
                this.halfSpeed = true;
                setTimeout(function() {
                  this.halfSpeed = false;
                }.bind(this), 7500);
              } 
              this.tank.to = true;
              this.canToolkit = false;
              this.timers.toolkit = new Date();
              setTimeout(function() {
                this.canToolkit = true;
              }.bind(this), 30000);
              this.playAnimation('toolkit');
            }
          }
          break;
        case 70:
          if (PixelTanks.userData.class === 'stealth') {
            if (this.canInvis && !this.tank.in) {
              this.tank.in = true;
              this.canInvis = false;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 20000;
              clearTimeout(this.invis);
              this.invis = setTimeout(function() {
                this.tank.in = false;
                this.invis = setTimeout(function() {
                  this.canInvis = true;
                }.bind(this), 20000);
              }.bind(this), 20000);
            } else if (this.tank.in) {
              this.tank.in = false;
              this.canInvis = true;
            }
          } else if (PixelTanks.userData.class === 'normal') {
            // add sheidls ehre for idots
          } else if (PixelTanks.userData.class == 'tactical') {
            if (this.canMegamissle) {
              this.fire('megamissle');
              this.canMegamissle = false;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 20000
              setTimeout(function() {
                this.canMegamissle = true;
              }.bind(this), 20000);
            }
          } else if (PixelTanks.userData.class == 'builder') {
            if (this.canTurret) {
              this.canTurret = false;
              this.tank.tu = true;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 40000;
              setTimeout(function() {
                this.canTurret = true;
              }.bind(this), 40000);
            }
          } else if (PixelTanks.userData.class === 'warrior') {
            if (this.canBuff) {
              this.canBuff = false;
              this.tank.bu = true;
              this.tank.to = false;
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 30000;
              setTimeout(function() {
                this.canBuff = true;
              }.bind(this), 30000);
            }
          } else if (PixelTanks.userData.class === 'medic') {
            if (this.canHeal) {
              this.canHeal = false;
              this.tank.pl = true;
              this.tank.st = 'heal';
              this.timers.class.date = new Date();
              this.timers.class.cooldown = 30000;
              setTimeout(function() {
                this.canHeal = true;
              }.bind(this), 30000);
            }
          }
          break;
        case 27:
          this.paused = !this.paused;
          if (this.paused) {
            GUI.draw.fillStyle = '#000000';
            GUI.draw.fillRect(0, 0, 1600, 1000);
          } else {
            Menus.removeListeners();
          }
          break;
        case 18:
          document.write(JSON.stringify(this.hostupdate));
          break;
      }
    }

    keyLoop(e) {
      switch (e.keyCode) {
        case 68:
          if (this.collision(this.tank.x + this.speed, this.tank.y) && !this.tank.to) {
            this.tank.x += (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.left = false;
          } else {
            this.left = null;
          }
          break;
        case 87:
          if (this.collision(this.tank.x, this.tank.y - this.speed) && !this.tank.to) {
            this.tank.y -= (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.up = true;
          } else {
            this.up = null;
          }
          break;
        case 65:
          if (this.collision(this.tank.x - this.speed, this.tank.y) && !this.tank.to) {
            this.tank.x -= (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.left = true;
          } else {
            this.left = null;
          }
          break;
        case 83:
          if (this.collision(this.tank.x, this.tank.y + this.speed) && !this.tank.to) {
            this.tank.y += (this.halfSpeed) ? this.speed / 2 : this.speed;
            this.up = false;
          } else {
            this.up = null;
          }
          break;
        case 16:
          if (this.canBoost) {
            this.speed = 16;
            this.canBoost = false;
            this.tank.i = true;
            setTimeout(function() {
              this.speed = 4;
              this.tank.i = false;
            }.bind(this), 500);
            var cooldown = 5000;
            if (PixelTanks.userData.kit == 'cooldown') {
              cooldown *= .9;
            }
            setTimeout(function() {
              this.canBoost = true;
            }.bind(this), cooldown);
          }
          break;
      }
      if (this.SETTINGS.fps) {
        this.send();
      }
    }

    emote(id) {
      clearInterval(this.emoteAnimation);
      clearTimeout(this.emoteTimeout);
      if (PixelTanks.images.emotes[id].type === 0) { // loop emote
        this.tank.e = {
          a: id,
          f: 0,
        };
        this.emoteAnimation = setInterval(function() {
          if (this.tank.e.f != PixelTanks.images.emotes[id].frames) {
            this.tank.e.f++;
          } else {
            this.tank.e.f = 0;
          }
        }.bind(this), 50);
        this.emoteTimeout = setTimeout(function() {
          clearInterval(this.emoteAnimation);
          this.tank.e = null;
        }.bind(this), 5000);
      } else if (PixelTanks.images.emotes[id].type === 1) { // single run emote
        this.tank.e = {
          a: id,
          f: 0,
        };
        this.emoteAnimation = setInterval(function() {
          if (this.tank.e.f != PixelTanks.images.emotes[id].frames) {
            this.tank.e.f++;
          } else {
            clearInterval(this.emoteAnimation);
            setTimeout(function() {
              this.tank.e = null;
            }.bind(this), 1500);
          }
        }.bind(this), 50);
      } else {
        this.tank.e = {
          a: id,
          f: 0,
        }
        this.emoteTimeout = setTimeout(function() {
          this.tank.e = null;
        }.bind(this), 5000);
      }
    }

    send() {
      this.ops++;
      this.socket.send({
        username: sessionStorage.username,
        type: 'update',
        data: {
          x: this.tank.x, // x [U, J]
          y: this.tank.y, // y [U, J]
          e: this.tank.e, // [U]
          br: this.tank.br, // leftright [U, J]
          r: this.tank.r, // rotation [U, J]
          to: this.tank.to, // toolkit [U]
          ta: this.tank.ta, // duck tape [U]
          gl: this.tank.gl, // glu [U]
          pl: this.tank.pl, // place scaffolding [U]
          st: this.tank.st, // scaffolding type
          ba: this.tank.ba, // base image stage [U, J]
          fl: this.tank.fl, // flashbang fired [U]
          bo: this.tank.bo, // bumb fired [U]
          po: this.tank.po, // power-bumb fired [U]
          i: this.tank.i, // immune [U]
          in: this.tank.in, // invis  [U]
          fi: this.tank.fi, // fire or not
          tu: this.tank.tu, // summoner turret
          bui: this.tank.bui, // builder block
          a: this.tank.a, // animation to play on tank :D
          bu: this.tank.bu, // warrior buff
          mi: this.tank.mi, // mines while boost TEMP DISABLED
          dy: this.tank.dy, // dynamite
          sh: this.tank.sh, // shields :D
          as: this.tank.as, // airstrike
        },
      });
      this.tank.st = null;
      this.tank.blockShield = false;
      this.tank.fi = [];
      this.tank.mi = false;
      this.tank.to = false;
      this.tank.ta = false;
      this.tank.pl = false;
      this.tank.tu = false;
      this.tank.fl = false;
      this.tank.bo = false;
      this.tank.po = false;
      this.tank.bu = false;
      this.tank.bui = false;
      this.tank.dy = false;
      this.tank.gl = false;
      this.tank.sh = false;
      this.tank.as = false;
    }

  }

  class SinglePlayerTank {
    constructor() {

      this.m = PixelTanks.userData.material;
      this.world = PixelTanks.user.world;

      this.b = true;
      this.p = 0;
      this.h = (this.m * 50 + 300);
      this.r = 0;
      this.x = 0;
      this.y = 0;
      this.u = true;
      this.l = true;
      this.i = false;
      this.in = false;
      this.class = PixelTanks.userData.class;
      this.s = 0;

      this.canFlashbang = true;
      this.canBlock = true;
      this.canBoost = true;
      this.canToolkit = true;
      this.canInvis = true;
      this.canFire = true;
      this.canChangeInvisStatus = true;
      this.canPowermissle = true;
      this.canMegamissle = true;
      this.canGrapple = true;

      this.speed = 4;
      this.fireType = 1;
      this.team = 'Player';

      this.intervals = [];
      this.intervals2 = [];
      this.helper = [];

      document.addEventListener('keydown', this.keydown.bind(this)); // revise | add a addListeners() function ?
      document.addEventListener('keyup', this.keyup.bind(this));
      document.addEventListener('mousemove', this.mousemove.bind(this));
      document.addEventListener('mousedown', this.mousedown.bind(this));
      document.addEventListener('mouseup', this.mouseup.bind(this));
    }

    damage(s) {
      if (this.s > 0) {
        this.s -= 1;
        return;
      }

      if (this.i) {
        return;
      }

      this.health -= s.damage;

      if (this.health <= 0) {
        setTimeout(defeat, 20);
        PixelTanks.user.world.endGame();
      }
    }

    draw() {

      if (this.in) {
        GUI.draw.globalAlpha = .5;
      }

      GUI.draw.translate(this.x + 20, this.y + 20)
      GUI.draw.rotate(this.br * Math.PI / 180);

      var key = {
        0: 'red',
        1: 'steel',
        2: 'crystal',
        3: 'dark',
        4: 'light',
      };

      GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[this.m]].bottom[this.b ? 0 : 1], -20, -20);

      GUI.draw.rotate(-this.br * Math.PI / 180);
      GUI.draw.translate((-this.x - 20), (-this.y - 20));

      GUI.draw.translate(this.x + 20, this.y + 20);
      GUI.draw.rotate(this.r * Math.PI / 180);

      GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack][key[this.m]].top, -20, -20);

      if (PixelTanks.userData.cosmetic) {
        var l = 0;
        while (l < PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics.length) {
          if (PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].name == PixelTanks.userData.cosmetic) {
            GUI.draw.drawImage(PixelTanks.images.tanks[PixelTanks.texturepack].cosmetics[l].image, -20, -20 + this.p);
          }
          l++;
        }
      }

      GUI.draw.rotate(-(this.r * Math.PI / 180));
      GUI.draw.translate(-this.x - 20, -this.y - 20);

      // Health Bar!
      GUI.draw.fillStyle = '#000000';
      GUI.draw.fillRect(this.x, this.y + 50, 40, 5);
      GUI.draw.fillStyle = '#90EE90';
      GUI.draw.fillRect(this.x + 2, this.y + 51, 36 * this.h / ((this.m * 50 + 300) * (1 + .02 * PixelTanks.userData.stats[4])), 3);

      if (this.s > 0) {
        GUI.draw.strokeStyle = '#7DF9FF';
        GUI.draw.globalAlpha = .2;
        GUI.draw.lineWidth = 5;
        GUI.draw.beginPath();
        GUI.draw.arc(this.x + 20, this.y + 20, 33, 0, Math.PI * 2);
        GUI.draw.fill();
        GUI.draw.globalAlpha = 1;
      }

      if (this.in) {
        GUI.draw.globalAlpha = 1;
      }
    }

    keydown(e) {
      e.preventDefault();
      if (this.helper[e.keyCode] !== 'pressed') {
        var cooldown = 15;
        if (this.in) {
          cooldown = 12;
        }
        this.keyHandler(e);
        this.intervals2[e.keyCode] = setInterval(function() {
          this.ba = !this.ba;
          var left = this.left;
          var up = this.up;
          if (left === null) {
            if (up === null) {
              // no angle
            } else if (up) {
              this.br = 180;
            } else if (!up) {
              this.br = 0;
            }
          } else if (left) {
            if (up === null) {
              this.br = 90;
            } else if (up) {
              this.br = 135;
            } else if (!up) {
              this.br = 45;
            }
          } else if (!left) {
            if (up === null) {
              this.br = 270;
            } else if (up) {
              this.br = 225;
            } else if (!up) {
              this.br = 315;
            }
          }
        }.bind(this), 20);
        this.intervals[e.keyCode] = setInterval(this.keyHandler.bind(this), cooldown, e);
      }
      this.helper[e.keyCode] = 'pressed';
    }

    keyup(e) {
      e.preventDefault();
      clearInterval(this.intervals[e.keyCode]);
      clearInterval(this.intervals2[e.keyCode]);
      if (e.keyCode == 65 || e.keyCode == 68) {
        this.left = null;
      }
      if (e.keyCode == 87 || e.keyCode == 83) {
        this.up = null;
      }
      this.helper[e.keyCode] = false;
    }

    mousemove(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x /= PixelTanks.resizer;
      y /= PixelTanks.resizer;
      var targetX = x - (250 + PixelTanks.offset),
        targetY = y - 250;
      var rotation = this.toAngle(targetX, targetY);
      this.r = Math.round(rotation);
    }

    toAngle(x, y) {
      var angle = Math.atan2(x, y) * 180 / Math.PI
      angle = -angle //-90;
      if (angle < 0) {
        angle += 360;
      }
      if (angle >= 360) {
        angle -= 360;
      }
      return angle;
    }

    toPoint(angle) {
      var theta = (-angle) * Math.PI / 180;
      var y = Math.cos(theta);
      var x = Math.sin(theta);
      if (x < 0) {
        if (y < 0) {
          return {
            x: -1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: -1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      } else {
        if (y < 0) {
          return {
            x: 1,
            y: Math.round(-Math.abs(y / x) * 1000) / 1000,
          };
        } else {
          return {
            x: 1,
            y: Math.round(Math.abs(y / x) * 1000) / 1000,
          };
        }
      }
    } // REVISE maybe make a Math class for this (not named Math tho)

    mousedown(e) {
      var x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - ((window.innerWidth-window.innerHeight*1.6)/2)*PixelTanks.resizer;
      var y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
      x /= PixelTanks.resizer;
      y /= PixelTanks.resizer;

      var cooldown = 0;
      if (this.fireType == 1) {
        cooldown = 200;
      }
      if (this.fireType == 2) {
        cooldown = 600;
      }
      if (this.fireType == 3) {
        cooldown = 600;
      }
      if (this.fireType == 4) {
        cooldown = 50;
      }
      if (this.fireType == 5) {
        this.fire(e.button);
        return;
      }

      if (this.canFire) {
        this.fire(e.button);
        this.canFire = false;
        setTimeout(function() {
          this.canFire = true;
        }.bind(this), cooldown);
      }

      clearInterval(this.fireInterval);
      this.fireInterval = setInterval(this.fire.bind(this), cooldown, e.button);
    }

    mouseup() {
      clearInterval(this.fireInterval);
    }

    collision(x, y) {
      if (x < 0 || y < 0 || x + 40 > 1500 || y + 40 > 1500) {
        return false;
      }
      if (this.in && this.i) return true;
      var l = 0;
      while (l<this.world.b.length) { // REVISE add the world object to be a part of the objects properties (this.world) 
        
        l++;
      }
      var l = 0, b = this.world.b;
      while (l<b.length) {
        if ((x > b[l].x || x + 40 > b[l].x) && (x < b[l].x + 50 || x + 40 < b[l].x + 50) && (y > b[l].y || y + 40 > b[l].y) && (y < b[l].y + 50 || y + 50 < b[l].y + 50)) {
          if (b[l].c) {
            return false;
          }
        }
        l++;
      }
      return true;
    }

    keyHandler(e) {
      switch (e.keyCode) {
        case 68:
          if (this.collision(this.x + this.speed, this.y)) {
            this.x += this.speed;
            this.left = false;
          } else {
            this.left = null;
          }
          break;
        case 87:
          if (this.collision(this.x, this.y - this.speed)) {
            this.y -= this.speed;
            this.up = true;
          } else {
            this.up = null;
          }
          break;
        case 65:
          if (this.collision(this.x - this.speed, this.y)) {
            this.x -= this.speed;
            this.left = true;
          } else {
            this.left = null;
          }
          break;
        case 83:
          if (this.collision(this.x, this.y + this.speed)) {
            this.y += this.speed;
            this.up = false;
          } else {
            this.up = null;
          }
          break;
        case 16:
          break;
        case 81:
          break;
        case 32:
          break;
        case 69:
          break;
        case 70:
          break;
        case 76:
          break;
        case 9:
          break;
        case 49:
          this.fireType = 1;
          clearInterval(tankSupport);
          break;
        case 50:
          //if (PixelTanks.userData.fire2) {
          this.fireType = 2;
          clearInterval(tankSupport);
          //}
          break;
        case 51:
          //if (PixelTanks.userData.fire3) {
          this.fireType = 3;
          clearInterval(tankSupport);
          //}
          break;
        case 17:
          break;
      }
    }

    fire(type) {
      var fireType = this.fireType;

      if (type === 'megamissle') {
        fireType = 1;
      } else if (type == 2) {
        if (this.canPowermissle) {
          type = 'powermissle';
          fireType = 1;
          this.canPowermissle = false;
          var cooldown = 10000;
          if (PixelTanks.userData.kit === 'cooldown') {
            cooldown *= .9;
          }
          setTimeout(function() {
            this.canPowermissle = true;
          }.bind(this), cooldown);
        } else {
          if (this.fireType == 1) {
            type = 'bullet';
          } else if (this.fireType == 2) {
            type = 'shotgun';
          }
        }
      } else if (type === 'dynamite') {
        type = 'dynamite';
        fireType = 1;
      } else {
        if (this.fireType == 1) {
          type = 'bullet';
        } else if (this.fireType == 2) {
          type = 'shotgun';
        }
      }

      var xd = [],
        yd = [],
        ro = [];

      if (fireType == 1) {
        var data = this.toPoint(this.r);
        xd.push(data.x);
        yd.push(data.y);
        ro.push(this.r);
      } else if (fireType == 2) {
        var offset = -10;
        while (offset !== 10) {
          var data = this.toPoint(this.r + offset);
          xd.push(data.x);
          yd.push(data.y);
          ro.push(this.r);
          offset += 5;
        }
      } else if (fireType == 3) {
        var data = this.toPoint(this.tank.r);
        var l = 0;
        while (l < 5) {
          xd.push(data.x);
          yd.push(data.y);
          ro.push(this.r);
          l++;
        }
      } else if (fireType == 4) {
        var data = this.toPoint(this.tank.r);
        var l = 0;
        while (l < 5) {
          xd.push(data.x);
          yd.push(data.y);
          ro.push(this.r);
          l++;
        }
      }
      var l = 0;
      while (l < xd.length) {
        PixelTanks.user.world.s.push(new Shot(this.x+20, this.y+20, xd[l], yd[l], type, ro[l], PixelTanks.user.username, PixelTanks.user.world));
        l++;
      }
    }
  }

  class World {
    constructor() {
      this.LEVELS = {
        1: ['                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '         111111111            ', '         122222221            ', '         12#####21            ', '         12#===#21            ', '         12#=@=#21            ', '         12#= =#21            ', '         12## ##21            ', '         1222 2221            ', '         1111 1111            ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              ', '                              '],

        2: [],
        3: [],
        4: [],
        5: [],
      };
      this.xp = 0;
      this.coins = 0;
      this.foes = 0;
      this.i = [];
      this.t = [];
      this.s = [];
      this.b = [];
      this.ai = [];
    }

    pauseGame() {

    }

    endGame() {
      window.removeEventListener('blur', PixelTanks.user.world.leaveWindow);
      document.removeEventListener('visibilitychange', PixelTanks.user.world.tabHandler);
      document.removeEventListener('click', pauseOnClick);
      document.removeEventListener('keydown', PixelTanks.user.world.pauseHandler);
      this.removeListeners();
      var l = 0;
      while (l < PixelTanks.user.tank.intervals.length) {
        if (PixelTanks.user.tank.intervals[l]) {
          clearInterval(PixelTanks.user.tank.intervals[l]);
        }
        l++;
      }
      gameHelper();
    }

    tick() {
      var l = 0;
      while (l < this.s.length) {
        this.s[l].update();
        l++;
      }

      if (PixelTanks.user.tank.p != 0) {
        PixelTanks.user.tank.p += 1;
      }
      // ai here
    }

    init() {
      this.intervals();
      PixelTanks.user.tank = new SinglePlayerTank();
      this.levelReader(this.LEVELS[this.level]);
    }

    levelReader(array) {
      this.map = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];
      this.borders = [0, 1500, 0, 1500];
      for (var l = 0; l < array.length; l++) {
        for (var q = 0; q < array[l].split('').length; q++) {
          var p = array[l].split(''); // Block key: # = invincible, 1 = weak, 2 = strong, @ = player, A = Area, C = Condensed, W = Weak Tanks, I = normal T = turret, B = Invis Area, D = Invis Condensed, X = Invis Weak, L = loot create, U = PowerBullet Turret, V = Mega Bullet Turret
          if (p[q] == '#') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, Infinity, 'barrier', this));
          } else if (p[q] == '1') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, 120, 'weak', this))
          } else if (p[q] == 'G') {
            this.map[l] += '1';
            this.b.push(new Block(q*50, l**50, 360, 'gold', this));
          } else if (p[q] == '2') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, 240, 'strong', this));
          } else if (p[q] == '=') {
            this.map[l] += '1';
            this.b.push(new Block(q * 50, l * 50, Infinity, 'void', this));
          } else if (p[q] == 'I') {
            this.map[l] += '0';
            createAi(q * 50, l * 50, false, 1, false, null, 'Eval');
          } else if (p[q] == 'W') {
            PixelTanks.user.world.map[l] += '0';
            createAi(q * 50, l * 50, false, 0, false, null, 'Eval');
          } else if (p[q] == 'A') {
            PixelTanks.user.world.map[l] += '0';
            createAi(q * 50, l * 50, false, 2, false, null, 'Eval');
          } else if (p[q] == 'C') {
            PixelTanks.user.world.map[l] += '0';
            createAi(q * 50, l * 50, false, 3, false, null, 'Eval');
          } else if (p[q] == 'T') {
            createAi(q * 50, l * 50, 'turret', 1, false, null, 'Eval');
          } else if (p[q] == '@') {
            PixelTanks.user.world.map[l] += '0';
            PixelTanks.user.tank.x = q * 50;
            PixelTanks.user.tank.y = l * 50;
          } else if (p[q] == ' ') {
            PixelTanks.user.world.map[l] += '0';
          }
        }
      }
    }

    frame() {
      GUI.draw.translate(PixelTanks.offset, 0);

      GUI.draw.fillStyle = '#000000';
      GUI.draw.fillRect(-1000, -1000, 3500, 3500);
      GUI.draw.clearRect(0, 0, 500, 500);

      GUI.draw.drawImage(PixelTanks.images.blocks[PixelTanks.texturepack].floor, 0, 0, 1500, 1500);

      PixelTanks.user.tank.draw();

      var l = 0;
      while (l<this.ai.length) {
        this.ai[l].draw();
        l++;
      }

      l = 0;
      while (l<this.b.length) {
        this.b[l].draw();
        l++;
      }

      l = 0;
      while (l<this.b.length) {
        this.b[l].drawHealth();
        l++;
      }

      l = 0;
      while (l<this.s.length) {
        this.s[l].draw([{
          u: sessionStorage.username,
          col: '#0000FF',
        }, {
          u: 'Eval',
          col: '#FF0000',
        }]);
        l++;
      }

      var l = 0;
      while (l < this.b.length) {
        if (this.b[l].damagedRecent) {
          GUI.draw.fillStyle = '#000000';
          GUI.draw.fillRect(b[l].x * 50, b[l].y * 50 + 50, 50, 5);
          GUI.draw.fillStyle = 'blue';
          GUI.draw.fillRect(b[l].x * 50 + 2, b[l].y * 50 + 51, 46 * b[l].health / b[l].maxHealth, 3);
        }
        l++;
      }


      GUI.draw.fillStyle = '#ffffff';
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.CanBoost) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(100, 450, 50, 50);
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.CanToolkit) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(183, 450, 50, 50);
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.canPlaceScaffolding) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(266, 450, 50, 50);
      GUI.draw.globalAlpha = .4;

      if (PixelTanks.user.tank.canFireFlashbang) GUI.draw.globalAlpha = .8;
      GUI.draw.fillRect(349, 450, 50, 50);

      //GUI.draw.drawImage(coins, 0, 0);
      GUI.draw.fillText(PixelTanks.user.world.coins, 70, 40);

      if (this.level == 1) {
        GUI.draw.textAlign = 'center';
        GUI.draw.fillText('Welcome To Pixel Tanks!', 4 * 50, 26 * 50);
        GUI.draw.fillText('These Are Weak Blocks. Shoot to break them.', 11 * 50, 25.5 * 50);
        GUI.draw.fillText('Strong Blocks. Harder to Break.', 21 * 50, 25.5 * 50);
        GUI.draw.fillText('Invincible Blocks.', 21 * 50, 27 * 50);
        GUI.draw.fillText('Your first enemy awaits. Use right click(missle) and F(shield).', 17 * 50, 15 * 50);
        GUI.draw.fillText('Destroy all enemies to win!', 3 * 50, 12 * 50);
      }
    }

    intervals() {
      this.fps = 60;
      this.i.push(setInterval(this.frame.bind(this), 1000/this.fps));
      //this.i.push(setInterval(this.tick.bind(this), 1000/120));
    }
  }

  window.onload = PixelTanks.start;
})();
