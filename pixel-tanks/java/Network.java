public class Network {
  public static get(callback) {
    PixelTanks.socket.send({
      op: 'database',
      type: 'get',
      username: sessionStorage.username,
      token: sessionStorage.token,
    });
    PixelTanks.socket.on('message', (data) => {
      if (data.status === 'success' && data.type === 'get') {
        PixelTanks.socket.no('message');
        callback(JSON.parse(data.data));
      }
    });
  }
  static update(key, value) {
    try {
      PixelTanks.socket.send({
        op: 'database',
        type: 'set',
        username: sessionStorage.username,
        token: sessionStorage.token,
        key: key,
        value: value,
      });
      PixelTanks.socket.on('message', function(data) {
        if (data.success) {
          PixelTanks.socket.no('message');
          console.log('Saved Game Successfully!');
        }
      });
    } catch (e) {}
  }
  static auth(username, password, type, callback) {
    PixelTanks.socket.send({
      op: 'auth',
      type: type,
      username: username,
      password: password,
    });
    PixelTanks.socket.on('message', (data) => {
      if (data.status === 'success') {
        PixelTanks.socket.no('message');
        sessionStorage.username = username;
        sessionStorage.token = data.token;
        callback();
      }
    })
  }
}
